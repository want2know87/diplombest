$(document).ready(function() {
    if (localStorage.getItem('hideVideoBaner') == 1) {
        $(".call-banner").hide();
    }
    //Слайдер
    var myPaySwiper = new Swiper('.pay-container-js', {
        speed: 600,
        spaceBetween: 0,
        slidesPerView: 4,
        allowSwipeToNext: true,
        nextButton: '.page-slider__next',
        prevButton: '.page-slider__prev',
        breakpoints: {
            768: {
                slidesPerView: 2
            },

            500: {
                slidesPerView: 1
            },
        }
    });

    //Отрисовка svg
    var svg = new Walkway({
        selector: "#abc",
        duration: 8000
    }).draw();

    var svg = new Walkway({
        selector: "#work",
        duration: 8000
    }).draw();

    var svg = new Walkway({
        selector: "#science",
        duration: 8000
    }).draw();

    var svg = new Walkway({
        selector: "#pen",
        duration: 8000
    }).draw();

    var svg = new Walkway({
        selector: "#внутренний_круг",
        duration: 15000
    }).draw();


    //Закрытие вверхнего баннера
    (function() {
        $(".call-banner__close").click(function(e) {
            e.preventDefault();
            $(".call-banner").hide();
            localStorage.setItem('hideVideoBaner', 1);
        });
    }());

    //Мобильное меню
    $(".burger").on("click", function(e) {
        $(this).toggleClass("open");
        $(".main-nav").toggleClass("active");
    });

    $(".main-nav__close").on("click", function(e) {
        $(".main-nav").removeClass("active");
        $(".burger").removeClass("open");
    });

    $(".contact-link").on("click", function(e) {
        e.preventDefault();
        $(".contacts").stop(true, true).toggleClass("active");
    });

    // fileinput change
    $('.fileinput-button input[type="file"]').change(function () {
        var files = $(this)[0].files;
        if (files.length > 1) {
            $('.file-hint').html('Число файлов: '+files.length);
            return;
        }
        $('.file-hint').html(files[0].name);
    });
});//Конец

(function() {
    function showContacts() {
        var $windowWidth = $(window).width(),
            $contacts = $(".contacts"),
            $mobileContacts = $(".contacts-mobile");

        if ($windowWidth > 1205) {
            $contacts.removeClass("active");
            $contacts.on("mouseenter", function() {

                    $(this).addClass("active");

                })
                .on("mouseleave", function() {

                    $(this).removeClass("active");
                })
                .off("click");

        } else {
            $contacts.off("mouseenter mouseleave");
            $contacts.on("click", function(e) {
                e.preventDefault();
                $(this).toggleClass("active");

                if ($contacts.hasClass("active")) {
                    contacts.css("display", "block");
                } else {
                    contacts.css("display", "none");
                }

            });
        }
    };

    $(window).on("load resize", showContacts);

}());



(function() {
    function showSubMenu() {
        var $windowWidth = $(window).width(),
            $item=$(".main-nav__item .main-nav__link--trigger"),
            $subMenu = $(".sub-menu");

        if ($windowWidth > 1205) {
            $item.off("click");
            $subMenu.removeAttr("style");


        } else {
            $item.on("click", function() {
                $subMenu.stop(true,false).slideToggle().toggleClass("open");

            });
        };

    };

    setTimeout(function() {
        $(window).on("load resize",showSubMenu);
    },400)




}());


(function() {

    $(window).on('load resize', function() {
            if($(window).width() < 767) {
                $('#Order_theme').tooltip('destroy');
                 $('#Order_email').tooltip('destroy');
                 $('#Order_phone').tooltip('destroy');

                $('#Order_theme').tooltip({
                    placement:'bottom'
                });

                 $('#Order_theme').tooltip({
                    placement:'bottom'
                });

                $('#Order_email').tooltip({
                    placement:'bottom'
                });

                 $('#Order_phone').tooltip({
                    placement:'bottom'
                });

            } else {

                $('#Order_theme').tooltip({
                    placement:'right'
                });

                $('#Order_email').tooltip({
                    placement:'right'
                });

                  $('#Order_phone').tooltip({
                    placement:'right'
                });
            }
   });
}());

