<?php

/**
 * This is the model class for table "orderStatus".
 *
 * The followings are the available columns in table 'orderStatus':
 * @property integer $id
 * @property string $name
 * @property string $description
 */
class OrderStatus extends CActiveRecord
{
	const NEWORDER = 1;
	const CLOSED = 10;
	const CANCELLED = 3;

	const CONTEXT_WORK = 'work';
	const CONTEXT_PAYMENT = 'payment';
	const CONTEXT_USER_MESSAGES = 'userMessages';
	const CONTEXT_AUTHOR_MESSAGES = 'authorMessages';
	const CONTEXT_MEMBERS = 'members';

	protected static $contexts = array(
		self::CONTEXT_WORK => 'История операций',
		self::CONTEXT_PAYMENT => 'Оплата',
		self::CONTEXT_USER_MESSAGES => 'Сообщения пользователя',
		self::CONTEXT_AUTHOR_MESSAGES => 'Сообщения автора',
		//self::CONTEXT_MEMBERS,
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orderStatus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('context, name, isInitial', 'required'),
			array('name', 'length', 'max'=>100),
			array('context', 'length', 'max'=>64),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'orders' => array(self::HAS_MANY, 'Order', 'orderStatus_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Статус',
			'context' => 'Контекст',
			'description' => 'Что происходит сейчас',
			'isInitial' => 'Является начальным',
			'isFinal' => 'Является конечным',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('context',$this->context);
		$criteria->compare('isInitial',$this->isInitial);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getContexts() {
	    return self::$contexts;
	}

	public static function getContextName($context) {
		if (self::contextExists($context)) {
		    return self::$contexts[$context];
		}
	}

	public static function contextExists($context) {
		if (array_key_exists($context, self::$contexts)) {
			return true;
		}
		throw new Exception('Unknown status context');
	}

	public function initial() {
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 't.isInitial=1',
		));
		return $this;
	}

	public function context($context) {
		if (OrderStatus::contextExists($context)) {
			$this->getDbCriteria()->mergeWith(array(
				'condition'=> sprintf('t.context=\'%s\'', $context),
			));
		}
		return $this;
	}

}
