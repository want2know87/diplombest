<?php

/**
 * This is the model class for table "orderPayments".
 *
 * The followings are the available columns in table 'orderPayments':
 * @property integer $id
 * @property integer $order_id
 * @property integer $user_id
 * @property integer $manager_id
 * @property string $paymentType
 * @property string $sum
 * @property string $sumTotal
 * @property integer $sumConfirmed
 * @property string $paymentStatus
 * @property string $paymentDate
 * @property string $comment
 * @property string $createdOn
 * @property string $updatedOn
 */
class OrderPayment extends CActiveRecord
{
	const STATUS_NEW = 'new';
	const STATUS_CANCELLED = 'cancelled';
	const STATUS_CONFIRMED = 'confirmed';

	const TYPE_BANK = 'bank';
	const TYPE_YAMONEY = 'yamoney';
	const TYPE_WEBMONEY = 'webmoney';

	protected static $types = array(
		self::TYPE_BANK => 'Банк',
		self::TYPE_YAMONEY => 'Яндекс.Деньги',
		self::TYPE_WEBMONEY => 'Webmoney',
	);

	protected static $statuses = array(
		self::STATUS_NEW => 'Новый',
		self::STATUS_CANCELLED => 'Отменен',
		self::STATUS_CONFIRMED => 'Подтвержден',
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orderPayments';
	}

	/**
	 * @return array behaviours for model.
	 */
	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'createdOn',
				'updateAttribute' => 'updatedOn',
			)
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, user_id, paymentType, sum, sumTotal, paymentStatus, paymentDate', 'required', 'on' => 'insert'),
			array('manager_id, sumConfirmed', 'unsafe', 'on' => 'insert'),
			array('paymentDate', 'date', 'format' => 'dd.MM.yyyy hh:mm', 'on' => 'insert'),
			array('order_id, manager_id, paymentType, sum, sumTotal, sumConfirmed, paymentStatus, comment', 'required', 'on' => 'update'),
			array('order_id, user_id, manager_id, sumConfirmed', 'numerical', 'integerOnly'=>true),
			//array('order_id', 'exist', 'className' => 'Order', 'attributeName' => 'id'),
			array('paymentType', 'in', 'range' => array_keys(self::getTypes())),
			array('paymentStatus', 'in', 'range' => array_keys(self::getStatuses())),
			array('sum, sumTotal', 'length', 'max'=>11),
			array('comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, order_id, user_id, manager_id, paymentType, sum, sumTotal, sumConfirmed, paymentStatus, paymentDate, comment, createdOn, updatedOn', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
			'attachments' => array(
				self::HAS_MANY, 
				'OrderAttachment', 
				array('order_id' => 'order_id'), 
				'with' => array(
					'message',
					'select' => false,
				),
				'condition' => 'message.context=:context',
				'params' => array(':context' => OrderStatus::CONTEXT_PAYMENT),
				'order' => 'uploadedOn DESC', 
				'group' => 'path',
			),
		);
	}

	/**
	 * @return array scopes.
	 */
	public function scopes()
	{
		return array(
			'own' => array('condition' => 'user_id=:user_id', 'params' => array(':user_id' => Yii::app()->user->getId())),
			/*'new' => array(
				'condition' => 'orderStatus_id=' . OrderStatus::NEWORDER
			),*/
			'actual' => array(
				'condition' => 'paymentStatus IN (:new, :confirmed)',
				'params' => array(':new' => self::STATUS_NEW, ':confirmed' => self::STATUS_CONFIRMED),
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Платеж',
			'order_id' => 'Заказ',
			'user_id' => 'Пользователь',
			'manager_id' => 'Менеджер',
			'paymentType' => 'Способ платежа',
			'sum' => 'Сумма без комиссии',
			'sumTotal' => 'Сумма с учетом комиссии',
			'sumConfirmed' => 'Сумма',
			'paymentStatus' => 'Статус платежа',
			'paymentDate' => 'Дата и время платежа',
			'comment' => 'Комментарий',
			'createdOn' => 'Создан',
			'updatedOn' => 'Изменен',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('manager_id',$this->manager_id);
		$criteria->compare('paymentType',$this->paymentType,true);
		$criteria->compare('sum',$this->sum,true);
		$criteria->compare('sumTotal',$this->sumTotal,true);
		$criteria->compare('sumConfirmed',$this->sumConfirmed);
		$criteria->compare('paymentStatus',$this->paymentStatus,true);
		$criteria->compare('paymentDate',$this->paymentDate,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('createdOn',$this->createdOn,true);
		$criteria->compare('updatedOn',$this->updatedOn,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function forOrder(Order $order) {
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'t.order_id=:order_id',
			'params' => array(
				':order_id' => $order->id,
			),
			'order'=>'t.createdOn desc'
		));
		return $this;
	}

	public function forUser(User $user) {
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'t.user_id=:user_id',
			'params' => array(
				':user_id' => $user->id,
			),
			'order'=>'t.createdOn desc'
		));
		return $this;
	}

	public static function getStatuses() {
	    return self::$statuses;
	}

	public static function getTypes() {
	    return self::$types;
	}

	public function statusName() {
	    return self::$statuses[$this->paymentStatus];
	}

	public function typeName() {
	    return self::$types[$this->paymentType];
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
