<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 */
class User extends CActiveRecord
{
	const SCENARIO_SILENT = 'silent';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_EXTERNAL = 'external';
	const SCENARIO_LOGIN = 'login';
	const SCENARIO_REMIND = 'remind';
	const SCENARIO_RECOVER = 'recover';
	const SCENARIO_CHANGE_PASSWORD = 'change_password';
	const SCENARIO_ORDER = 'order';

	public $password_open;
	public $password_old;
	public $password_new;
	public $password_repeat;

	public $token;

	public $rememberMe;

	protected $identity;
	protected $isSilentlyRegistered = false;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username', 'safe'),
			array('email', 'required', 'on' => self::SCENARIO_SILENT),
			array('email', 'email', 'on' => self::SCENARIO_SILENT),
			array('password', 'required', 'on' => self::SCENARIO_SILENT),
			array('email', 'safe', 'on' => self::SCENARIO_EXTERNAL), //TODO: e-mail is not safe!
			array('username, password, email', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, password, email', 'safe', 'on'=>'search'),

			// email and password are required
			array('email, password', 'required', 'on' => self::SCENARIO_LOGIN),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean', 'on' => self::SCENARIO_LOGIN),
			// password needs to be authenticated
			array('password', 'authenticate', 'on' => self::SCENARIO_LOGIN),

			array('email', 'validateEmail', 'on' => self::SCENARIO_REMIND),

			array('email', 'required', 'on' => self::SCENARIO_ORDER),
			array('email', 'email', 'on' => self::SCENARIO_ORDER),

			array('token, password_new, password_repeat', 'required', 'on' => self::SCENARIO_RECOVER),
			array('password_repeat', 'compare', 'compareAttribute'=>'password_new', 'on' => self::SCENARIO_RECOVER),

			array('password_new, password_repeat', 'required', 'on' => self::SCENARIO_CHANGE_PASSWORD),
			array('password_old', 'validatePassword', 'on' => self::SCENARIO_CHANGE_PASSWORD),
			array('password_repeat', 'compare', 'compareAttribute'=>'password_new', 'on' => self::SCENARIO_CHANGE_PASSWORD),

			array('email', 'required', 'on' => self::SCENARIO_UPDATE),
			array('email', 'email', 'on' => self::SCENARIO_UPDATE),
			array('password', 'unsafe', 'on' => self::SCENARIO_UPDATE),
			array('name', 'safe', 'on' => self::SCENARIO_UPDATE),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accounts' => array(self::HAS_MANY, 'Account', 'user_id'),
			'tokens' => array(self::HAS_MANY, 'UserToken', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Имя пользователя',
			'email' => 'E-mail',
			'password'=>'Пароль',
			'rememberMe'=>'Запомнить меня на 30 дней',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	protected function beforeValidate(){
		if ($this->scenario == self::SCENARIO_SILENT) {
			$this->password_open = self::generatePassword();
			$this->password = md5($this->password_open);
		}
        return parent::beforeValidate();
    }

	protected function beforeSave(){
		if ($this->scenario == self::SCENARIO_CHANGE_PASSWORD) {
			$this->password_open = $this->password_new;
			$this->password = md5($this->password_new);
		}
        //$this->password = md5($this->password);
        return parent::beforeSave();
    }

	protected function afterSave() {
		if ($this->scenario == self::SCENARIO_SILENT) {
			$this->notify('auth-register', Yii::t('User', 'Регистрация на сайте'));
		}
		if ($this->scenario == self::SCENARIO_CHANGE_PASSWORD) {
			$this->notify('auth-change-password', Yii::t('User', 'Изменение пароля'));
		}
		return parent::afterSave();
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute, $params)
	{
		if (!$this->hasErrors()) {
			$this->identity = new UserIdentity($this->email, $this->password);
			if (!$this->identity->authenticate()) {
				$this->addError('password', Yii::t('User', 'Неверный логин или пароль'));
			}
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if(is_null($this->identity))
		{
			return false;
			$this->identity = new UserIdentity($this->email, $this->password);
			//$this->identity->authenticate();
		}
		if ($this->identity->isAuthenticated)
		{
			$duration = $this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->identity, $duration);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Logs in silently created user.
	 * @return boolean whether login is successful
	 */
	public function trySilentLogin() {
		if ($this->isSilentlyRegistered) {
			return $this->silentLogin();
		} else {
		    //throw new Exception(CVarDumper::dumpAsString($this->getAttributes()));
		}
		return false;
	}

	/**
	 * Silently logs in user.
	 * @return boolean whether login is successful
	 */
	public function silentLogin() {
		if (!$this->isNewRecord) {
			$identity = new SilentUserIdentity($this);
			Yii::app()->user->login($identity);
			echo 'Logged in';
			return true;
		} else {
		    //throw new Exception(CVarDumper::dumpAsString($this->getAttributes()));
		}
		return false;
	}

	public function notify($view, $subject) {
		Yii::import('ext.yii-mail.*');
		$message = new YiiMailMessage($subject, null, 'text/html', 'UTF-8');
		$message->view = $view;
		$message->from = Yii::app()->params['noreplyEmail'];
		$message->addTo($this->email);
		$message->setBody(array('model' => $this));
		Yii::app()->mail->send($message);
	}

    //matching the old password with your existing password.
    public function validatePassword($attribute, $params)
    {
        $user = User::model()->findByPk(Yii::app()->user->id);
        if ($user->password != md5($this->password_old))
            $this->addError($attribute, 'Old password is incorrect.');
    }

	public function validateEmail($attribute, $params)
	{
		$user = User::model()->findByAttributes(array('email' => $this->email));
		if (!$user) {
            $this->addError($attribute, 'User not found.');
		} else {
			$this->setPrimaryKey($user->getPrimaryKey());
		    $this->setAttributes($user->getAttributes());
		}
	}

	public static function getByEmail($email, $silentRegister = false) {
		$user = User::model()->findByAttributes(array('email' => $email));
		if (!$user && $silentRegister) {
			$user = new User(self::SCENARIO_SILENT);
			$user->email = $email;
			$user->username = substr($email, 0, strpos($email, '@'));
			if ($user->save()) {
			    $user->refresh();
			}
		}
		return $user;
	}

	public function account($service){
		foreach ($this->accounts as $account) {
		    if ($account->service == $service) {
		        return $account;
		    }
		}
		return;
	}

	public function withToken($token) {
		$this->getDbCriteria()->mergeWith(array(
			'with' => 'tokens',
			'condition' => 'tokens.token=:token AND tokens.validTill > NOW()',
			'params' => array(
				':token' => $token,
			),
		));
		return $this;
	}

	public static function generatePassword() {
		$password = base_convert(md5(microtime()), 16, 36);
		$len = rand(7, 10);
		$password = substr($password, rand(0, strlen($password) - $len), $len);
		$password = strtoupper(substr($password, 0, 4)) . substr($password, 4);
		$password = str_shuffle($password);
		return $password;
	}

	public function mergeWith($id) {
	    Account::model()->updateAll(
			array('user_id' => $this->id),
			'user_id = :user_id',
			array('user_id' => $id)
		);
	    Order::model()->updateAll(
			array('user_id' => $this->id),
			'user_id = :user_id',
			array('user_id' => $id)
		);
	    OrderStatusLog::model()->updateAll(
			array('user_id' => $this->id),
			'user_id = :user_id',
			array('user_id' => $id)
		);
	    OrderAttachment::model()->updateAll(
			array('user_id' => $this->id),
			'user_id = :user_id',
			array('user_id' => $id)
		);
		return $this;
	}

	public function getUsernameLabel()
	{
		$label = trim($this->username);
		if (!empty($label)) {
			return $label;
		}

		$label = trim($this->name);
		if (!empty($label)) {
			return $label;
		}

		$label = trim($this->email);
		if (!empty($label)) {
			return $label;
		}

		return null;
	}

}
