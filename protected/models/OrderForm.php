<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class OrderForm extends CFormModel
{
	public $theme;
	public $subject;
	public $type;
	public $date;
	public $extra;
	public $name;
	public $phone;
	public $user_id;
	public $email;
	public $verifyCode;

	public $subjects = array(
		'1С',
		'1С программирование',
		'ArchiCAD',
		'Assembler',
		'AutoCADs',
		'C',
		'C#',
		'C++',
		'Compas',
		'Delphi',
		'ERP системы',
		'FoxPro',
		'FreeBSD',
		'HTML',
		'IBM DB2',
		'Informix',
		'InterBase',
		'Java',
		'Linux, Unix',
		'MBA право',
		'MBA экономика',
		'MathCAD',
		'MathLAB',
		'Microsoft Access',
		'Microsoft Excel',
		'Microsoft .NET',
		'Microsoft Office',
		'MySQL',
		'Olap',
		'Olap 2',
		'Oracle',
		'PHP, Perl',
		'Pascal',
		'Postgres',
		'Power Point',
		'PowerBuilder',
		'Progress',
		'SPSS',
		'SQL Server',
		'Statistica',
		'Sybase',
		'Visual Basic',
		'Windows',
		'АХД предприятия',
		'Авиатранспорт',
		'Автоматизация',
		'Автотранспорт',
		'Аграрное право',
		'Адвокатура',
		'Административное право',
		'Акушерство',
		'Анализ текста',
		'Анализ хозяйственной деятельности',
		'Аналитическая геометрия',
		'Аналитическая химия',
		'Анатомия',
		'Анестезиология',
		'Антикризисное управление',
		'Античная философия',
		'Антропология',
		'Арбитражный процесс',
		'Археология',
		'Архивоведение',
		'Архитектура',
		'Астрология, составление гороскопов',
		'Астрометрия',
		'Астрономия',
		'Аудит',
		'Аэрогазодинамика',
		'Банки и биржи',
		'Банковский кредит',
		'Банковский маркетинг',
		'Банковское дело',
		'Банкротство',
		'Беспроводные сети',
		'Библиография',
		'Библиотековедение',
		'Бизнес-планирование',
		'Биогеография',
		'Биография',
		'Биология',
		'Биотехнология',
		'Биохимия',
		'Биржевое дело',
		'Богословие',
		'Ботаника',
		'Брендинг',
		'Бухгалтерский учет',
		'Бухгалтерский учет, анализ и аудит',
		'Бухучет в бюджетных организациях',
		'Бюджетирование',
		'Бюджетные системы',
		'Валеология',
		'Валютная организация и валютный контроль',
		'Валютные операции',
		'Введение в МЭО',
		'Введение в бизнес',
		'Введение в экономическую специальность',
		'Вирусология',
		'Внешнеэкономическая деятельность',
		'Внешняя политика и дипломатия',
		'Водоснабжение',
		'Военная педагогика',
		'Военная психология',
		'Военноное законадательство',
		'Возрастная психология',
		'Востоковедение',
		'Высшая математика',
		'Газоснабжение',
		'Генетика',
		'География',
		'Геодезия',
		'Геоинформатика',
		'Геология',
		'Геометрия',
		'Геоморфология',
		'Геополитика',
		'Геофизика',
		'Геохимия',
		'Геоэкология',
		'Гигиена',
		'Гидравлика',
		'Гидрология',
		'Гистология',
		'Горное дело',
		'Городской кадастр',
		'Гостиничное дело',
		'Государственная служба',
		'Государственное и муниципальное управление',
		'Государственное право',
		'Государственное регулирование экономики',
		'Государственные и муниципальные финансы',
		'Гражданский процесс',
		'Гражданское право',
		'Графика',
		'Графический дизайн',
		'Грибы и микология',
		'Декоративно-прикладное искусство',
		'Деловое общение',
		'Деловой этикет',
		'Делопроизводство и документооборот',
		'Демография',
		'Дендрология',
		'Денежное обращение',
		'Деньги и кредит',
		'Деревообработка',
		'Деревянные конструкции',
		'Детали машин',
		'Дефектология',
		'Диалектология',
		'Дизайн',
		'Дизайн интерьеров',
		'Дипломатическая служба и дипкорпус',
		'Дискретная математика',
		'Дифференциальная психология',
		'Дифференциальные уравнения',
		'Документационное обеспечение',
		'Документоведение',
		'Документооборот в бухгалтерии',
		'Дошкольная педагогика',
		'Драматургия',
		'Естествознание',
		'Ж/д транспорт',
		'Железобетонные конструкции',
		'Живопись',
		'Жилищное право',
		'Журналистика',
		'Зарубежная литература',
		'Зарубежная философия',
		'Звукотехника',
		'Здания и сооружения',
		'Земельное право',
		'Земельный кадастр',
		'Землеведение',
		'Землеустройство',
		'Зоология',
		'Игровые методики',
		'Издательское дело',
		'Имиджелогия',
		'Иммунология',
		'Инвестиции и проекты',
		'Инвестиционный менеджмент',
		'Инженерная геодезия',
		'Инженерная геология',
		'Инженерная графика',
		'Инновационный менеджмент',
		'Иностранная грамматика',
		'Институциональная экономика',
		'Интернет протоколы',
		'Интернет технологии',
		'Интерпретация текста',
		'Информатика',
		'Информатика и математика',
		'Информатика основы',
		'Информатика экономическая',
		'Информационно-аналитическая работа',
		'Информационное право',
		'Информационные ресурсы',
		'Информационные системы в БухУчёте',
		'Информационные системы в политологии',
		'Информационные технологии',
		'Информационные технологии в экономике',
		'Информационные технологии в юриспруденции',
		'Информационные технологии управления',
		'Информационный менеджмент',
		'Искусство',
		'Искусствоведение',
		'Исследование операций',
		'Исследование операций и теория игр',
		'Исследования в психологии',
		'История',
		'История 20 века',
		'История Востока',
		'История Древняя',
		'История Зарубежная',
		'История МЭО',
		'История Мировая',
		'История Отечества',
		'История Санкт-Петербурга',
		'История Средних веков',
		'История Татарстана',
		'История балета',
		'История бизнеса',
		'История государства и права',
		'История дизайна',
		'История журналистики',
		'История зарубежного искусства',
		'История искусств',
		'История кино',
		'История костюма',
		'История культуры',
		'История массовых праздников',
		'История медицины',
		'История мировой литературы',
		'История нового времени',
		'История педагогики',
		'История политических и правовых учений',
		'История психологии',
		'История развивающихся стран',
		'История рекламы',
		'История религии',
		'История современная',
		'История таможенного дела',
		'История экономики',
		'История экономических учений',
		'История эстетики',
		'История языка',
		'Ихтиология',
		'Казахский',
		'Картография',
		'Качество труда',
		'Квалиметрия',
		'Квантовая теория',
		'Кино и телевидение',
		'Климатология',
		'Книговедение',
		'Колебания и волны',
		'Коллоидная химия',
		'Коммерция',
		'Коммерческое право',
		'Коммуникации в организации',
		'Коммуникационные технологии',
		'Композиция',
		'Компьютерные технологии',
		'Кондиционирование и системы вентиляции',
		'Конкуренция и антимонопольная политика',
		'Конституционное право',
		'Конструирование',
		'Конструирование измерительных приборов',
		'Контракты',
		'Контроллинг',
		'Конфликтология',
		'Концепция современного естествознания',
		'Корпоративная культура',
		'Корпоративный менеджмент',
		'Коррекционная педагогика',
		'Космология',
		'Краеведение',
		'Кредитная политика',
		'Криминалистика',
		'Криминология',
		'Культура речи',
		'Культурный обмен',
		'Культурология',
		'Ландшафтоведение',
		'Латинский',
		'Легкая промышленность',
		'Лексикология',
		'Лесной кадастр',
		'Лизинг',
		'Лингвистика',
		'Лингвистика текста',
		'Линейная алгебра',
		'Литература',
		'Литературоведение',
		'Литология',
		'Логика',
		'Логистика, управление запасами',
		'Логопедия',
		'Локально-вычислительные сети',
		'Лучевая диагностика',
		'Макроэкономика',
		'Маркетинг',
		'Маркетинг в здравоохранении',
		'Маркетинг в фармакологии',
		'Маркетинговое исследование',
		'Математическая лингвистика',
		'Математическая статистка',
		'Математические методы в экономике',
		'Математические методы и моделирование',
		'Математические модели физической кинетики',
		'Математические основы теории систем',
		'Математический анализ',
		'Математическое программирование (линейное, динамическое)',
		'Материаловедение',
		'Матрицы',
		'Машиностроение',
		'Машины и аппараты пищевых производств',
		'Медицина',
		'Медицинское право',
		'Международное право',
		'Международные организации',
		'Международные отношения',
		'Международные политические процессы',
		'Международные рынки',
		'Международные стандарты учёта финансов',
		'Международные экономические отношения',
		'Международный бизнес',
		'Международный маркетинг',
		'Межкультурная коммуникация',
		'Менеджмент',
		'Менеджмент в здравоохранении',
		'Менеджмент в машиностроении',
		'Менеджмент в фармакологии',
		'Менеджмент организации',
		'Менеджмент туризма',
		'Менталитет русской культуры',
		'Мерзлотоведение',
		'Металловедение',
		'Металлургия',
		'Метеорология',
		'Методика',
		'Методика преподавания',
		'Методика преподавания ОБЖ',
		'Методика преподавания биологии',
		'Методика преподавания географии',
		'Методика преподавания иностранных языков',
		'Методика преподавания математики',
		'Методика преподавания менеджмента',
		'Методика преподавания педагогики',
		'Методика преподавания психологии',
		'Методика преподавания рисования',
		'Методика преподавания технических наук',
		'Методика преподавания физики',
		'Методика преподавания химии',
		'Методика преподавания экологии',
		'Методика преподавания экономики',
		'Методика проведения тренингов',
		'Методология',
		'Методы в психологии',
		'Методы защиты информации',
		'Методы оптимизации',
		'Методы распознавания образов',
		'Методы статистических испытаний (метод Монте-Карло)',
		'Метрология',
		'Метрология техническая',
		'Механизация и электрификация сельского хозяйства',
		'Механика',
		'Микробиология',
		'Микроконтроллеры',
		'Микропроцессоры',
		'Микропроцессоры в швейном производстве',
		'Микросхемы',
		'Микроэкономика',
		'Минералогия',
		'Мировая Художественная Культура',
		'Мировая культура',
		'Мировая культура моды',
		'Мировая политика',
		'Мировая экономика',
		'Моделирование одежды',
		'Молекулярная физика и термодинамика',
		'Мониторинг окружающей среды',
		'Музееведение',
		'Музыка',
		'Муниципальное право',
		'Налоги, налогообложение и налоговое планирование',
		'Налоговое право',
		'Народное искусство',
		'Начертательная геометрия',
		'Неврология',
		'Недвижимость',
		'Нейропсихология',
		'Неорганическая химия',
		'Нефтепереработка',
		'Нормы проектирования',
		'Нотариат',
		'ОБЖ',
		'Обогащение и переработка полезных ископаемых',
		'Образовательная политика',
		'Общая психология',
		'Обществознание',
		'Океанология',
		'Оптика',
		'Организационная культура и бизнес-этикет',
		'Организационное поведение',
		'Организационное производство',
		'Организация борьбы с таможенными правонарушениями',
		'Организация деятельности туристско-гостиничного бизнеса',
		'Организация и планирование производства',
		'Организация перевозок',
		'Организация производства в строительстве',
		'Организация производства и обслуживания в рест.-гостин. бизнесе',
		'Организация, нормирование и оплата труда',
		'Органическая химия',
		'Ортопедия',
		'Основы организации обслуживания населения',
		'Основы строения вещества',
		'Основы творческой деятельности журналиста',
		'Основы экологического природопользования',
		'Отопление и вентиляция',
		'Охрана окружающей среды при проектировании',
		'Охрана среды',
		'Охрана труда',
		'Оценка бизнеса',
		'Оценка персонала',
		'Оценочная деятельность',
		'ПГС',
		'Палеонтология',
		'Патология',
		'Педагогика',
		'Педагогическая психология',
		'Переводоведение',
		'Переговоры в психологии',
		'Петрография',
		'Планирование',
		'Подъемно-транспортные машины',
		'Полимеры',
		'Политическая география',
		'Политическая история России',
		'Политическая психология',
		'Политические системы',
		'Политический анализ и прогнозирование',
		'Политическое право',
		'Политология',
		'Построение и проектирование ресторанного бизнеса',
		'Почта и почтовые службы',
		'Поэтика',
		'Право',
		'Правоведение',
		'Правовые основы бизнеса',
		'Правоохранительные органы',
		'Практическая психология',
		'Предпринимательство',
		'Пресс-секретарь',
		'Пресс-служба',
		'Приборостроение',
		'Прикладная лингвистика',
		'Прикладная социология',
		'Прикладная физика',
		'Применение вычислительных методов в инженерных расчетах',
		'Природоведение',
		'Природопользование',
		'Прогнозирование и планирование',
		'Программирование',
		'Проектирование',
		'Проектирование ЖБК',
		'Проектирование баз данных',
		'Проектирование гостиничных предприятий',
		'Проектирование дорог',
		'Проектирование конструкций кожанных изделий',
		'Проектирование конструкций швейных изделий',
		'Проектирование предприятий общественного питания',
		'Проектирование ресторанных предприятий',
		'Проза и публицистика',
		'Производственное планирование',
		'Производственный маркетинг',
		'Производственный менеджмент',
		'Прокурорский надзор',
		'Промышленная вентиляция',
		'Промышленность',
		'Процессы и аппараты',
		'Психиатрия',
		'Психоанализ',
		'Психогигиена бизнеса',
		'Психодиагностика',
		'Психолингвистика',
		'Психологическое консультирование',
		'Психология',
		'Психология деятельности',
		'Психология и педагогика',
		'Психология кадровой работы',
		'Психология личности',
		'Психология менеджмента',
		'Психология рекламы',
		'Психология труда',
		'Психотерапия',
		'Психофизиология',
		'Радиоприемные и передающие устройства',
		'Радиосвязь',
		'Радиотехника',
		'Радиофизика',
		'Развитие речи',
		'Региональная экономика',
		'Регионоведение',
		'Режиссура',
		'Реклама и РR',
		'Рекламные тексты, копирайтинг',
		'Религиоведение',
		'Ремонт автомобилей и двигателей',
		'Ресторанный бизнес',
		'Римское право',
		'Риторика',
		'Розничное кредитование',
		'Русская литература',
		'Русская философия',
		'Русский',
		'Русский язык и культура речи',
		'Рынок транспортных услуг',
		'Рынок труда',
		'Рынок ценных бумаг',
		'Рыночная экономика',
		'Сберегательное дело',
		'Связи с общественностью',
		'Сексология',
		'Семейное право',
		'Семиотика',
		'Сепарационные процессы',
		'Серверные технологии',
		'Сертификация товара',
		'Сестринское дело',
		'Сети и системы связи',
		'Системный анализ',
		'Системы технологий',
		'Системы управления',
		'Случайные процессы',
		'Сопротивление материалов',
		'Социализация личности',
		'Социальная география',
		'Социальная педагогика',
		'Социальная психология',
		'Социальная работа',
		'Социально-культурная деятельность',
		'Социально-экономические и политические процессы',
		'Социо-культурный менеждмент',
		'Социолингвистика',
		'Социологическое исследование',
		'Социология общая',
		'Социология труда',
		'Социология управления',
		'Специальная психология',
		'Средства массовой информации',
		'Стандартизация и нормирование',
		'Статистика',
		'Стилистика',
		'Стоматология',
		'Страноведение',
		'Стратегический менеджмент',
		'Страхование',
		'Строительная механика',
		'Строительство (фундаменты, материаловедение)',
		'Строительство и архитектура',
		'Судебная медицина',
		'Судебно-бухгалтерская экспертиза',
		'Судебный процесс',
		'Судостроение',
		'Сурдопедагогика',
		'Схемотехника и моделирование',
		'Таможенное дело',
		'Таможенное дело и таможенные режимы',
		'Таможенное право',
		'Таможенные платежи',
		'Татарский',
		'Театр',
		'Тектоника',
		'Телекоммуникационные технологии',
		'Телемаркетинг',
		'Теоретическая механика',
		'Теория вероятности',
		'Теория второго языка',
		'Теория государства и права',
		'Теория и история книги',
		'Теория и методика преподавания иностранных языков',
		'Теория и методология обучения и воспитания',
		'Теория и практика СМИ',
		'Теория машин и механизмов',
		'Теория множеств',
		'Теория оптимального управления',
		'Теория организации',
		'Теория политики и политический процесс в современной России',
		'Теория управления',
		'Теория упругости',
		'Теория функций комплексных переменных',
		'Теория цепей',
		'Теория эволюции',
		'Тепломассообмен',
		'Теплообмен',
		'Теплотехника',
		'Термодинамика',
		'Техника защиты водных объектов',
		'Техническая механика',
		'Технологические процессы',
		'Технология и организация железнодорожных перевозок',
		'Технология конструкционных материалов',
		'Технология приготовления пищи',
		'Технология продукции общественного питания',
		'Техпроцессы и операции (обработка)',
		'Тифлопедагогика',
		'Товарная номенклатура ВЭД',
		'Товароведени медицинское',
		'Товароведение',
		'Товароведение в фармакологии',
		'Транспорт',
		'Транспортное право',
		'Тренинги',
		'Трудовое право',
		'Туризм',
		'Туристический бизнес',
		'Уголовное право',
		'Уголовный процесс',
		'Украинский',
		'Управление',
		'Управление денежными потоками',
		'Управление дошкольным образованием',
		'Управление качеством',
		'Управление общественными отношениями',
		'Управление персоналом',
		'Управление проектами',
		'Управление производством',
		'Управленческие решения',
		'Управленческий анализ',
		'Управленческий учет',
		'Уравнения математической физики',
		'Урология',
		'Устройства приема и обработки сигнала',
		'Учет в банках',
		'Учет в торговле',
		'Учет на предприятиях малого бизнеса',
		'Фармацевтика',
		'Физика',
		'Физика атома и атомного ядра',
		'Физика твердого тела',
		'Физиология',
		'Физическая химия',
		'Физические основы механики',
		'Физкультура',
		'Филология',
		'Философия',
		'Философские школы',
		'Финансовая математика',
		'Финансовая отчетность',
		'Финансовая система зарубежных стран',
		'Финансовое планирование',
		'Финансовое право',
		'Финансовые риски',
		'Финансовый менеджмент',
		'Финансы',
		'Финансы и кредит',
		'Финский',
		'Фольклор',
		'Фонетика',
		'Фотография',
		'Французский',
		'Франчайзинг',
		'Химические технологии',
		'Химия',
		'Хирургия',
		'Хозяйственное право',
		'Холодильные технологии',
		'Ценные бумаги',
		'Ценообразование',
		'Цитология',
		'Чертежи в электронном виде',
		'Чертежи от руки',
		'Численные методы',
		'Чрезвычайные ситуации',
		'Швейное производство',
		'Школьная математика',
		'Экологическое право',
		'Экология',
		'Эконометрика',
		'Экономика',
		'Экономика в туризме',
		'Экономика городского хозяйства',
		'Экономика здравоохранения',
		'Экономика и управление народного хозяйства',
		'Экономика на английском языке',
		'Экономика народного хозяйства',
		'Экономика недвижимости',
		'Экономика отрасли',
		'Экономика пищевой промышленности',
		'Экономика предприятия',
		'Экономика сельского хозяйства',
		'Экономика труда',
		'Экономико-математическое моделирование',
		'Экономическая безопасность',
		'Экономическая география',
		'Экономическая оценка',
		'Экономическая оценка инвестиций',
		'Экономическая социология',
		'Экономическая статистика',
		'Экономическая теория',
		'Экономические системы',
		'Экономический анализ',
		'Экономическое право',
		'Экскурсионные и выставочные работы',
		'Экскурсоведение',
		'Экспериментальная психология',
		'Экспертиза, ревизия и контроль',
		'Электрика',
		'Электрические аппараты',
		'Электричество и магнетизм',
		'Электроакустика',
		'Электромеханика',
		'Электроника',
		'Электронная торговля',
		'Электроснабжение',
		'Электротехника',
		'Эмпирическая социология',
		'Энергетика',
		'Энергоснабжение',
		'Энтомология',
		'Эстетика',
		'Этика',
		'Этикет',
		'Этнография',
		'Этнолингвистика',
		'Этнология',
		'Этнопсихология',
		'Юридическая психология',
		'Юриспруденция',
		'Языкознание',
		'Ясновидение, предсказания',
		'агрономия',
		'английский',
		'антенны',
		'белорусский',
		'водоочистка/водоподготовка',
		'голланский',
		'животноводство',
		'звукотехника',
		'земледелие',
		'инструмент/приспособления',
		'испанский',
		'итальянский',
		'китайский',
		'конструирование',
		'лесоводство',
		'металлические конструкции',
		'немецкий',
		'норвежский',
		'растениеводство',
		'сельское хозяйство',
		'сельскохозяйственные растения',
		'технология хим. производства',
		'цветоводство',
		'цифровые аппараты',
		'электроника',
		'электротехника',
		'японский',
	);

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('theme', 'required', 'message' => 'Тему нужно указать'),
			array('extra', 'ExRequiredValidator', 'boundAttribute' => 'type', 'boundValue' => 0, 'message' => 'Нужно указать требования'),
			array('type, email', 'required'),
			// email has to be a valid email address
			array('email', 'email'),
			// verifyCode needs to be entered correctly
			//array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'theme' => 'Тема работы',
			'subject' => 'Предмет',
			'type' => 'Тип работы',
			'date' => 'Дата сдачи',
			'extra' => 'Количество страниц, дополнительные требования',
			'date' => 'Дата сдачи',
			'email' => 'E-mail',
			'phone' => 'Телефон',
			'name' => 'Имя и фамилия',
			'verifyCode'=>'Код с картинки',
		);
	}

}