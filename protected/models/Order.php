<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property integer $user_id
 * @property integer $orderType_id
 * @property integer $branch_id
 * @property integer $orderStatus_id
 * @property string $discipline
 * @property string $theme
 * @property string $extra
 * @property string $date
 * @property string $phone
 * @property string $price
 */
class Order extends CActiveRecord
{

	protected $currentState = array();

	protected $access = array();

	public $email;

	public $orderType_secondary;

	public $manager_id = 1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array behaviours for model.
	 */
	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'createdOn',
				'updateAttribute' => 'updatedOn',
			)
		);
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('user_id, orderType_id, branch_id, discipline, theme, extra, date, price, authorSum, paidSum, requestedSum', 'required'),
			array('user_id, author_id, orderType_id, branch_id', 'numerical', 'integerOnly'=>true),
			array('discipline, theme', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>100),
			array('price', 'length', 'max'=>14),
			array('date', 'date', 'format'=>'dd.MM.yyyy', 'on'=>'add'),
			array('date', 'date', 'format'=>'dd.MM.yyyy', 'on'=>'cabinet'),
			array('extra', 'safe'),
			array('email', 'required', 'on'=>'add'),
			array('email', 'email', 'on'=>'add'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, author_id, orderType_id, branch_id, discipline, theme, extra, date, phone, price', 'safe', 'on'=>'search'),
			array('orderType_secondary, extra, uploadFiles', 'safe', 'on'=>'add'),
			array('orderType_id, theme, discipline, date, phone', 'required', 'on'=>'add'),
			array('price', 'compare', 'operator' => '>', 'compareValue' => 0, 'on'=>'evaluate'),
			array('requestedSum', 'compare', 'operator' => '>', 'compareValue' => 0, 'on'=>'evaluate'),
			array('requestedSum', 'compare', 'operator' => '<=', 'compareAttribute' => 'price', 'on'=>'evaluate'),
			array('previewAttachments', 'required', 'on'=>'preview'),
			array('finalAttachments', 'required', 'on'=>'ready'),
			array('price', 'compare', 'operator' => '>', 'compareValue' => 0, 'on'=>'payConfirm'),
			array('paidSum', 'compare', 'operator' => '>', 'compareValue' => 0, 'on'=>'payConfirm'),
			array('authorSum, paidSum, requestedSum', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'manager' => array(self::BELONGS_TO, 'User', 'manager_id'),
			'orderType' => array(self::HAS_ONE, 'OrderType', array('id' => 'orderType_id')),
			'branch' => array(self::HAS_ONE, 'Branch', array('id' => 'branch_id')),
			'state' => array(self::HAS_MANY, 'OrderStatusState', array('order_id' => 'id'), 'with' => array('status')),
			'workState' => array(
				self::HAS_ONE,
				'OrderStatusState',
				array('order_id' => 'id'),
				'condition' => 'workState.context=:context',
				'params' => array(':context' => OrderStatus::CONTEXT_WORK),
				'with' => array('status'),
			),
			'paymentState' => array(
				self::HAS_ONE,
				'OrderStatusState',
				array('order_id' => 'id'),
				'condition' => 'paymentState.context=:context',
				'params' => array(':context' => OrderStatus::CONTEXT_PAYMENT),
				'with' => array('status'),
			),
			'userLog' => array(
				self::HAS_MANY,
				'OrderStatusLog',
				array('order_id' => 'id'),
				'condition' => 'userLog.context in (:work, :payment, :messages)',
				'params' => array(
					':work' => OrderStatus::CONTEXT_WORK,
					':payment' => OrderStatus::CONTEXT_PAYMENT,
					':messages' => OrderStatus::CONTEXT_USER_MESSAGES,
				),
				'with' => array('attachments'),
			),
			'authorLog' => array(
				self::HAS_MANY,
				'OrderStatusLog',
				array('order_id' => 'id'),
				'condition' => 'userLog.context in (:messages)',
				'params' => array(
					//':work' => OrderStatus::CONTEXT_WORK,
					//':payment' => OrderStatus::CONTEXT_PAYMENT,
					':messages' => OrderStatus::CONTEXT_AUTHOR_MESSAGES,
				),
				'with' => array('attachments'),
			),
			'userMessages' => array(
				self::HAS_MANY,
				'OrderStatusLog',
				array('order_id' => 'id'),
				'condition' => 'userMessages.context=:context',
				'params' => array(':context' => OrderStatus::CONTEXT_USER_MESSAGES),
				'with' => array('attachments'),
			),
			'authorMessages' => array(
				self::HAS_MANY,
				'OrderStatusLog',
				array('order_id' => 'id'),
				'condition' => 'authorMessages.context=:context',
				'params' => array(':context' => OrderStatus::CONTEXT_AUTHOR_MESSAGES),
				'with' => array('attachments'),
			),
			'log' => array(self::HAS_MANY, 'OrderStatusLog', array('order_id' => 'id')),
			'attachments' => array(self::HAS_MANY, 'OrderAttachment', array('order_id' => 'id'), 'order' => 'uploadedOn DESC', 'group' => 'path'),
			'userAttachments' => array(
				self::HAS_MANY,
				'OrderAttachment',
				array('order_id' => 'id'),
				'with' => array(
					'message',
					'select' => false,
				),
				'condition' => 'message.context=:context',
				'params' => array(':context' => OrderStatus::CONTEXT_USER_MESSAGES),
				'order' => 'uploadedOn DESC',
				'group' => 'path',
			),
			'authorAttachments' => array(
				self::HAS_MANY,
				'OrderAttachment',
				array('order_id' => 'id'),
				'with' => array(
					'message',
					'select' => false,
				),
				'condition' => 'message.context=:context',
				'params' => array(':context' => OrderStatus::CONTEXT_AUTHOR_MESSAGES),
				'order' => 'uploadedOn DESC',
				'group' => 'path',
			),
			'previewAttachments' => array(self::HAS_MANY, 'OrderAttachment', array('order_id' => 'id'), 'condition' => 'isPreview = 1', 'order' => 'uploadedOn DESC', 'group' => 'path'),
			'finalAttachments' => array(self::HAS_MANY, 'OrderAttachment', array('order_id' => 'id'), 'condition' => 'isFinal = 1', 'order' => 'uploadedOn DESC', 'group' => 'path'),
			'payments' => array(self::HAS_MANY, 'OrderPayment', array('order_id' => 'id')),
		);
	}

	/**
	 * @return array scopes.
	 */
	public function scopes()
	{
		return array(
			'own' => array(
				'condition' => 'user_id=:user_id',
				'params' => array(':user_id' => Yii::app()->user->getId()),
				'order'=>'createdOn DESC',
			),
			/*'new' => array(
				'condition' => 'orderStatus_id=' . OrderStatus::NEWORDER
			),*/
			'unauthored' => array(
				'condition' => 'author_id IS NULL'
			),
			'authored' => array(
				'condition' => 'author_id=:author_id',
				'params' => array(':author_id' => Yii::app()->user->getId()),
			),
			'authorable' => array(
				'condition' => 'author_id IS NULL OR author_id=:author_id',
				'params' => array(':author_id' => Yii::app()->user->getId()),
				'order'=>'createdOn DESC',
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '№',
			'user_id' => 'Пользователь',
			'user.username' => 'Пользователь',
			'email' => 'E-mail',
			'author_id' => 'Автор',
			'orderType_id' => 'Тип работы',
			'branch_id' => 'Область',
			'discipline' => 'Предмет',
			'theme' => 'Тема',
			'extra' => 'Дополнительные требования',
			'date' => 'Дата сдачи',
			'phone' => 'Телефон',
			'price' => 'Цена',
			'authorSum' => 'Вознаграждение автора',
			'requestedSum' => 'Требуется оплатить',
			'paidSum' => 'Оплачено',
			'leftSum' => 'Осталось оплатить',
			'previewAttachments' => 'Файлы для ознакомления',
			'finalAttachments' => 'Готовая работа',
			'uploadFiles' => 'Прикрепить файлы',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('orderType_id',$this->orderType_id);
		$criteria->compare('branch_id',$this->branch_id);
		$criteria->compare('workState.orderStatus_id',$this->workState ? $this->workState->orderStatus_id : null);
		$criteria->compare('discipline',$this->discipline,true);
		$criteria->compare('theme',$this->theme,true);
		$criteria->compare('extra',$this->extra,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('price',$this->price,true);
		$criteria->order = 'createdOn DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function afterSave() {

		if ($this->isNewRecord) {
			$this->initializeState();

		}
		return parent::afterSave();
	}

	protected function beforeSave() {
		if ($this->isNewRecord) {

		    $this->branch_id = Discipline::getBranchId($this->discipline);
		    $this->manager_id = 1;//TODO: choose manager

			if (!$this->user) {
				$user = User::getByEmail($this->email, true);
				if ($user->hasErrors()) {
					$this->addErrors($user->getErrors());
				    return false;
				}
				$user->trySilentLogin();
				//throw new Exception('logging in');
				$this->user_id = $user->id;
			}

			if ($this->orderType_secondary) {
			    $this->orderType_id = $this->orderType_secondary;
			}
		}
		$this->date = date('Y-m-d', strtotime($this->date));
		return parent::beforeSave();
	}

	public function perform(OrderOperation $operation, $message = null) {
		$state = $this->getState($operation->context);
		if ($state->orderStatus_id != $operation->orderStatus_id) {
			throw new Exception('Impossible operation for current state.');
		}
		if (!Yii::app()->user->checkAccess($operation->role, array('order' => $this, 'operation' => $operation))) {
			throw new Exception('Impossible operation for you.');
		}
		$state->orderStatus_id = $operation->nextOrderStatus_id;
		if ($state->save()) {
		    $log = new OrderStatusLog;
			$log->context = $operation->context;
			$log->order_id = $this->id;
			$log->orderOperation_id = $operation->id;
			$log->user_id = Yii::app()->user->getId();
			$log->message = $this->evaluateExpression('"' . $operation->comment . '"', array('data' => $this));
			$log->save();
			$log->refresh();
			$state->refresh();
			$this->refresh();
			Yii::app()->user->setFlash('success', $log->message);
			return $log;
		}
		$state->refresh();
		$this->refresh();
	}

	public function initializeState() {
	    $statuses = OrderStatus::model()->initial()->findAll();
		if ($statuses) {
			foreach ($statuses as $status) {
				$state = new OrderStatusState;
				$state->context = $status->context;
				$state->order_id = $this->id;
				$state->orderStatus_id = $status->id;
				$state->save();

				if ($status->context == OrderStatus::CONTEXT_WORK) {
					$log = new OrderStatusLog;
					$log->context = $status->context;
					$log->order_id = $this->id;
					$log->orderOperation_id = OrderOperation::CREATE;
					$log->user_id = $this->user_id;
					$log->message = 'Заказ создан';
					$log->save();
				}
			}
		}
	}

	public function getState($context) {
	    foreach ($this->state as $state) {
	        if ($state->context == $context) {
	            return $state;
	        }
	    }
		$state = new OrderStatusState;
		$status = OrderStatus::model()->context($context)->initial()->find();
		if (is_null($status)) {
		    throw new Exception('Cannot initialize status in missing context.');
		}
		$state->context = $context;
		$state->order_id = $this->id;
		$state->orderStatus_id = $status->id;
		return $state;
	}

	public function checkAccess($operation, $params = array(), $allowCaching = true) {
		if ($allowCaching && !array_key_exists($operation, $this->access)) {
			$this->access[$operation] = Yii::app()->user->checkAccess($operation, array_merge($params, array('order' => $this)));
		}
		return $this->access[$operation];
	}

	public function getAllowedOperations($context) {
		if (Yii::app()->user->checkAccess('managedOrder', array('order' => $this))) {
			return $this->getState($context)->managerOperations;
		} else if (Yii::app()->user->checkAccess('authoredOrder', array('order' => $this))) {
			return $this->getState($context)->authorOperations;
		} else if (Yii::app()->user->checkAccess('myOrder', array('order' => $this))) {
			return $this->getState($context)->userOperations;
		}
	}

	public function isNew() {
	    return (bool) $this->workState->status->isInitial;
	}

	/*public function isClosed() {
	    return $this->workState->status->isFinal;
	}*/

	public function isMine() {
	    return $this->user_id == Yii::app()->user->getId();
	}

	public function isAuthored() {
	    return $this->author_id == Yii::app()->user->getId();
	}

	public function isClarified() {
	    return is_null($this->unique);
	}


}
