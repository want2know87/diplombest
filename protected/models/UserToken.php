<?php

/**
 * This is the model class for table "userTokens".
 *
 * The followings are the available columns in table 'userTokens':
 * @property integer $id
 * @property integer $user_id
 * @property string $token
 * @property string $validTill
 * @property string $createdOn
 * @property string $host
 */
class UserToken extends CActiveRecord
{
	public $isValid;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'userTokens';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, token, host', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('token', 'length', 'max'=>32),
			array('host', 'length', 'max'=>15),
			array('validTill', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, token, validTill, createdOn, host', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array scopes.
	 */
	public function scopes()
	{
		return array(
			'valid' => array('condition' => 't.validTill > NOW() AND t.usedOn IS NULL', 'with' => 'user'),
		);
	}

	/**
	 * @return array default scope.
	 */
	public function defaultScope()
	{
		return array(
			'select' => array(
				'*', 
				'CASE WHEN t.validTill > NOW() AND t.usedOn IS NULL THEN 1 ELSE 0 END as isValid',
			),
			'with' => 'user',
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'token' => 'Token',
			'validTill' => 'Valid Till',
			'createdOn' => 'Created On',
			'host' => 'Host',
		);
	}

	public function beforeValidate() {
		if ($this->scenario == 'insert') {
			$this->token = self::generate();
			$this->validTill = date('Y-m-d H:i:s', time() + 7200);
			$this->host = $_SERVER['REMOTE_ADDR'];
		}
		if ($this->scenario == 'update') {
		    
		}
	    return true;
	}


	public static function generate() {
	    return base_convert(md5(microtime()), 16, 36);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserToken the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
