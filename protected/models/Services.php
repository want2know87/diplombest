<?php

//namespace app\models;

/**
 * Модель для страницы услуги
 */
class Services extends OrderType
{
    /**
     * Возврощает все услуги
     * @return array
     */
    public function servicesAll()
	{
        $criteria = new CDbCriteria;
        $criteria->order = 't.priority desc';
        return $this->model()->base()->findAll($criteria);
	}

    public function servicesVisibleAll()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'is_primary=:is_primary';
        $criteria->order = 't.priority desc';
        $criteria->params = array(':is_primary' => 1);
        return $this->model()->base()->findAll($criteria);
    }

    public static function findBranchesByOrderType($order_type_id)
    {
        $criteria = new CDbCriteria;
        $criteria->order = 't.priority desc';
        $criteria->condition = 'visible=:visible';
        $criteria->params = array(':visible' => 1);
        $all = Branch::model()->findAll($criteria);
        $array = array();
        foreach ($all as $item) {
            $array[$item->id] = $item;
        }

        // если Отчеты по практике, то Преддипломная, Производственная и Ознакомительная
        if ($order_type_id == 3) {
            return array(
                $array[20],
                $array[21],
                $array[22],
            );
        }

        return array_filter($array, function ($item) {
            return !in_array($item->id, array(20, 21, 22));
        });
    }
}
