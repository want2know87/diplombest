<?php

class ItemContext
{
	/**
	 * @return string the associated database table name
	 */
	protected static $_config = array(
		'Order' => array(
			'name' => 'Заказ',
			'contexts' => array(
				'orderWork' => 'Работа с заказом',
				'orderPayment' => 'Оплата',
				'orderMembers' => 'Участники',
				'orderCommunication' => 'Переписка по заказу',
			),
		)
	);

	protected $_type;
	protected $_contexts;
	protected $_context;
	protected $_name;

	public function __construct($type, $name = null)
	{
		if (array_key_exists($type, self::$_config)) {
			$this->_type = $type;
			$this->_contexts = self::$_config[$this->_type]['contexts'];
			$this->_name = self::$_config[$this->_type]['name'];
		} else {
			throw new Exception('Unknown type.');
		}
	}

	public function attributeNames() {
	    return array();
	}

	public function __get($name)
	{
		if (array_key_exists($name, $this->_contexts)) {
		    return $name;
		}
		if ($name == 'context') {
			return $this->_context;
		}
		if ($name == 'name') {
			return $this->_name;
		}
	}

	public static function contexts()
	{
		return array_keys(self::_config);
	}

	public static function namedContexts()
	{
		return self::_config;
	}

	/*public static function __callStatic($name, $parameters)
	{
		if (array_key_exists($name, self::$_config)) {
			if (count($parameters) > 0 && $parameters[0] == 'name') {
			    return $contexts[$name];
			} else {
			    return $name;
			}
		} else {
		    throw new Exception('Unknown context.');
		}
	}*/

}
