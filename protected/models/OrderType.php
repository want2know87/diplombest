<?php

/**
 * This is the model class for table "orderTypes".
 *
 * The followings are the available columns in table 'orderTypes':
 * @property integer $id
 * @property string $name
 * @property string $pages
 * @property boolean $is_primary
 * @property boolean $on_main
 * @property integer $priority
 */
class OrderType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orderTypes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('alias', 'filter', 'filter'=>'trim'),
			array('name, pages, priority', 'required'),
			array('priority', 'numerical', 'integerOnly'=>true),
			array('name, title, descr, parent_id, alias', 'length', 'max'=>255),
			array('pages', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, pages', 'safe', 'on'=>'search'),
			array('is_primary, on_main', 'boolean'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array scopes.
	 */
	public function scopes()
	{
		return array(
			'base' => array('condition' => 't.parent_id is null'),
			'secondary' => array('condition' => 't.parent_id is not null'),
		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Вид работы',
			'pages' => 'Количество страниц',
			'title' => 'Заголовок',
			'is_primary' => 'Основные',
			'on_main' => 'На главной',
			'priority' => 'Порядок',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('pages',$this->pages,true);
		$criteria->compare('on_main',$this->on_main);
		$criteria->compare('is_primary',$this->is_primary);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('alias',$this->alias,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
			    'defaultOrder'=>'priority DESC',
		  	),
		));
	}

	public function getSvgIcon()
	{
		$list = array(
			'diplom'                => 'diploms.svg',
			'dissertacia'           => 'dissertacii.svg',
			'esse'                  => 'esse.svg',
			'biznes-plan'           => 'biznes-plany.svg',
			'kursovaya'             => 'kursovie.svg',
			'otchet-po-praktike'    => 'otchet-praktika.svg',
			'referat'               => 'referaty.svg',
			'podbor-repetitora'     => 'repetitor.svg',
			'prakticheskoe-zadanie' => 'zadania.svg',
		);
		return isset($list[$this->alias]) ? $list[$this->alias] : null;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function recently($limit=5)
    {
        $this->getDbCriteria()->mergeWith(array(
            'limit'=>$limit,
        ));

        return $this;
    }

    public function onMain($limit=8)
    {
        $this->getDbCriteria()
        	 ->compare('on_main',1)
             ->mergeWith(array(
            'limit'=>$limit,
            'order'=>'on_main DESC, priority DESC'
        ));

        return $this;
    }
}
