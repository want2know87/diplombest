<?php

/**
 * This is the model class for table "orderAttachments".
 *
 * The followings are the available columns in table 'orderAttachments':
 * @property integer $id
 * @property string $mimeType
 * @property string $name
 * @property string $path
 * @property integer $size
 * @property string $uploadedOn
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $orderStatusLog_id
 * @property integer $isPreview
 * @property integer $isFinal
 */
class OrderAttachment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	public $file;
	public $secureFileNames = true;

	public function tableName()
	{
		return 'orderAttachments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mimeType, name, path, size, user_id, order_id', 'required'),
			array('mimeType, name, path, size', 'required', 'on' => 'upload'),
			array('uploadedOn, uploadedOn, orderStatusLog_id, isPreview, isFinal, file', 'safe'),
			array('size, user_id, order_id, orderStatusLog_id, isPreview, isFinal', 'numerical', 'integerOnly'=>true),
			array('mimeType', 'length', 'max'=>50),
			array('name', 'length', 'max'=>255),
			array('path', 'length', 'max'=>100),
			array('file', 'file', 'on' => 'upload'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, mimeType, name, path, size, uploadedOn, user_id, order_id, orderStatusLog_id, isPreview, isFinal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
			'message' => array(self::BELONGS_TO, 'OrderStatusLog', 'orderStatusLog_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'mimeType' => 'Mime Type',
			'name' => 'Name',
			'path' => 'Path',
			'size' => 'Size',
			'uploadedOn' => 'Uploaded On',
			'user_id' => 'User',
			'order_id' => 'Order',
			'orderStatusLog_id' => 'Order Status Log',
			'isPreview' => 'Is Preview',
			'isFinal' => 'Is Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('mimeType',$this->mimeType,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('size',$this->size);
		$criteria->compare('uploadedOn',$this->uploadedOn,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('orderStatusLog_id',$this->orderStatusLog_id);
		$criteria->compare('isPreview',$this->isPreview);
		$criteria->compare('isFinal',$this->isFinal);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderAttachment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getReadableFileSize($retstring = null) {
			// adapted from code at http://aidanlister.com/repos/v/function.size_readable.php
			$sizes = array('bytes', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');

			if ($retstring === null) { $retstring = '%01.2f %s'; }

			$lastsizestring = end($sizes);

			foreach ($sizes as $sizestring) {
					if ($this->size < 1024) { break; }
					if ($sizestring != $lastsizestring) { $this->size /= 1024; }
			}
			if ($sizestring == $sizes[0]) { $retstring = '%01d %s'; } // Bytes aren't normally fractional
			return sprintf($retstring, $this->size, $sizestring);
	}

	public function path($path) {
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 't.path=:path',
			'params' => array(
				':path' => $path,
			),
		));
		return $this;
	}

	/**
	 * A stub to allow overrides of thumbnails returned
	 * @since 0.5
	 * @author acorncom
	 * @return string thumbnail name (if blank, thumbnail won't display)
	 */
	public function getThumbnailUrl($publicPath) {
		return $publicPath.$this->name;
	}

	public function getRealPath() {
		return Yii::app()->params['orderAttachmentFolder'] . '/' . substr($this->path, 0, 2) . '/' . $this->path;
	}

	/**
	 * Change our filename to match our own naming convention
	* @return bool
	*/
	public function beforeValidate() {

		//(optional) Generate a random name for our file to work on preventing
		// malicious users from determining / deleting other users' files
		if($this->secureFileNames && $this->file)
		{
			//$this->path = sha1( Yii::app( )->user->id.microtime( ).$this->name);
			//$this->path .= ".".$this->file->getExtensionName( );
			$this->path = sha1_file($this->file->getTempName()) . $this->file->getSize();
		}

		return parent::beforeValidate();
	}
}
