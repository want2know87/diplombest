<?php

class BackCallForm extends CFormModel
{
	public $name;
	public $phone;

	public function rules()
	{
		return array(
			array('name, phone', 'required'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'name'  => 'Имя',
			'phone' => 'Телефон',
		);
	}
}
