<?php

/**
 * This is the model class for table "disciplines".
 *
 * The followings are the available columns in table 'disciplines':
 * @property integer $id
 * @property string $name
 * @property integer $branch_id
 * @property boolean $visible
 * @property integer $priority
 * @property string $alias
 */
class Discipline extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'disciplines';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('alias', 'filter', 'filter'=>'trim'),
			array('name, branch_id, priority', 'required'),
			array('branch_id, priority', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('alias', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, branch_id, alias', 'safe', 'on'=>'search'),
			array('visible', 'boolean'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'branch' => array(self::BELONGS_TO, 'Branch', 'branch_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'branch_id' => 'Специализация',
			'visible' => 'Видимость',
			'priority' => 'Порядок',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('branch_id',$this->branch_id);
		$criteria->compare('visible',$this->visible);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('alias',$this->alias,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
			    'defaultOrder'=>'priority DESC',
		  	),
		));
	}

	public function getDisciplineAlias()
	{
		$alias = $this->alias;
		if (empty($alias)) {
			return 'discipline-' . $this->id;
		}
		return $alias;
	}

	public static function getBranchId($discipline) {
		$branch_id = null;
		$discipline = self::model()->findByAttributes(array('name' => $discipline));
		if ($discipline) {
			$branch_id = $discipline->branch_id;
		}
		return $branch_id;
	}

	public static function findAllByOrderType($order_type_id)
	{
		// $branches = Branch::model()->findAll('order_type_id=:order_type_id', array(':order_type_id'=>$order_type_id));
		$branches = Services::findBranchesByOrderType($order_type_id);
		$branchIds = array_map(function ($b) {
			return $b->id;
		}, $branches);
		$criteria = new CDbCriteria();
		$criteria->addInCondition("branch_id", $branchIds);
		return Discipline::model()->findAll($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Discipline the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
