<?php

/**
 * This is the model class for table "orderStatusState".
 *
 * The followings are the available columns in table 'orderStatusState':
 * @property integer $id
 * @property integer $order_id
 * @property string $context
 * @property integer $orderStatus_id
 */
class OrderStatusState extends CActiveRecord
{

	const GROUP_USER = 'user';
	const GROUP_MANAGER = 'manager';
	const GROUP_AUTHOR = 'author';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orderStatusState';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, context, orderStatus_id', 'required'),
			array('order_id, orderStatus_id', 'numerical', 'integerOnly'=>true),
			array('context', 'length', 'max'=>64),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, order_id, context, orderStatus_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'status' => array(self::HAS_ONE, 'OrderStatus', array('id' => 'orderStatus_id')),
			'operations' => array(self::HAS_MANY, 'OrderOperation', array('orderStatus_id' => 'orderStatus_id')),
			'userOperations' => array(self::HAS_MANY, 'OrderOperation', array('orderStatus_id' => 'orderStatus_id'), 'condition' => 'role=:role', 'params' => array(':role' => self::GROUP_USER)),
			'authorOperations' => array(self::HAS_MANY, 'OrderOperation', array('orderStatus_id' => 'orderStatus_id'), 'condition' => 'role=:role', 'params' => array(':role' => self::GROUP_AUTHOR)),
			'managerOperations' => array(self::HAS_MANY, 'OrderOperation', array('orderStatus_id' => 'orderStatus_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_id' => 'Order',
			'context' => 'Context',
			'orderStatus_id' => 'Статус заказа',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('context',$this->context,true);
		$criteria->compare('orderStatus_id',$this->orderStatus_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderStatusState the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function context($context) {
		if (OrderStatus::contextExists($context)) {
			$this->getDbCriteria()->mergeWith(array(
				'condition' => 't.context=:context',
				'params' => array(
					':context' => $context,
				),
			));
		}
		return $this;
	}
}
