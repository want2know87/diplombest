<?php

/**
 * This is the model class for table "seo_data".
 *
 * The followings are the available columns in table 'seo_data':
 * @property integer $id
 * @property string $url
 * @property string $content
 * @property string $block1
 * @property string $block2
 * @property string $block3
 * @property string $type
 */
class SeoData extends CActiveRecord
{
    const NEW_SCHOOL = 'new.school';
    const OLD_SCHOOL = 'old.school';

    public function tableName()
    {
        return 'seo_data';
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('url, content, block1, block2, block3, type', 'filter', 'filter'=>'trim'),
            array('url, type', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'url' => 'URL',
            'content' => 'Content',
            'block1' => 'Block 1',
            'block2' => 'Block 2',
            'block3' => 'Block 3',
            'type'   => 'Type',
        );
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('url',$this->url,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
