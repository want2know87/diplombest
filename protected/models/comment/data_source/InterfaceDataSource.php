<?php

/**
 * InterfaceDataSource - интерфейс для хранилеща данных, в моделях app\models\comments\data_source
 */
interface InterfaceDataSource
{
    /**
     * Добавление отзыва
     * @param string $message - сообщение
     * @param string $name - имя
     * @return array - добавленная запись ['id', 'content', 'name', 'date']
     */
    public function create($message,$name);
}
