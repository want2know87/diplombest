<?php


/**
 * DefaultBdDataSource - BD по умолчанию для хранилеща данных
 */
class DefaultBdDataSource extends CActiveRecord implements InterfaceDataSource
{
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comment';
	}
	
    public function create($message, $name) {
        // Вставляем в BD
        
        $this->name = $name;
        $this->content = $message;
        $this->date = time();
        $this->save();

        // Возврощаем вставленные данные
        return [
            'id' => Yii::app()->db->lastInsertID,
            'content' => $this->content,
            'name' => $this->name,
            'date' => $this->date
        ];
    }
}
