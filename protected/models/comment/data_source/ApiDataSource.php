<?php

/**
 * ApiDataSource - Api для хранилеща данных
 */
class ApiDataSource implements InterfaceDataSource
{
    public function create($message, $name) {
        return (new DefaultBdDataSource)->create($message, $name);
    }
}
