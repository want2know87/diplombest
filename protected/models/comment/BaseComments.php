<?php

/**
 * BaseComments - базовая модель для отзывов
 */
abstract class BaseComments
{
    /**
     * класс ApiDataSource, храниться информация о отзывах
     */
    private $data_source;


    /**
     * @param IDataSource $data_source - передаеться класс ApiDataSource, в нем храниться информация о отзывах
     */
    public function setSource(InterfaceDataSource $data_source)
    {
        $this->data_source = $data_source;
    }

    /**
     * Возврощает класс ApiDataSource, в нем храниться информация о отзывах
     */
    public function getSource()
    {
        return $this->data_source;
    }
}
