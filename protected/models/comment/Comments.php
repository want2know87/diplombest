<?php

/**
 * Comments - модель для отзывов
 */
class Comments extends BaseComments
{
    /** Добавление отзыва
     * @param string $message - сообщение
     * @param string $name - имя
     */
    public function create($message, $name)
    {
        return parent::getSource()->create($message, $name);
    }
}


