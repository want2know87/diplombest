<?php

/**
 * This is the model class for table "orderStatusLog".
 *
 * The followings are the available columns in table 'orderStatusLog':
 * @property integer $id
 * @property integer $order_id
 * @property integer $orderAction_id
 * @property integer $user_id
 * @property string $message
 * @property string $timestamp
 */
class OrderStatusLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orderStatusLog';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, context, user_id', 'required'),
			array('orderOperation_id, message', 'safe'),
			array('order_id, orderOperation_id, user_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, context, order_id, orderOperation_id, user_id, message, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
			'orderStatus' => array(self::HAS_ONE, 'OrderStatus', array('id' => 'orderStatus_id')),
			'orderOperation' => array(self::HAS_ONE, 'OrderOperation', array('id' => 'orderOperation_id')),
			'attachments' => array(self::HAS_MANY, 'OrderAttachment', array('orderStatusLog_id' => 'id'), 'group' => 'path'),
			'attachmentCount' => array(self::STAT, 'OrderAttachment', 'orderStatusLog_id', 'group' => 'path'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'context' => 'Контекст',
			'order_id' => 'Заказ',
			'orderOperation_id' => 'Операция',
			'user_id' => 'Пользователь',
			'message' => 'Сообщение',
			'timestamp' => 'Время',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('context',$this->context);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('orderOperation_id',$this->orderAction_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderStatusLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function forOrder(Order $order) {
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'t.order_id=:order_id',
			'params' => array(
				':order_id' => $order->id,
			),
			'order'=>'t.timestamp desc'
		));
		return $this;
	}

	public function forOrderUser(Order $order) {
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'t.order_id=:order_id AND t.context in (:work, :payment, :messages)',
			'params' => array(
				':order_id' => $order->id,
				':work' => OrderStatus::CONTEXT_WORK,
				':payment' => OrderStatus::CONTEXT_PAYMENT,
				':messages' => OrderStatus::CONTEXT_USER_MESSAGES,
			),
			'order'=>'t.timestamp desc'
		));
		return $this;
	}

	public function forOrderAuthor(Order $order) {
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'t.order_id=:order_id AND t.context in (:messages)',
			'params' => array(
				':order_id' => $order->id,
				':messages' => OrderStatus::CONTEXT_AUTHOR_MESSAGES,
			),
			'order'=>'t.timestamp desc'
		));
		return $this;
	}

	public function context($context) {
		if (OrderStatus::contextExists($context)) {
			$this->getDbCriteria()->mergeWith(array(
				'condition' => 't.context=:context',
				'params' => array(
					':context' => $context,
				),
			));
		}
		return $this;
	}

	protected function afterSave() {
		if ($this->isNewRecord) {
			$this->notify();
		}
		return parent::afterSave();
	}


	public function getAttachmentList() {
		$result = '';
	    foreach ($this->attachments as $attachment) {
			$result .= CHtml::tag(
				'li',
				array(), 
				CHtml::link(
					$attachment->name, 
					Yii::app()->createUrl(
						'//cabinet/file/download', 
						array(
							'path' => $attachment->path, 
							'name' => $attachment->name,
						)
					), 
					array('target' => '_blank')
				)
			);
	    }
		$result = CHtml::tag('ul', array('class' => 'attachments'), $result);
		return $result;
	}

	public function notify() {
		Yii::import('ext.yii-mail.*');
		$message = new YiiMailMessage('Заказ №' . $this->order_id, null, 'text/html', 'UTF-8');
		$message->from = Yii::app()->params['noreplyEmail'];
		if ($this->user_id != $this->order->manager_id && $this->order->manager->email) {
			$message->view = 'order-operation-manager';
			$message->addTo($this->order->manager->email);
			$message->setBody(array('model' => $this));
			Yii::app()->mail->send($message);
		} else if (in_array($this->context, array(OrderStatus::CONTEXT_WORK, OrderStatus::CONTEXT_PAYMENT, OrderStatus::CONTEXT_USER_MESSAGES))) {
		    if ($this->user_id != $this->order->user_id && $this->order->user->email) {
				$message->view = 'order-operation-user';
				$message->addTo($this->order->user->email);
				$message->setBody(array('model' => $this));
				Yii::app()->mail->send($message);
		    }
		} else if (in_array($this->context, array(OrderStatus::CONTEXT_AUTHOR_MESSAGES))) {
		    if ($this->user_id != $this->order->author_id && $this->order->author->email) {
				$message->view = 'order-operation-author';
				$message->addTo($this->order->author->email);
				$message->setBody(array('model' => $this));
				Yii::app()->mail->send($message);
		    }
		}
	}

}
