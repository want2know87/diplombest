<?php
//use app\models\Services;

class TrestController extends Controller
{
    public $layout = 'trest';
	/**
	 * Declares class-based actions.
	 */
	public $pageKeywords='Диплом на заказ написать ДипломБест';
	public $pageDescription='Диплом на заказ и другие виды студенческих работ, помощь в написании - ДипломБест';
	public $npTitle='Диплом на заказ, помощь в написании - ДипломБест!';
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Благодарим за ваш вопрос. В ближайшее время пришлем ответ на указанный e-mail.');
				$this->refresh();
			}
		} else {
		    if (!Yii::app()->user->isGuest) {
				$user = Yii::app()->user->getRecord();
		        $model->email = $user->email;
				$model->name  = $user->name;
		    }
		}
		$this->npTitle=Yii::app()->name . ' - Обратная связь';
		$this->render('contact',array('model'=>$model));
	}

	public function actionPage1()
	{
		$this->render('page1', array(
		));
	}

	public function actionPage2()
	{
		$this->render('page2');
	}

	public function actionPage3()
	{
		$this->render('page3');
	}
}
