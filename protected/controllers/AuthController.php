<?php

class AuthController extends Controller
{
	public $layout = 'main-nineseven';

    /**
	 * Declares class-based actions.
	 */
	public $pageKeywords='Диплом на заказ написать ДипломБест';
	public $pageDescription='Диплом на заказ и другие виды студенческих работ, помощь в написании - ДипломБест';
	public $npTitle='Диплом на заказ, помощь в написании - ДипломБест!';

	public function actionLogin()
	{
		//$model = new LoginForm;
		$model = new User(User::SCENARIO_LOGIN);

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		if (Yii::app()->request->isAjaxRequest) {
		    $this->renderPartial('login',array('model'=>$model));
		} else {
		    $this->render('login',array('model'=>$model));
		}
	}

	public function actionExternal()
	{
		$serviceName = Yii::app()->request->getQuery('service');
		if (isset($serviceName)) {
			/** @var $eauth EAuthServiceBase */
			Yii::app()->eauth->setRedirectView('redirect');
			$eauth = Yii::app()->eauth->getIdentity($serviceName);
			//$eauth->redirectUrl = Yii::app()->user->returnUrl;
			$eauth->redirectUrl = $this->createAbsoluteUrl('cabinet/default/index');
			$eauth->cancelUrl = $this->createAbsoluteUrl('auth/login');

			try {
				if ($eauth->authenticate()) {
					$eauth->getAttributes();
					$identity = new ExternalUserIdentity($eauth);

					// successful authentication
					if ($identity->authenticate()) {

						Yii::app()->user->login($identity);

						// Save the attributes to display it in layouts/main.php
						$session = Yii::app()->session;
						$session['eauth_profile'] = $eauth->attributes;

						// redirect and close the popup window if needed
						$eauth->redirect();
					}
					else {
						// close popup window and redirect to cancelUrl
						$eauth->cancel();
					}
				}

				// Something went wrong, redirect back to login page
				$this->redirect(array('auth/login'));
			}
			catch (EAuthException $e) {
				// save authentication error to session
				Yii::app()->user->setFlash('error', 'EAuthException: '.$e->getMessage());

				// close popup window and redirect to cancelUrl
				$eauth->redirect($eauth->getCancelUrl());
			}
		}

		$this->redirect(array('auth/login'));
		/*
		$model = new LoginForm;

		// display the login form
		if (Yii::app()->request->isAjaxRequest) {
		    $this->renderPartial('login',array('model'=>$model));
		} else {
		    $this->render('login',array('model'=>$model));
		}
		*/
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionRemind()
	{
		$model = new User(User::SCENARIO_REMIND);

		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			if ($model->validate()) {
				$token = new UserToken();
				$token->user_id = $model->id;
				if ($token->save()) {
					$token->refresh();
					//TODO: move notification to model
					Yii::import('ext.yii-mail.*');
					$message = new YiiMailMessage('Восстановление доступа к сайту', null, 'text/html', 'UTF-8');
					$message->view = 'auth-recover';
					$message->from = Yii::app()->params['adminEmail'];
					$message->addTo($model->email);
					$message->setBody(array('model' => $token));
					Yii::app()->mail->send($message);
					Yii::app()->user->setFlash('success', 'Сообщение отправлено.');
					$this->refresh();
				} else {
					Yii::app()->user->setFlash('error', 'Произошла ошибка');
				}
			}
		}
		$this->render('remind', array('model' => $model));
	}


	public function actionRecover()
	{
		$token = UserToken::model()->findByAttributes(array('token' => Yii::app()->request->getParam('token')));
		if ($token) {
			$model = $token->user;
			if (!$token->isValid) {
				$model->setScenario(User::SCENARIO_REMIND);
				Yii::app()->user->setFlash('error', 'Ссылка для восстановления пароля действует 3 часа. Эта ссылка устарела. Попробуйте получить новую ссылку.');
				$this->render('remind', array('model' => $model));
			} else {
				$model->setScenario(User::SCENARIO_RECOVER);
				$model->token = $token->token;
				if (isset($_POST['User'])) {
				    $model->attributes = $_POST['User'];
					if ($model->save()) {
					    Yii::app()->user->setFlash('success', 'Пароль изменен.');
						$model->silentLogin();
						$this->refresh();
					} else {
					    Yii::app()->user->setFlash('error', 'Пароль не был изменен.');
					}
				}
				$this->render('recover', array('model' => $model, 'token' => $token->token));
			}
		} else {
		    throw new CHttpException(404, 'Not found');
		}
	}

	public function actionChangePassword()
	{
		if (!Yii::app()->user->isGuest) {
			$model = Yii::app()->user->getRecord();
			$model->setScenario(User::SCENARIO_CHANGE_PASSWORD);
			if (isset($_POST['User'])) {
				$model->attributes = $_POST['User'];
				if ($model->save()) {
					Yii::app()->user->setFlash('success', 'Пароль изменен.');
				}
			}
			$this->render('changePassword', array('model' => $model));
		} else {
		    throw new CHttpException(403, 'Forbidden');
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
