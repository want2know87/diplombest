<?php

class OrderController extends Controller
{
	public $layout = 'main-nineseven';

	public $pageKeywords='Диплом на заказ написать ДипломБест';
	public $pageDescription='Диплом на заказ и другие виды студенческих работ. Качественно, без плагиата.';
	public $npTitle='Диплом на заказ, помощь в написании - ДипломБест!';
	public function actionAdd()
	{
	   // Атрибуты для модели
	   $attributes = [];

	   // Закачанные файлы
	   $uploadFil = [];

	   $uploadFil = HelpersMain::uploadFil(
	        [
	            'upload_file' => $_FILES['pictures']['tmp_name'],
	            'name_file' => $_FILES['pictures']['name'],
	            'type_file' => $_FILES['pictures']['type']
	        ],
	        'images/order_add_foto/',
	        [
	        	'image/jpeg',
	        	'application/pdf',
	        	'application/vnd.oasis.opendocument.text',
	        	'text/plain',
	        	'image/png',
	        	'image/gif',
	        	'application/msword',
	        	'application/vnd.ms-excel',
	        	'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
	        	'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        	]
	    );

		$model = new Order('add');
		if (isset($_POST['Order']))
		{
		    $attributes = $_POST['Order'];
		    $attributes['uploadFiles'] = implode(',',$uploadFil);

			$model->attributes = $attributes;
			if ($model->save()) {
				$model->refresh();
				Yii::app()->user->setFlash('success', 'Заказ успешно создан.');
				$this->render('add', array('model' => $model));
			} else {
				$this->render('form', array('model' => $model));
			}
		} else {
			$this->render('form', array('model' => $model));
		}
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionUpdate()
	{
		$this->render('update');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/

	public function actionPromo($type)
	{
		$model = OrderType::model()->findByAttributes(array('alias' => $type));
		if($model===null) throw new CHttpException(404,'The requested operation does not exist.');
		$order = new Order('add');
		$order->orderType_id = $model->id;
		$this->banner = null;
		switch ($model->alias) {
		    case 'diplom':
			$this->npTitle='Сколько стоит заказать дипломную работу, купить дипломную работу в Москве недорого';
			$this->pageKeywords='Заказать дипломную работу недорого';
			$this->pageDescription='Написание дипломной работы на заказ в Москве, Санкт-Петербурге, более 1000 авторов преподавателей российских ВУЗов, полное сопровождение до защиты, низкие цены.';
			break;
		    case 'dissertacia':
			$this->npTitle='Стоимость диссертации на заказ, кандидатская, докторская, магистерская диссертация под ключ';
			$this->pageKeywords='Заказать диссертацию';
			$this->pageDescription='Написание диссертаций на заказ, низкие цены. Редактирование, исправление и доработки бесплатно. Гарантии, официальный договор. Более 1000 авторов преподавателей российских ВУЗов.';
			break;
		    case 'prakticheskoe-zadanie':
			$this->npTitle='Практическое задание на заказ!';
			$this->pageKeywords='Практическое задание на заказ';
			$this->pageDescription='Практическое задание на заказ - решаем любые задачи, сроки от 3 часов. Звоните';
			break;
		    case 'referat':
			$this->npTitle='Заказать реферат онлайн недорого, стоимость реферата на заказ, цены';
			$this->pageKeywords='Заказать реферат недорого онлайн';
			$this->pageDescription='Предлагаем услуги написания реферата на заказ, гарантия, официальный договор, низкие цены. Более 1000 профессиональных авторов преподавателей российских ВУЗов.';
			break;
		    case 'otchet-po-praktike':
			$this->npTitle='Заказать отчет по практике в Москве, отчет по научно исследовательской практике по доступной цене';
			$this->pageKeywords='Заказать отчет по практике срочно недорого';
			$this->pageDescription='Написание отчета по научно исследовательской практике на заказ в Москве. Низкие цены, профессиональные авторы, полное сопровождение работы и последующие доработки бесплатно.';
			break;
		    case 'kursovaya':
			$this->npTitle='Купить курсовую работу на заказ, заказать курсовую работу на сайте';
			$this->pageKeywords='Заказать курсовую работу недорого цена';
			$this->pageDescription='Сайт курсовых работ на заказ, квалифицированные авторы, официальный договор на выполнение курсовой работы, все дополнительные исправления бесплатно.';
			break;
			case 'part-diploma':
			$this->npTitle='Заказать часть дипломной работы в Москве';
			$this->pageKeywords='Заказать часть дипломной работы недорого цена';
			$this->pageDescription='Возможность заказать написание части дипломной работы в Москве, низкие цены, более 1000 профессиональных авторов, гарантия, официальный договор.';
			break;
		    default:
		    }
		$this->render('promo', array('model' => $model, 'order' => $order));
	}
}
