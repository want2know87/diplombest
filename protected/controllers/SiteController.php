<?php
//use app\models\Services;

class SiteController extends Controller
{
	public $layout = 'main-nineseven';

	/**
	 * Declares class-based actions.
	 */
	public $pageKeywords='Диплом на заказ написать ДипломБест';
	public $pageDescription='Выполнение студенческих работ от реферата до диплома на заказ, более 1000 авторов преподавателей российских ВУЗов, полное сопровождение до защиты, низкие цены.';
	public $npTitle='Заказать выполнение реферата, диплома, студенческих работ';
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
	    /*
	    $info=var_export('11',true);
        $file=fopen("/var/www/images/order_add_foto/base.txt", "w");  // открываем w - разрешаем запись
        fwrite($file, $info);       // записываем в файл
        fclose($file);    // закрываем
        */

		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->npTitle='Заказать выполнение реферата, диплома, студенческих работ';
		$this->pageKeywords='Диплом на заказ написать ДипломБест';
		$this->pageDescription='Выполнение студенческих работ от реферата до диплома на заказ, более 1000 авторов преподавателей российских ВУЗов, полное сопровождение до защиты, низкие цены.';

		//Отзывы
		$comment = Yii::app()->createController('comment');

		$orderTypes = OrderType::model()->base()->onMain()->findAll();
	    $price_of_services = $this->renderPartial('price-of-services', [
	        'services' => $orderTypes,
	        'services_all_boolean' => true,

	    ], true);

		$this->render('index', [
		    'services' => $price_of_services,
		    'comment' => $comment[0]->actionViewSelectBd()
		]);
	}

	/**
     * Скачать файл
     * @param string $path - путь к файлу
     */
    public function actionDownloadFile($path)
    {
        HelpersMain::downloadFile($path);
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$this->banner = null;
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Благодарим за ваш вопрос. В ближайшее время пришлем ответ на указанный e-mail.');
				$this->refresh();
			}
		} else {
		    if (!Yii::app()->user->isGuest) {
				$user = Yii::app()->user->getRecord();
		        $model->email = $user->email;
				$model->name  = $user->name;
		    }
		}
		$this->npTitle=Yii::app()->name . ' - Обратная связь';
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the contact page
	 */
	public function actionAuthors()
	{
		$this->banner = '/images/authors.jpg';

		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Благодарим за ваш вопрос. В ближайшее время пришлем ответ на указанный e-mail.');
				$this->refresh();
			}
		} else {
		    if (!Yii::app()->user->isGuest) {
				$user = Yii::app()->user->getRecord();
		        $model->email = $user->email;
				$model->name  = $user->name;
		    }
		}
		$this->npTitle=Yii::app()->name . ' - Авторам';
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Страница услуги
	 */
    public function actionServices()
	{
	    $price_of_services = $this->renderPartial('price-of-services', [
	        'services' => (new Services)->servicesAll()
	    ], true);

        $this->render('services', array('content' => $price_of_services));
	}

	public function actionAjaxservices()
	{
	    $this->renderPartial('price-of-services', [
	        'services' => (new Services)->servicesAll()
	    ], false, true);
	}
}
