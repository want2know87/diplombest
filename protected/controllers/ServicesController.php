<?php

class ServicesController extends Controller
{
    public $layout = 'main-nineseven';

    public $pageKeywords='Диплом на заказ написать ДипломБест';
    public $pageDescription='Диплом на заказ и другие виды студенческих работ, помощь в написании - ДипломБест';
    public $npTitle='Диплом на заказ, помощь в написании - ДипломБест!';

    protected function requestBackCall()
    {
        $form=new BackCallForm;
        if(isset($_POST['BackCallForm']))
        {
            $form->attributes=$_POST['BackCallForm'];
            if($form->validate())
            {
                $name=$form->name;
                $subject='=?UTF-8?B?'.base64_encode('Обратный звонок').'?=';
                $body = <<<BODY
Поступил заказ на обратный звонок.
Имя: {$name}
Номер: {$form->phone}
BODY
;
                mail(Yii::app()->params['adminEmail'], $subject, $body);
                Yii::app()->user->setFlash('success','Заявка принята. Перезвоним в ближайшее время.');
                $this->refresh();
            }
        }
        return $form;
    }

    public function actionIndex($order_type_alias)
    {
        $form = $this->requestBackCall();
        $orderType = $this->findOrderType($order_type_alias);
        $this->render('index', array(
            'orderType'     => $orderType,
            'branches'      => $this->findBranchesByOrderType($orderType->id),
            'backcallModel' => $form,
        ));
    }

    public function actionBranch($order_type_alias, $branch_id)
    {
        $form = $this->requestBackCall();
        $orderType = $this->findOrderType($order_type_alias);
        $branch = $this->findBranch($branch_id);
        $disciplines = $this->findDisciplinesByBranch($branch->id);
        $this->render('branch', array(
            'orderType'      => $orderType,
            'branch'         => $branch,
            'disciplines'    => $disciplines,
            'backcallModel'  => $form,
        ));
    }


    public function actionDiscipline($order_type_alias, $branch_id, $discipline_id)
    {
        $form = $this->requestBackCall();
        $orderType = $this->findOrderType($order_type_alias);
        $branch = $this->findBranch($branch_id);
        $discipline = $this->findDiscipline($discipline_id);
        $this->render('discipline', array(
            'orderType'      => $orderType,
            'branch'         => $branch,
            'discipline'     => $discipline,
            'backcallModel'  => $form,
        ));
    }

    protected function findDiscipline($discipline_id)
    {
        if (strpos($discipline_id, 'discipline-') === 0) {
            $discipline_id = str_replace('discipline-', '', $discipline_id);
            $model = Discipline::model()->findByPk($discipline_id);
        } else {
            $model = Discipline::model()->find('alias=:discipline_alias', array(':discipline_alias'=>$discipline_id));
        }
        if($model===null)
            throw new CHttpException(404);
        return $model;
    }

    protected function findDisciplinesByBranch($branch_id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'branch_id=:branch_id AND visible=:visible';
        $criteria->order = 't.priority desc';
        $criteria->params = array(':branch_id' => $branch_id, ':visible' => 1);
        return Discipline::model()->findAll($criteria);
    }

    protected function findBranchesByOrderType($order_type_id)
    {
        return Services::findBranchesByOrderType($order_type_id);
    }

    protected function findBranch($branch_id)
    {
        if (strpos($branch_id, 'branch-') === 0) {
            $branch_id = str_replace('branch-', '', $branch_id);
            $model = Branch::model()->findByPk($branch_id);
        } else {
            $model = Branch::model()->find('alias=:branch_alias', array(':branch_alias'=>$branch_id));
        }
        if($model===null)
            throw new CHttpException(404);
        return $model;
    }

    protected function findOrderType($order_type_alias)
    {
        $model=OrderType::model()->find('alias=:order_type_alias', array(':order_type_alias'=>$order_type_alias));
        if($model===null)
            throw new CHttpException(404);
        return $model;
    }
}
