<?php
/**
 * Отзывы
 */
class CommentController extends Controller
{
    /**
     * Добавление отзыва, через ajax
     * @param array $_POST - ['name'] - имя, ['content'] - описание
     */
	public function actionCreateAjax()
	{
	    $name = $_POST['name'];
	    $content = $_POST['content'];
	    
		$model = new Comments;
        $model->setSource(new ApiDataSource);
        $create = [(object) $model->create($content, $name)];
        echo json_encode(['comment' => $this->actionViewHtml( $create )]);
	}
	
	/**
     * Вывод отзывов, выбирая из BD
     */
	public function actionViewSelectBd()
	{
	    return $this->actionViewHtml(Comment::model()->findAll(array('order'=>'date DESC')));
	}
	
	/**
     * Вывод отзывов html
     * @param array $data - массив с данными
     */
	private function actionViewHtml($data)
	{
	    return $this->renderPartial('//comment/view', [
	        'comment' => $data
	    ], true);
	}
}