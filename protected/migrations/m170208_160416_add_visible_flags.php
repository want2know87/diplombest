<?php

class m170208_160416_add_visible_flags extends CDbMigration
{
	public function up()
	{
		$this->addColumn('orderTypes',  'visible', "TinyInt( 1 ) NOT NULL DEFAULT '1'");
		$this->addColumn('branches',    'visible', "TinyInt( 1 ) NOT NULL DEFAULT '1'");
		$this->addColumn('disciplines', 'visible', "TinyInt( 1 ) NOT NULL DEFAULT '1'");
	}

	public function down()
	{
		$this->dropColumn('orderTypes',  'visible');
		$this->dropColumn('branches',    'visible');
		$this->dropColumn('disciplines', 'visible');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
