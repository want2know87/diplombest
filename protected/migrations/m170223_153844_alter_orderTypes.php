<?php

class m170223_153844_alter_orderTypes extends CDbMigration
{
	public function up()
	{
		$this->renameColumn('orderTypes', 'visible', 'is_primary');
		$this->addColumn('orderTypes',  'on_main', "TinyInt( 1 ) NOT NULL DEFAULT '0'");
	}

	public function down()
	{
		$this->renameColumn('orderTypes', 'is_primary', 'visible');
		$this->dropColumn('orderTypes',  'on_main');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
