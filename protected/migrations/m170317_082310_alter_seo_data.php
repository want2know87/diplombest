<?php

class m170317_082310_alter_seo_data extends CDbMigration
{
	public function up()
	{
		$this->addColumn('seo_data',  'type', "string NOT NULL");
		$this->addColumn('seo_data',  'block1', "text");
		$this->addColumn('seo_data',  'block2', "text");
		$this->addColumn('seo_data',  'block3', "text");
	}

	public function down()
	{
		$this->dropColumn('seo_data',  'type');
		$this->dropColumn('seo_data',  'block1');
		$this->dropColumn('seo_data',  'block2');
		$this->dropColumn('seo_data',  'block3');
	}
}
