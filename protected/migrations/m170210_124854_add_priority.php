<?php

class m170210_124854_add_priority extends CDbMigration
{
	public function up()
	{
		$this->addColumn('orderTypes',  'priority', "Int( 11 ) NOT NULL DEFAULT '0'");
		$this->addColumn('branches',    'priority', "Int( 11 ) NOT NULL DEFAULT '0'");
		$this->addColumn('disciplines', 'priority', "Int( 11 ) NOT NULL DEFAULT '0'");
	}

	public function down()
	{
		$this->dropColumn('orderTypes',  'priority');
		$this->dropColumn('branches',    'priority');
		$this->dropColumn('disciplines', 'priority');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
