<?php

class m170307_073007_seo_data_init extends CDbMigration
{
	public function up()
	{
		$this->createTable('seo_data', array(
            'id'      => 'pk',
            'url'     => 'string NOT NULL',
            'content' => 'text',
        ));
	}

	public function down()
	{
		$this->dropTable('seo_data');
	}

}
