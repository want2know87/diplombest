<?php

class m170227_164745_unlink_order_type_and_branch extends CDbMigration
{
	public function up()
	{
		$this->dropForeignKey('fk_branches-order_type_id', 'branches');
		$this->dropColumn('branches',  'order_type_id');
	}

	public function down()
	{
		$this->addColumn('branches',  'order_type_id', "INT ( 11 )");
		$this->addForeignKey('fk_branches-order_type_id', 'branches', 'order_type_id', 'orderTypes', 'id', 'CASCADE');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
