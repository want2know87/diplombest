<?php

class m170210_113811_alter_branches_table extends CDbMigration
{
	public function up()
	{
		$this->addColumn('branches',  'order_type_id', "INT ( 11 )");
		$this->addForeignKey('fk_branches-order_type_id', 'branches', 'order_type_id', 'orderTypes', 'id', 'CASCADE');
	}

	public function down()
	{
		$this->dropForeignKey('fk_branches-order_type_id', 'branches');
		$this->dropColumn('branches',  'order_type_id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
