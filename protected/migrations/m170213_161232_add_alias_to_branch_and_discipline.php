<?php

class m170213_161232_add_alias_to_branch_and_discipline extends CDbMigration
{
	public function up()
	{
		$this->addColumn('branches',  'alias', "VarChar( 50 )");
		$this->addColumn('disciplines',  'alias', "VarChar( 50 )");
	}

	public function down()
	{
		$this->dropColumn('branches',  'alias');
		$this->dropColumn('disciplines',  'alias');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
