<?php
/* @var $this OrderTypeController */
/* @var $model OrderType */

$this->breadcrumbs=array(
	'Branches'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Branch', 'url'=>array('index')),
	array('label'=>'Create Branch', 'url'=>array('create')),
	array('label'=>'View Branch', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Branch', 'url'=>array('admin')),
);
?>

<h1>Update Branch <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'orderTypeList'=>$orderTypeList)); ?>
