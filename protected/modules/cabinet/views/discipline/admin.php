<?php
/* @var $this OrderTypeController */
/* @var $model OrderType */

$this->breadcrumbs=array(
	'Disciplines'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Disciplines', 'url'=>array('index')),
	array('label'=>'Create Discipline', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#order-type-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Discipline</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'discipline-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'visible',
			'value'=>'$data->visible ? "Да" : "Нет"',
			'filter'=>array(0=>'Нет', 1 => 'Да'),
		),
		'id',
		'name',
		array(
			'name'=>'branch_id',
			'value'=>'$data->branch ? $data->branch->name : null',
			'filter'=>$branchList,
		),
		'alias',
		'priority',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
