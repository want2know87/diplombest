<?php 

$grid = $this->widget(
	'bootstrap.widgets.TbGridView',
	array(
		'dataProvider'=>$provider,
		'columns'=>array(
			array(
				'name' => 'user_id',
				'htmlOptions'=>array('class'=>'text-right span1'),
				'value' => '$data->user->username',
			),
			array(
				'name' => 'message',
			),
			array(
				'type' => 'raw',
				'header' => '<i class="icon-paperclip"></i>',
				'value' => '$data->getAttachmentList()',
			),
			array(
				'name' => 'timestamp',
				'type' => 'datetime',
			),
		),
	)
);

?>

<div class="row">
<div class="span6">
<?php

if (!isset($submitText)) {
    $submitText = 'Отправить';
}

$form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm', 
	array(
		'action' => $this->createUrl('order/message', array('id' => $message->order_id)),
		'enableAjaxValidation' => false,
		'htmlOptions' => array(
			'class' => 'order-message-form',
			'enctype' => 'multipart/form-data',
			'data-update-grid' => $grid->getId(),
		),
	)
);
?>

<?php

echo $form->errorSummary($message);

echo $form->hiddenField($message, 'order_id');

if (Yii::app()->user->checkAccess('manager')) {
	echo $form->hiddenField($message, 'context');
}

echo $form->textAreaRow(
	$message, 
	'message',
	array(
		'class' => 'span6',
		'rows' => 5,
	)
);
?>
<div class="buttons">
	<?php echo CHtml::submitButton($submitText, array('class' => 'btn btn-primary pull-right')); ?>
<?php

Yii::import("xupload.models.XUploadForm");
$this->widget('ext.xupload.XUpload', array(
	'url' => $this->createUrl('order/attach', array('id' => $message->order_id)),
	//'model' => new XUploadForm,
	'model' => new OrderAttachment,
	'attribute' => 'file',
	'multiple' => true,
	'previewImages' => false,
	'autoUpload' => true,
	'imageProcessing' => false,
	'showForm' => false,
	'formView' => 'files-form',
	'downloadView' => 'files-download',
	'uploadView' => 'files-upload',
	'htmlOptions' => array(
		'id' => $form->getId(),
	),
));

Yii::app()->getClientScript()->registerScript('order-attachment', "
$('.order-message-form').on('submit', function(e) {
	e.preventDefault();
	$.fn.yiiGridView.update(
		$(this).attr('data-update-grid'),
		{
			url: $(this).attr('action'),
			type: 'POST',
			data: $(this).serializeArray()
		}
	);
	$(this).trigger('reset').find('input[name=\"attachment[]\"]').remove();
	$(this).find('.files').empty();
}).on('fileuploadadd', function(e, data) {
	$(this).find('input[type=submit]').prop('disabled', true);
}).on('fileuploaddone', function(e, data) {
	for (var i in data.result) {
		$(this).append($('<input type=\"hidden\" name=\"attachment[]\">').attr('data-url', data.result[i].delete_url).val(data.result[i].id));
	}
}).on('fileuploaddestroy', function(e, data) {
	$(this).find('input[data-url=\"' + data.url + '\"]').remove();
}).on('fileuploadalways', function(e, data) {
	$(this).find('input[type=submit]').prop('disabled', false);
});
");

?>
</div>


<?php $this->endWidget(); ?>
</div>
<div class="span4">
</div>
</div>