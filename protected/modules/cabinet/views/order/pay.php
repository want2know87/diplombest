<div class="modal-title"><h3>Выберите способ оплаты</h3><p>Не забудьте после оплаты сообщить о ней, это ускорит работу</p></div>
<?php

$cs = Yii::app()->clientScript;
//Yii::app()->bootstrap->registerPackage('bootstrap.js');

$this->widget(
	'ext.yii-booster.widgets.TbMenu',
	array(
		'type' => 'pills',
		'items' => array(
			array('label' => 'Яндекс-деньги', 'url' => '#ym', 'active' => true, 'linkOptions' => array('data-toggle' => 'tab')),
			array('label' => 'Webmoney', 'url' => '#wm', 'linkOptions' => array('data-toggle' => 'tab')),
			array('label' => 'Банк', 'url' => '#bank', 'linkOptions' => array('data-toggle' => 'tab')),
			//array('label' => 'Салоны связи', 'url' => '#telecom', 'linkOptions' => array('data-toggle' => 'tab')),
			//array('label' => 'Мобильные операторы', 'url' => '#mobile', 'linkOptions' => array('data-toggle' => 'tab')),
			//array('label' => 'Электронные деньги', 'url' => '#emoney', 'linkOptions' => array('data-toggle' => 'tab')),
			//array('label' => 'Терминалы, банкоматы', 'url' => '#terminal', 'linkOptions' => array('data-toggle' => 'tab')),
			//array('label' => 'Денежные переводы', 'url' => '#transfer', 'linkOptions' => array('data-toggle' => 'tab')),
		),
	)
);

//CVarDumper::dump($tab);
?>
<div class="tab-content">
	<div id="ym" class="tab-pane fade active in">
		<p>Номер нашего кошелька: <?=Yii::app()->config->get('payment.yandexmoney')?>.</p>
		<p>Вы можете отправить деньги через <a href="https://money.yandex.ru/doc.xml?id=522781" target="_blank">терминал</a>, <a href="https://money.yandex.ru/doc.xml?id=522870" target="_blank">банкомат</a>, <a href="https://money.yandex.ru/doc.xml?id=242314" target="_blank">интернет-банкинг</a> или <a href="https://money.yandex.ru/doc.xml?id=523896" target="_blank">офисы продаж</a>.</p>
		<p>Если у вас уже есть кошелёк Яндекс.Денег, воспользуйтесь <a href="https://money.yandex.ru/direct-payment.xml" target="_blank">специальной формой</a>.</p>
		<p>Для ускорения обработки платежа укажите комментарий: «Оплата по договору №<?=$model->id?> от&nbsp;<?=Yii::app()->format->formatDate($model->createdOn, 'short')?>».</p>
	</div>
	<div id="wm" class="tab-pane fade">
		<p>Номер нашего кошелька: <?=Yii::app()->config->get('payment.webmoney')?>.</p>
		<p>Для ускорения обработки платежа укажите комментарий: «Оплата по договору №<?=$model->id?> от&nbsp;<?=Yii::app()->format->formatDate($model->createdOn, 'short')?>».</p>
	</div>
	<div id="bank" class="tab-pane fade">
	<?php /*$this->widget(
		'ext.yii-booster.widgets.TbButton', 
		array(
			'label' => 'Получить квитанцию',
			'url' => $this->createUrl('bank', array('id' => $model->id)),
			'htmlOptions' => array('class' => 'btn-primary', 'target' => '_blank'),
		)
	);*/ ?>
	<?php
	$message = new OrderStatusLog;
	$message->order_id = $model->id;
	$message->context = OrderStatus::CONTEXT_USER_MESSAGES;
	$message->user_id = Yii::app()->user->getId();
	$message->message = 'Я хочу оплатить через банк, пришлите мне реквизиты.';

	$form = $this->beginWidget(
		'bootstrap.widgets.TbActiveForm', 
		array(
			'action' => $this->createUrl('order/message', array('id' => $message->order_id)),
			'enableAjaxValidation' => false,
			'htmlOptions' => array(
				'class' => 'order-message-form',
			),
		)
	);
	echo $form->errorSummary($message);

	echo $form->hiddenField($message, 'order_id');

	if (Yii::app()->user->checkAccess('manager')) {
		echo $form->hiddenField($message, 'context');
	}

	echo $form->textAreaRow(
		$message, 
		'message',
		array(
			//'class' => 'span6',
			'rows' => 5,
		)
	);
	?>
	<div class="buttons">
		<?php echo CHtml::submitButton('Отправить', array('class' => 'btn btn-primary'));?>
	</div>
	<?php $this->endWidget();?>
	</div>
</div>
<?php

/*
$this->widget(
    'ext.yii-booster.widgets.TbTabs',
    array(
        'type' => 'pills', // 'tabs' or 'pills'
        'tabs' => array(
            array(
                'label' => 'Банк',
                'content' => 'Сбербанк, Альфа-банк, ВТБ-24',
                'active' => true
            ),
            array('label' => 'Салоны связи', 'content' => 'Евросеть, Связной'),
            array('label' => 'Электронные деньги', 'content' => 'Яндекс-деньги, Webmoney'),
            array('label' => 'Терминалы, банкоматы', 'content' => 'Profile Content'),
            array('label' => 'Денежные переводы', 'content' => 'Contact, UniStream, Город'),
        ),
    )
);
*/