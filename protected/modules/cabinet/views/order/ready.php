<div class="modal-title">
<h1><?=$preview ? 'Готов, доступен для ознакомления' : 'Готовая работа доступна'?></h1>
</div>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'order-preview-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php 

echo $form->select2Row($model, $preview ? 'previewAttachments' : 'finalAttachments', array(
	'data' => CHtml::listData($model->attachments, 'id', 'name'),
	'multiple' => true,
));


?>

<?php 

if ($preview) {
	echo $form->textFieldRow($model,'price',array('class'=>'span5','maxlength'=>14)); 
	echo $form->textFieldRow($model,'requestedSum',array('class'=>'span5','maxlength'=>14, 'value' => $model->price - $model->paidSum)); 
}

?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Сохранить',
		)); ?>
</div>

<?php $this->endWidget(); ?>
