<?php
$this->beginWidget(
    'bootstrap.widgets.TbBox',
    array(
		'title' => sprintf('<i class="icon-%s"></i> ', $data->branch ? $data->branch->icon : 'question-sign') . CHtml::link(CHtml::encode($data->theme),array('view','id'=>$data->id)),
		'htmlOptions' => array('class' => '')
    ))
?>
<div class="view1">
		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderType_id')); ?>:</b>
	<?php echo CHtml::encode($data->orderType->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_id')); ?>:</b>
	<?php echo CHtml::encode($data->branch ? $data->branch->name : '<не определена>'); ?>
	<br />

	<b><?php echo 'Статус'; ?>:</b>
	<?php if (!$data->workState->status) {
	    //$data->initializeState();
	}?>
	<?php echo CHtml::encode($data->workState->status->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discipline')); ?>:</b>
	<?php echo CHtml::encode($data->discipline); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('theme')); ?>:</b>
	<?php echo CHtml::encode($data->theme); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo Yii::app()->dateFormatter->formatDateTime($data->date, 'long', null); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('extra')); ?>:</b>
	<?php echo CHtml::encode($data->extra); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	*/ ?>

</div>
<?php

$this->endWidget();
