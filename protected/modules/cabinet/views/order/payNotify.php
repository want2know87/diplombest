<?php

?>

<div class="modal-title">
<h3>Сообщить об оплате по заказу <?=$model->id;?></h3>
</div>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'order-notify-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($payment); ?>

<?php echo $form->dropdownListRow($payment,'paymentType',OrderPayment::getTypes(),array('class'=>'')); ?>

<?php echo $form->datetimepickerRow(
		$payment,
		'paymentDate',
		array(
				'options' => array(
				'title' => 'Когда была произведена оплата',
				'language' => 'ru',
				'format' => 'dd.mm.yyyy hh:ii',
				'weekStart' => 1,
				'beforeShowDay' => 'js:function(date) {return date.valueOf() > (new Date()).valueOf() ? \'disabled\' : \'\';}',
			),
			'append' => '<label for="' . CHtml::activeId($payment, 'paymentDate') . '"><i class="icon-calendar"></i></label>',
			//'class' => 'input-small',
		)
	); ?>

<?php echo $form->textFieldRow($payment,'sum',array('class'=>'','maxlength'=>14)); ?>

<?php echo $form->textFieldRow($payment,'sumTotal',array('class'=>'','maxlength'=>14)); ?>

<?php echo $form->textareaRow($payment,'comment',array('class'=>'')); ?>

<div class="buttons">
<?php

Yii::import("xupload.models.XUploadForm");
$this->widget('ext.xupload.XUpload', array(
	'url' => $this->createUrl('order/attach', array('id' => $model->id)),
	//'model' => new XUploadForm,
	'model' => new OrderAttachment,
	'attribute' => 'file',
	'multiple' => false,
	'previewImages' => false,
	'autoUpload' => true,
	'imageProcessing' => false,
	'showForm' => false,
	'formView' => 'files-form-check',
	'downloadView' => 'files-download',
	'uploadView' => 'files-upload',
	'htmlOptions' => array(
		'id' => $form->getId(),
	),
));

Yii::app()->getClientScript()->registerScript('order-attachment', "
$('#{$form->getId()}').on('submit', function(e) {
	/*e.preventDefault();
	$.fn.yiiGridView.update(
		$(this).attr('data-update-grid'),
		{
			url: $(this).attr('action'),
			type: 'POST',
			data: $(this).serializeArray()
		}
	);
	$(this).trigger('reset').find('input[name=\"attachment[]\"]').remove();
	$(this).find('.files').empty();*/
}).on('fileuploadadd', function(e, data) {
	$(this).find('input[type=submit]').prop('disabled', true);
}).on('fileuploaddone', function(e, data) {
	for (var i in data.result) {
		$(this).append($('<input type=\"hidden\" name=\"attachment[]\">').attr('data-url', data.result[i].delete_url).val(data.result[i].id));
	}
}).on('fileuploaddestroy', function(e, data) {
	$(this).find('input[data-url=\"' + data.url + '\"]').remove();
}).on('fileuploadalways', function(e, data) {
	$(this).find('input[type=submit]').prop('disabled', false);
});
");

?>
</div>

<div class="form-actions modal-footer1">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Отправить',
		)); ?>
</div>

<?php $this->endWidget(); ?>
