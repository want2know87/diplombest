<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'order-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

	<?php echo $form->dropDownListRow(
        $model,
        'orderType_id',
        CHtml::listData(OrderType::model()->findAll(), 'id', 'name'),//CHtml::listData($orderTypes, 'id', 'name'),
		array(
			'empty' => '',
			'class' => 'worktype',
			'onchange' => CJavaScript::encode("js:\$(this).closest('form').find('label[for=" . CHtml::activeId($model, 'extra') . "] span.required').toggle($(this).val() == '0');"),
			'encode' => false,
			'message' => 'Необходимо указать тип'
		)
    );
	?>

	<?php echo $form->dropDownListRow(
        $model,
        'branch_id',
        CHtml::listData(Branch::model()->findAll(), 'id', 'name')
    );
	?>

	<?php 
	echo $form->typeAheadRow(
		$model,
		'discipline',
		array(
			'source' => array_keys(CHtml::listData(Discipline::model()->findAll(), 'name', 'name')),
		)
	);
	?>
	<?php echo $form->textFieldRow($model,'theme',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'extra',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->datepickerRow(
			$model,
			'date',
			array(
					'options' => array(
					'title' => 'Дата сдачи работы влияет на стоимость',
					'language' => 'ru',
					'format' => 'dd.mm.yyyy',
					'weekStart' => 1,
					'beforeShowDay' => 'js:function(date) {return date.valueOf() < (new Date()).valueOf() ? \'disabled\' : \'\';}',
				),
				'append' => '<label for="' . CHtml::activeId($model, 'date') . '"><i class="icon-calendar"></i></label>',
				'class' => 'input-small',
			)
		); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'price',array('class'=>'span5','maxlength'=>14)); ?>

	<?php echo $form->textFieldRow($model,'paidSum',array('class'=>'span5','maxlength'=>14)); ?>

	<?php echo $form->textFieldRow($model,'requestedSum',array('class'=>'span5','maxlength'=>14)); ?>

	<?php echo $form->textFieldRow($model,'authorSum',array('class'=>'span5','maxlength'=>14)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
