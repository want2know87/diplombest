<?php
$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id,
);

// файлы в заказе
$uploadFiles = preg_replace('/(.*)/us', '<div>'.CHtml::link('$1', Yii::app()->createUrl('site/downloadfile?path=images/order_add_foto/$1')) . '</div>', explode(',' , $model->uploadFiles), 1);
$uploadFiles = implode('',$uploadFiles);

$this->menu=array(
array('label'=>'Список заказов','url'=>array('index')),
array('label'=>'Создать заказ','url'=>array('create')),
array('label'=>'Изменить заказ','url'=>array('update','id'=>$model->id),'visible'=>Yii::app()->user->checkAccess('manager')),
array('label'=>'Удалить заказ','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены, что хотите удалить этот заказ?'),'visible'=>Yii::app()->user->checkAccess('manager')),
array('label'=>'Управление заказами','url'=>array('admin'),'visible'=>Yii::app()->user->checkAccess('manager')),
);

//var_dump(Yii::app()->user->checkAccess('viewOwnOrders',array('order' => $model)));

Yii::app()->getClientScript()->registerScript('modal-remote', "
$('#modal').on('hidden', function() {
    $(this).removeData('modal');
});
");

Yii::app()->getClientScript()->registerScript('order-operations', "
$(function() {
	$('.order-operations a').click(function(e) {
		\$this = $(this);
		if (\$this.attr('data-action')) {
			\$this.closest('.order-operations').attr('action', \$this.attr('data-action')).submit();
		}
	});
});
");

$this->beginClip('orderAttachments');
if ($model->checkAccess('managedOrder')) {
	if ($model->attachments) {
		echo '<ul class="attachments">';
		foreach ($model->attachments as $attachment) {
			echo '<li>' . CHtml::link($attachment->name, Yii::app()->createUrl('cabinet/file/download') . '/' . $attachment->path . '/' . $attachment->name, array('target' => '_blank')) . '</li>';
		}
		echo '</ul>';
	}
} else if ($model->checkAccess('myOrder')) {  
	if ($model->userAttachments) {
		echo '<ul class="attachments">';
		foreach ($model->userAttachments as $attachment) {
			echo '<li>' . CHtml::link($attachment->name, Yii::app()->createUrl('cabinet/file/download') . '/' . $attachment->path . '/' . $attachment->name, array('target' => '_blank')) . '</li>';
		}
		echo '</ul>';
	}
} else if ($model->checkAccess('authoredOrder')) {
	if ($model->authorAttachments) {
		echo '<ul class="attachments">';
		foreach ($model->authorAttachments as $attachment) {
			echo '<li>' . CHtml::link($attachment->name, Yii::app()->createUrl('cabinet/file/download') . '/' . $attachment->path . '/' . $attachment->name, array('target' => '_blank')) . '</li>';
		}
		echo '</ul>';
	}
}
$this->endClip('orderAttachments');

?>


<h1>Заказ № <?php echo $model->id; ?></h1>

<?php 

$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true,
    'fade' => true,
    'closeText' => '&times;',
    'events' => array(),
    'htmlOptions' => array(),
    'userComponentId' => 'user',
));

if ($model->finalAttachments) {
    $this->beginWidget(
		'bootstrap.widgets.TbBox',
		array(
			'title' => 'Готовая работа',
			'headerIcon' => 'icon-download-alt',
		)
	);
	echo '<ul class="attachments">';
	foreach ($model->finalAttachments as $attachment) {
		//echo '<li>' . CHtml::link($attachment->name, Yii::app()->createUrl('cabinet/file/download') . '/' . $attachment->path . '/' . $attachment->name, array('target' => '_blank')) . '</li>';
		echo '<li>' . CHtml::link($attachment->name, Yii::app()->createUrl('cabinet/file/download', array('path' => $attachment->path, 'name' => $attachment->name)), array('target' => '_blank')) . '</li>';
	}
	echo '</ul>';
	$this->endWidget();
} else if ($model->previewAttachments) {
    $this->beginWidget(
		'bootstrap.widgets.TbBox',
		array(
			'title' => 'Предварительная версия работы',
			'headerIcon' => 'icon-search',
		)
	);
	echo '<ul class="attachments">';
	foreach ($model->previewAttachments as $attachment) {
		//echo '<li>' . CHtml::link($attachment->name, Yii::app()->createUrl('cabinet/file/download') . '/' . $attachment->path . '/' . $attachment->name, array('target' => '_blank')) . '</li>';
		echo '<li>' . CHtml::link($attachment->name, Yii::app()->createUrl('cabinet/file/download', array('path' => $attachment->path, 'name' => $attachment->name)), array('target' => '_blank')) . '</li>';
	}
	echo '</ul>';
	$this->endWidget(); 
}

$this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		array(
			'name' => 'user.username',
			'visible' => Yii::app()->user->checkAccess('managedOrder', array('order' => $model)),
		),
		array(
			'name' => 'user.email',
			'visible' => Yii::app()->user->checkAccess('managedOrder', array('order' => $model)),
		),
		array(
			'name' => 'phone',
			'visible' => Yii::app()->user->checkAccess('managedOrder', array('order' => $model)),
		),
		array(
			'name' => 'workState.status.name',
		),
		array(
			'name' => 'workState.status.description',
			'type' => 'html',
		),
		'orderType.name',
		'branch.name',
		'discipline',
		'theme',
		'extra',
		array(
			'name' => 'date',
			'type' => 'date',
		),
		array(
			'name' => 'uploadFiles',
			'value'=> $uploadFiles,
			'type' => 'html',
			'label' => 'Прикрепленные файлы'
		),
		array(
			'name' => 'authorSum',
			'visible' => Yii::app()->user->checkAccess('authoredOrder', array('order' => $model)) ||
						 Yii::app()->user->checkAccess('managedOrder', array('order' => $model))
		),
),
)); 
?>

<form class="order-operations" method="post">
<?php
$operations = $model->getAllowedOperations(OrderStatus::CONTEXT_WORK);
if ($operations):
?>
<?php
foreach ($operations as $operation):
?>

<?php
$options = array();
if ($operation->action) {
    $options['data-toggle'] = 'modal';
	$options['data-target'] = '#modal';
    $options['href'] = $this->createUrl('order/perform', array('id' => $model->id, 'operation' => $operation->id));
} else {
    $options['data-action'] = $this->createUrl('order/perform', array('id' => $model->id, 'operation' => $operation->id));
}
$this->widget(
	'bootstrap.widgets.TbButton',
	array(
		'label' => $operation->name,
		'htmlOptions' => $options,
		'icon' => $operation->icon,
	)
);
?>
<?php
endforeach;
endif;

$operations = $model->getAllowedOperations(OrderStatus::CONTEXT_PAYMENT);
?>
<h2>Оплата</h2>
<?php 
$this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		array(
			'name' => 'paymentState.status.name',
		),
		array(
			'name' => 'paymentState.status.description',
			'type' => 'html',
		),
		array(
			'name' => 'price',
			'visible' => (bool) $model->price,
		),
		array(
			'name' => 'requestedSum',
			'htmlOptions' => array('class' => 'warning'),
			'visible' => (bool) $model->requestedSum,
			'type' => 'raw',
			'value' => '<span class="badge badge-important">!</span> ' . $model->requestedSum,
		),
		array(
			'name' => 'paidSum',
			'visible' => (bool) $model->price,
		),
		array(
			'name' => 'leftSum',
			'type' => 'raw',
			'visible' => (bool) $model->price,
			'value' => $model->price - $model->paidSum,
		),
),
));

?>
<form class="order-operations" method="post">
<?php
foreach ($operations as $operation):
?>

<?php
$options = array();
if ($operation->action) {
    $options['data-toggle'] = 'modal';
	$options['data-target'] = '#modal';
    $options['href'] = $this->createUrl('order/perform', array('id' => $model->id, 'operation' => $operation->id));
} else {
    $options['data-action'] = $this->createUrl('order/perform', array('id' => $model->id, 'operation' => $operation->id));
}
$this->widget(
	'bootstrap.widgets.TbButton',
	array(
		'label' => $operation->name,
		'htmlOptions' => $options,
		'icon' => $operation->icon,
	)
);
?>
<?php
endforeach;
?>
</form>

<?php 

if ($model->checkAccess('myOrder') || $model->checkAccess('managedOrder')) {
	$grid = $this->widget(
		'bootstrap.widgets.TbGridView',
		array(
			'dataProvider'=>new CActiveDataProvider(OrderPayment::model()->forOrder($model)),
			'columns'=>array(
				array(
					'name' => 'paymentType',
					'value' => '$data->typeName()',
				),
				array(
					'name' => 'sum',
				),
				array(
					'name' => 'sumTotal',
				),
				array(
					'name' => 'paymentStatus',
					'value' => '$data->statusName()',
				),
				/*array(
					'type' => 'raw',
					'header' => '<i class="icon-paperclip"></i>',
					'value' => '$data->getAttachmentList()',
				),*/
				array(
					'name' => 'paymentDate',
					'type' => 'datetime',
				),
			),
		)
	);
}

$message = new OrderStatusLog;
$message->order_id = $model->id;
$message->user_id = Yii::app()->user->getId();

if ($model->checkAccess('managedOrder')) {
	$message->context = OrderStatus::CONTEXT_USER_MESSAGES;
	echo '<h2>Взаимодействие с клиентом</h2>';
	echo $this->renderPartial(
		'history', 
		array(
			'provider' => new CActiveDataProvider(OrderStatusLog::model()->forOrderUser($model)),
			'message' => $message,
			'submitText' => 'Отправить сообщение клиенту',
		)
	);

	$message->context = OrderStatus::CONTEXT_AUTHOR_MESSAGES;
	echo '<h2>Взаимодействие с автором</h2>';
	echo $this->renderPartial(
		'history', 
		array(
			'provider' => new CActiveDataProvider(OrderStatusLog::model()->forOrderAuthor($model)),
			'message' => $message,
			'submitText' => 'Отправить сообщение автору',
		)
	);
} else if ($model->checkAccess('myOrder')) {
	$message->context = OrderStatus::CONTEXT_USER_MESSAGES;
	echo '<h2>История операций по заказу</h2>';
	echo $this->renderPartial(
		'history', 
		array(
			'provider' => new CActiveDataProvider(OrderStatusLog::model()->forOrderUser($model)),
			'message' => $message,
		)
	);
} else if ($model->checkAccess('authoredOrder')) {
	$message->context = OrderStatus::CONTEXT_AUTHOR_MESSAGES;
	echo '<h2>История операций по заказу</h2>';
	echo $this->renderPartial(
		'history', 
		array(
			'provider' => new CActiveDataProvider(OrderStatusLog::model()->forOrderAuthor($model)),
			'message' => $message,
		)
	);
}

?>


