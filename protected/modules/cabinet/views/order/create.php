<?php
$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'Список заказов','url'=>array('index')),
array('label'=>'Управление заказами','url'=>array('admin'),'visible'=>Yii::app()->user->checkAccess('manager')),
);
?>

<h1>Создать заказ</h1>

<?php 

if (Yii::app()->user->checkAccess('manager')) {
	echo $this->renderPartial(
		'_form', 
		array(
			'model'=>$model, 
		)
	);
} else {
	$this->renderPartial('//order/form', array('model' => new Order('add')));
}

?>