<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'orderType_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'branch_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'discipline',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'theme',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textAreaRow($model,'extra',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'price',array('class'=>'span5','maxlength'=>14)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
