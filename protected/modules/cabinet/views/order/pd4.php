<table border="0" cellspacing="0" cellpadding="0" style="width:180mm; height:145mm;">
<tbody><tr valign="top">
	<td style="width:50mm; height:70mm; border:1pt solid #000000; border-bottom:none; border-right:none;" align="center">
	<b>Извещение</b><br>
	<font style="font-size:53mm">&nbsp;<br></font>
	<b>Кассир</b>
	</td>
	<td style="border:1pt solid #000000; border-bottom:none;" align="center">
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td align="right"><small><i>Форма № ПД-4</i></small></td>
			</tr>
			<tr>
				<td style="border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.recipient')?></td>
			</tr>
			<tr>
				<td align="center"><small>(наименование получателя платежа)</small></td>
			</tr>
		</tbody></table>

		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td style="width:37mm; border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.inn')?></td>
				<td style="width:9mm;">&nbsp;</td>
				<td style="border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.rs')?></td>
			</tr>
			<tr>
				<td align="center"><small>(ИНН получателя платежа)</small></td>
				<td><small>&nbsp;</small></td>
				<td align="center"><small>(номер счета получателя платежа)</small></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td>в&nbsp;</td>
				<td style="width:73mm; border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.name')?></td>
				<td align="right">БИК&nbsp;&nbsp;</td>
				<td style="width:33mm; border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.bik')?></td>
			</tr>
			<tr>
				<td></td>
				<td align="center"><small>(наименование банка получателя платежа)</small></td>
				<td></td>
				<td></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td width="1%" nowrap="">Номер кор./сч. банка получателя платежа&nbsp;&nbsp;</td>
				<td width="100%" style="border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.ks')?></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td style="width:60mm; border-bottom:1pt solid #000000;">Оплата за посреднические услуги по предоставлению информации по договору №<?=$model->id?> от&nbsp;<?=Yii::app()->format->formatDate($model->createdOn, 'short')?></td>
				<td style="width:2mm;">&nbsp;</td>
				<td style="border-bottom:1pt solid #000000;">&nbsp;</td>
			</tr>
			<tr>
				<td align="center"><small>(наименование платежа)</small></td>
				<td><small>&nbsp;</small></td>
				<td align="center"><small>(номер лицевого счета (код) плательщика)</small></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td width="1%" nowrap="">Ф.И.О. плательщика&nbsp;&nbsp;</td>
				<td width="100%" style="border-bottom:1pt solid #000000;"></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td width="1%" nowrap="">Адрес плательщика&nbsp;&nbsp;</td>
				<td width="100%" style="border-bottom:1pt solid #000000;">&nbsp;</td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td>Сумма платежа&nbsp;<font style="text-decoration:underline;">&nbsp;<?=$model->requestedSum?>&nbsp;</font>&nbsp;руб.&nbsp;<font style="text-decoration:underline;">&nbsp;&nbsp;</font>&nbsp;коп.</td>
				<td align="right">&nbsp;&nbsp;Сумма платы за услуги&nbsp;&nbsp;_____&nbsp;руб.&nbsp;____&nbsp;коп.</td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td>Итого&nbsp;&nbsp;_______&nbsp;руб.&nbsp;____&nbsp;коп.</td>
				<td align="right">&nbsp;&nbsp;«______»________________ 20____ г.</td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td><small>С условиями приема указанной в платежном документе суммы, 
				в т.ч. с суммой взимаемой платы за услуги банка, ознакомлен и согласен.</small></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td align="right"><b>Подпись плательщика _____________________</b></td>
			</tr>
		</tbody></table>
	</td>
</tr>



<tr valign="top">
	<td style="width:50mm; height:70mm; border:1pt solid #000000; border-right:none;" align="center">
	<b>Извещение</b><br>
	<font style="font-size:53mm">&nbsp;<br></font>
	<b>Кассир</b>
	</td>
	<td style="border:1pt solid #000000;" align="center">
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td align="right"><small><i>Форма № ПД-4</i></small></td>
			</tr>
			<tr>
				<td style="border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.recipient')?></td>
			</tr>
			<tr>
				<td align="center"><small>(наименование получателя платежа)</small></td>
			</tr>
		</tbody></table>

		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td style="width:37mm; border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.inn')?></td>
				<td style="width:9mm;">&nbsp;</td>
				<td style="border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.rs')?></td>
			</tr>
			<tr>
				<td align="center"><small>(ИНН получателя платежа)</small></td>
				<td><small>&nbsp;</small></td>
				<td align="center"><small>(номер счета получателя платежа)</small></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td>в&nbsp;</td>
				<td style="width:73mm; border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.name')?></td>
				<td align="right">БИК&nbsp;&nbsp;</td>
				<td style="width:33mm; border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.bik')?></td>
			</tr>
			<tr>
				<td></td>
				<td align="center"><small>(наименование банка получателя платежа)</small></td>
				<td></td>
				<td></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td width="1%" nowrap="">Номер кор./сч. банка получателя платежа&nbsp;&nbsp;</td>
				<td width="100%" style="border-bottom:1pt solid #000000;"><?=Yii::app()->config->get('payment.bank.ks')?></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td style="width:60mm; border-bottom:1pt solid #000000;">Оплата за посреднические услуги по предоставлению информации по договору №<?=$model->id?> от&nbsp;<?=Yii::app()->format->formatDate($model->createdOn, 'short')?></td>
				<td style="width:2mm;">&nbsp;</td>
				<td style="border-bottom:1pt solid #000000;">&nbsp;</td>
			</tr>
			<tr>
				<td align="center"><small>(наименование платежа)</small></td>
				<td><small>&nbsp;</small></td>
				<td align="center"><small>(номер лицевого счета (код) плательщика)</small></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td width="1%" nowrap="">Ф.И.О. плательщика&nbsp;&nbsp;</td>
				<td width="100%" style="border-bottom:1pt solid #000000;"></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td width="1%" nowrap="">Адрес плательщика&nbsp;&nbsp;</td>
				<td width="100%" style="border-bottom:1pt solid #000000;">&nbsp;</td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td>Сумма платежа&nbsp;<font style="text-decoration:underline;">&nbsp;<?=$model->requestedSum?>&nbsp;</font>&nbsp;руб.&nbsp;<font style="text-decoration:underline;">&nbsp;&nbsp;</font>&nbsp;коп.</td>
				<td align="right">&nbsp;&nbsp;Сумма платы за услуги&nbsp;&nbsp;_____&nbsp;руб.&nbsp;____&nbsp;коп.</td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td>Итого&nbsp;&nbsp;_______&nbsp;руб.&nbsp;____&nbsp;коп.</td>
				<td align="right">&nbsp;&nbsp;«______»________________ 20____ г.</td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td><small>С условиями приема указанной в платежном документе суммы, 
				в т.ч. с суммой взимаемой платы за услуги банка, ознакомлен и согласен.</small></td>
			</tr>
		</tbody></table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:122mm; margin-top:3pt;">
			<tbody><tr>
				<td align="right"><b>Подпись плательщика _____________________</b></td>
			</tr>
		</tbody></table>
	</td>
</tr>
</tbody></table>