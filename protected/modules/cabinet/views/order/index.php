<?php
$this->breadcrumbs=array(
	'Orders',
);

$this->menu=array(
array('label'=>'Создать заказ','url'=>array('create')),
array('label'=>'Управление заказами','url'=>array('admin'),'visible'=>Yii::app()->user->checkAccess('manager')),
);
?>

<h1>Список заказов</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
