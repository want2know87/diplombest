<?php
$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'Список заказов','url'=>array('index')),
	array('label'=>'Создать заказ','url'=>array('create')),
	array('label'=>'Просмотреть заказ','url'=>array('view','id'=>$model->id)),
	array('label'=>'Управление заказами','url'=>array('admin')),
	);

?>

<div class="modal-title">
<h3>Оценить заказ №<?=$model->id;?></h3>
</div>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'order-evaluate-form',
	'enableAjaxValidation'=>false,
)); ?>

<?=$form->errorSummary($model)?>

<?=$form->textFieldRow($model,'price',array('class'=>'span5','maxlength'=>14))?>

<?=$form->textFieldRow($model,'requestedSum',array('class'=>'span5','maxlength'=>14))?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Сохранить',
		)); ?>
</div>

<?php $this->endWidget(); ?>
