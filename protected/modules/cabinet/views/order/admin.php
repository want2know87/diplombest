<?php
$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'Список заказов','url'=>array('index')),
array('label'=>'Создать заказ','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('order-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Управление заказами</h1>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php /*$this->renderPartial('_search',array(
	'model'=>$model,
));*/ ?>
</div><!-- search-form -->

<?php 

$branches = CHtml::listData(Branch::model()->findAll(), 'id', 'name');
$orderStatuses = CHtml::listData(OrderStatus::model()->context(OrderStatus::CONTEXT_WORK)->findAll(), 'id', 'name');

$this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'order-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
				'name' => 'id',
				'htmlOptions'=>array('class'=>'text-right span1'),
		),
		'user.username',
		array(
			'name' => 'orderType_id',
			'type' => 'raw',
			'filter' => CHtml::listData(OrderType::model()->findAll(), 'id', 'name'),
			'value' => '$data->orderType->name',
		),
		array(
			'name' => 'branch_id',
			'filter' => $branches,
			'value' => '$data->branch ? $data->branch->name : "<не определена>"',
			'class' => 'ext.yii-booster.widgets.TbEditableColumn',
			'editable' => array(
				'url' => $this->createUrl('//cabinet/update/order'),
				'type' => 'select',
				'source' => $branches,
			),
		),
		array(
			'name' => 'workState.orderStatus_id',
			'filter' => $orderStatuses,
			'value' => '$data->workState->status->name',
			'class' => 'ext.yii-booster.widgets.TbEditableColumn',
			'editable' => array(
				'url' => $this->createUrl('//cabinet/update/order'),
				'type' => 'select',
				'source' => $orderStatuses,
			),
		),
		'discipline',
		array(
			'name' => 'price',
			'class' => 'ext.yii-booster.widgets.TbEditableColumn',
			'editable' => array(
				'url' => $this->createUrl('//cabinet/update/order'),
			),
		),
		array(
			'name' => 'date',
			'type' => 'date',
		),
		/*
		'theme',
		'extra',
		'phone',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'htmlOptions'=>array('class'=>'span2'),
		),
),
)); ?>
