<?php

?>

<div class="modal-title">
<h3>Подтвердить платеж по заказу <?=$model->id;?></h3>
</div>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'order-evaluate-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php //echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model,'price',array('class'=>'span5','maxlength'=>14)); ?>

<?php echo $form->textFieldRow($model,'paidSum',array('class'=>'span5','maxlength'=>14, 'hint' => $model->paidSum ? '+' . $model->paidSum : null)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Подтвердить',
		)); ?>
</div>

<?php $this->endWidget(); ?>
