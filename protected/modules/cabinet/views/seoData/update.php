<?php
$this->breadcrumbs=array(
    'Update',
);

$this->menu=array(
    array('label'=>'Create Seo Text for new pages', 'url'=>array('create', 'type'=>SeoData::NEW_SCHOOL)),
    array('label'=>'Create Seo Text for old pages', 'url'=>array('create', 'type'=>SeoData::OLD_SCHOOL)),
    array('label'=>'Manage Seo Text', 'url'=>array('admin')),
);
?>

<h1>Update Seo Text <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
