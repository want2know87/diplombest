<?php

$this->breadcrumbs=array(
    'Seo Texts'=>array('index'),
    'Create',
);

$this->menu=array(
    array('label'=>'Manage Seo Text', 'url'=>array('admin')),
);
?>

<h1>Create Seo Text</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
