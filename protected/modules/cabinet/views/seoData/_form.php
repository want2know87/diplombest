<?php

$cs = Yii::app()->clientScript;
$cs->registerScriptFile('https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js', CClientScript::POS_END);
$cs->registerScript(
'ckeditor'
, <<<JS
function registerCkeditor(name) {
    if ($('[name="' + name + '"]').length) {
        CKEDITOR.replace( name );
    }
}

registerCkeditor('SeoData[content]');
registerCkeditor('SeoData[block1]');
registerCkeditor('SeoData[block2]');
registerCkeditor('SeoData[block3]');
// CKEDITOR.replace( 'SeoData[content]' );
// CKEDITOR.replace( 'SeoData[block1]' );
// CKEDITOR.replace( 'SeoData[block2]' );
// CKEDITOR.replace( 'SeoData[block3]' );
JS
, CClientScript::POS_END
);
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'branch-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'id'); ?>
        <?php echo $form->textField($model,'id', array('readonly' => true)); ?>
        <?php echo $form->error($model,'id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'url'); ?>
        <?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'url'); ?>
    </div>

    <?php if ($model->type == SeoData::NEW_SCHOOL): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'content'); ?>
        <?php echo $form->textarea($model,'content',array('rows'=>5)); ?>
        <?php echo $form->error($model,'content'); ?>
    </div>
    <?php endif; ?>

    <?php if ($model->type == SeoData::OLD_SCHOOL): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'block1'); ?>
        <?php echo $form->textarea($model,'block1',array('rows'=>5)); ?>
        <?php echo $form->error($model,'block1'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'block2'); ?>
        <?php echo $form->textarea($model,'block2',array('rows'=>5)); ?>
        <?php echo $form->error($model,'block2'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'block3'); ?>
        <?php echo $form->textarea($model,'block3',array('rows'=>5)); ?>
        <?php echo $form->error($model,'block3'); ?>
    </div>
    <?php endif; ?>

    <div class="row buttons" style="margin-top: 10px;">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
