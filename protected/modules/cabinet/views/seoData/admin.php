<?php

$this->breadcrumbs=array(
    'Seo Texts'=>array('index'),
    'Manage',
);

$this->menu=array(
    array('label'=>'Create Seo Text for new pages', 'url'=>array('create', 'type'=>SeoData::NEW_SCHOOL)),
    array('label'=>'Create Seo Text for old pages', 'url'=>array('create', 'type'=>SeoData::OLD_SCHOOL)),
);

?>

<h1>Manage Seo Text</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'branch-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'url',
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>
