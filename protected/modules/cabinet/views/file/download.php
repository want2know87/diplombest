<?php
/* @var $this FileController */

$realpath = $file->getRealPath();

if (file_exists($realpath)) {
    header('Content-Description: File Transfer');
    header('Content-Type: ' . $file->mimeType);
    header('Content-Disposition: attachment; filename=' . $file->name);
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . $file->size);
    ob_clean();
    flush();
    readfile($realpath);
    exit;
} else {
	echo 'file not exists ' . $realpath;
}