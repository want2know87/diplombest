<?php

$this->breadcrumbs=array(
    'Seo Texts'=>array('index'),
    'Manage',
);
?>

<h1>Manage Seo Text</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'branch-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'url',
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>
