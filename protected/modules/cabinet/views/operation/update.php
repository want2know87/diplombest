<?php
$this->breadcrumbs=array(
	'Order Operations'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List OrderOperation','url'=>array('index')),
	array('label'=>'Create OrderOperation','url'=>array('create')),
	array('label'=>'View OrderOperation','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage OrderOperation','url'=>array('admin')),
	);
	?>

	<h1>Update OrderOperation <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>