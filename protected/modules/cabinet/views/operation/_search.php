<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'context',array('class'=>'span5','maxlength'=>64)); ?>

		<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'comment',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'orderStatus_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'nextOrderStatus_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'role',array('class'=>'span5','maxlength'=>64)); ?>

		<?php echo $form->textFieldRow($model,'icon',array('class'=>'span5','maxlength'=>50)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
