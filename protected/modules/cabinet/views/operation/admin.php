<?php
$this->breadcrumbs=array(
	'Order Operations'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List OrderOperation','url'=>array('index')),
array('label'=>'Create OrderOperation','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('order-operation-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Order Operations</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'order-operation-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'name' => 'context',
			'filter' => OrderStatus::getContexts(),
			'value' => 'OrderStatus::getContextName($data->context)',
		),
		'name',
		'comment',
		array(
			'name' => 'orderStatus_id',
			'value' => '$data->status->name',
		),
		array(
			'name' => 'nextOrderStatus_id',
			'value' => '$data->nextStatus->name',
		),
		'role',
		array(
			'name' => 'icon',
			'type' => 'html',
			'filter' => false,
			'value' => '"<i class=\\"icon-{$data->icon}\\" title=\\"{$data->icon}\\"></i>"',
		),
		/*
		'icon',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
