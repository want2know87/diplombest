<?php
$this->breadcrumbs=array(
	'Order Operations',
);

$this->menu=array(
array('label'=>'Create OrderOperation','url'=>array('create')),
array('label'=>'Manage OrderOperation','url'=>array('admin')),
);
?>

<h1>Order Operations</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
