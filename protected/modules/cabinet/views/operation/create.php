<?php
$this->breadcrumbs=array(
	'Order Operations'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List OrderOperation','url'=>array('index')),
array('label'=>'Manage OrderOperation','url'=>array('admin')),
);
?>

<h1>Create OrderOperation</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>