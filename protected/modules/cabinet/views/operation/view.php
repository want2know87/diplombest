<?php
$this->breadcrumbs=array(
	'Order Operations'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List OrderOperation','url'=>array('index')),
array('label'=>'Create OrderOperation','url'=>array('create')),
array('label'=>'Update OrderOperation','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete OrderOperation','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage OrderOperation','url'=>array('admin')),
);
?>

<h1>View OrderOperation #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'context',
		'name',
		'comment',
		'orderStatus_id',
		'nextOrderStatus_id',
		'role',
		'icon',
),
)); ?>
