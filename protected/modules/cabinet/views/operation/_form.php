<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'order-operation-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropdownListRow($model,'context',OrderStatus::getContexts(),array('class'=>'span5','maxlength'=>64)); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'comment',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->dropdownListRow($model,'orderStatus_id',CHtml::listData(OrderStatus::model()->findAll(), 'id', 'name'),array('class'=>'span5')); ?>

	<?php echo $form->dropdownListRow($model,'nextOrderStatus_id',CHtml::listData(OrderStatus::model()->findAll(), 'id', 'name'),array('class'=>'span5')); ?>

	<?php echo $form->dropdownListRow($model,'role',CHtml::listData(AuthItem::model()->roles()->findAll(), 'name', 'description'),array('class'=>'span5','maxlength'=>64)); ?>

	<?php echo $form->textFieldRow($model,'action',array('class'=>'span5','maxlength'=>64)); ?>

	<?php echo $form->textFieldRow($model,'icon',array('class'=>'span5','maxlength'=>50)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
