<?php
$this->breadcrumbs=array(
	'Configs'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('config-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Ведение настроек</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'config-grid',
'dataProvider'=>$model->search(),
'columns'=>array(
		'id',
		//'param',
		'label',
		array(
			'name' => 'value',
			'class' => 'ext.yii-booster.widgets.TbEditableColumn',
			'editable' => array(
				'url' => $this->createUrl('//cabinet/update/config'),
				'type' => 'text',
			),
		),
),
)); ?>
