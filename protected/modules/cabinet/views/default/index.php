<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<?php

$services = Yii::app()->eauth->getServices();
$user = Yii::app()->user->getRecord();

$this->widget('AuthWidget', array('action' => '//auth/external'));

if ($user->accounts) {
	$registeredAccountCss = array();
	foreach ($user->accounts as $account) {
		$registeredAccountCss[] = '.auth-link.' . $account->service;
	}
    Yii::app()->clientScript->registerScript('auth-filter', sprintf("$('%s').addClass('disabled').click(function(e) {e.preventDefault();});", implode(', ', $registeredAccountCss)));
}


?>
<!-- <ul class="unstyled auth-services">
<?php foreach ($services as $service): ?>
	<li class="auth-service <?=$service->id?>">
	<?php if ($account = $user->account($service->id)): ?>
		<a class="btn btn-large disabled" href="#">
			<i class="icon-ok icon-large"></i> <?=$service->title?>
		</a>
	<?php else: ?>
		<a href="<?=$this->createUrl('auth/external', array('service' => $service->id))?>" class="auth-link <?=$service->id?>">
			<span class="auth-icon <?=$service->id?>"><i></i></span>
			<span class="auth-title"><?=$service->title?></span>
		</a>
	<?php endif; ?>
	</li>
<?php endforeach; ?>
</ul> -->