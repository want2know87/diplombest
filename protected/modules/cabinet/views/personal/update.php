<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	);
	?>

	<h1>Изменить личные данные</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>