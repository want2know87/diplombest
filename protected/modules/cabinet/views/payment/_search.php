<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'order_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'manager_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'paymentType',array('class'=>'span5','maxlength'=>8)); ?>

		<?php echo $form->textFieldRow($model,'sum',array('class'=>'span5','maxlength'=>11)); ?>

		<?php echo $form->textFieldRow($model,'sumTotal',array('class'=>'span5','maxlength'=>11)); ?>

		<?php echo $form->textFieldRow($model,'sumConfirmed',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'paymentStatus',array('class'=>'span5','maxlength'=>9)); ?>

		<?php echo $form->textFieldRow($model,'paymentDate',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'comment',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'createdOn',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updatedOn',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
