<?php
$this->breadcrumbs=array(
	'Order Payments',
);

$this->menu=array(
array('label'=>'Create OrderPayment','url'=>array('create')),
array('label'=>'Manage OrderPayment','url'=>array('admin')),
);
?>

<h1>Order Payments</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
