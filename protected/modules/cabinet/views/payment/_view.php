<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_id')); ?>:</b>
	<?php echo CHtml::encode($data->order_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manager_id')); ?>:</b>
	<?php echo CHtml::encode($data->manager_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paymentType')); ?>:</b>
	<?php echo CHtml::encode($data->paymentType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sum')); ?>:</b>
	<?php echo CHtml::encode($data->sum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sumTotal')); ?>:</b>
	<?php echo CHtml::encode($data->sumTotal); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sumConfirmed')); ?>:</b>
	<?php echo CHtml::encode($data->sumConfirmed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paymentStatus')); ?>:</b>
	<?php echo CHtml::encode($data->paymentStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paymentDate')); ?>:</b>
	<?php echo CHtml::encode($data->paymentDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
	<?php echo CHtml::encode($data->comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdOn')); ?>:</b>
	<?php echo CHtml::encode($data->createdOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->updatedOn); ?>
	<br />

	*/ ?>

</div>