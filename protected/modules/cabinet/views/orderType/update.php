<?php
/* @var $this OrderTypeController */
/* @var $model OrderType */

$this->breadcrumbs=array(
	'Order Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OrderType', 'url'=>array('index')),
	array('label'=>'Create OrderType', 'url'=>array('create')),
	array('label'=>'View OrderType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OrderType', 'url'=>array('admin')),
);
?>

<h1>Update OrderType <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>