<?php
/* @var $this OrderTypeController */
/* @var $model OrderType */

$this->breadcrumbs=array(
	'Order Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List OrderType', 'url'=>array('index')),
	array('label'=>'Create OrderType', 'url'=>array('create')),
	array('label'=>'Update OrderType', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OrderType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OrderType', 'url'=>array('admin')),
);
?>

<h1>View OrderType #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'title',
		'descr',
		'pages',
		'parent_id',
		'alias',
	),
)); ?>
