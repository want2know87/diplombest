<?php
/* @var $this OrderTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Order Types',
);

$this->menu=array(
	array('label'=>'Create OrderType', 'url'=>array('create')),
	array('label'=>'Manage OrderType', 'url'=>array('admin')),
);
?>

<h1>Order Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
