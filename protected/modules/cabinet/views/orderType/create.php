<?php
/* @var $this OrderTypeController */
/* @var $model OrderType */

$this->breadcrumbs=array(
	'Order Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OrderType', 'url'=>array('index')),
	array('label'=>'Manage OrderType', 'url'=>array('admin')),
);
?>

<h1>Create OrderType</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>