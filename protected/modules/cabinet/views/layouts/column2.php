<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
<div class="row">
<div class="span12">
		<?php
			$this->widget('bootstrap.widgets.TbMenu', array(
					'items'=>array(
						'home' => array('label' => '�������', 'url'=>array('default/index'), 'icon' => 'home'),
						'order' => array('label' => '������', 'url'=>array('order/index'), 'icon' => 'home'),
						'payment' => array('label'=>'�������', 'url'=>array('payment/index')),
						'personal' => array('label'=>'������ ������', 'url'=>array('default/personal')),
						'users' => array('label'=>'������������', 'url'=>array('/site/page', 'view'=>'scheme'), 'visible'=>!Yii::app()->user->checkAccess('adminActions')),
					),
					'type'=>'tabs',
					'stacked' => false,
					'htmlOptions' => array(
						//'data-spy' => 'affix',
						//'data-offset-top' => '90',
					),
				)
			);
		?>
</div>
</div>
</div>
<div class="container">
<div class="row">
	<div class="span9">
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
	<div class="span3">
		<div id="sidebar">
		<?php
			$this->widget('bootstrap.widgets.TbMenu', array(
					'items'=>$this->menu,
					'type'=>'tabs',
					'stacked' => true,
					'htmlOptions' => array(
						//'data-spy' => 'affix',
						//'data-offset-top' => '90',
					),
				)
			);
		?>
		</div><!-- sidebar -->
		<div>
		<?php if(!empty($this->clips['orderAttachments'])) echo $this->clips['orderAttachments']?>
		</div>
	</div>
</div>
</div>
<?php $this->endContent(); ?>