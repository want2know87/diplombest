<?php $this->beginContent('//layouts/main-nineseven'); ?>
<div class="container">
<div class="row">
<div class="span12">
		<?php
			$this->widget('bootstrap.widgets.TbMenu', array(
					'items'=>array(
						'home' => array('label' => 'Кабинет', 'url'=>array('//cabinet/default/index'), 'icon' => 'user', 'visible'=>!Yii::app()->user->checkAccess('admin')),
						'order' => array('label' => 'Заказы', 'url'=>array('//cabinet/order/index'), 'icon' => 'briefcase'),
						'payment' => array('label'=>'Платежи', 'url'=>array('//cabinet/payment/index'), 'icon' => 'money', 'visible'=>Yii::app()->user->checkAccess('admin')),
						'personal' => array('label'=>'Личные данные', 'url'=>array('//cabinet/personal/update'), 'icon' => 'edit'),
						'users' => array('label'=>'Пользователи', 'url'=>array('user/index'), 'icon' => 'group', 'visible'=>Yii::app()->user->checkAccess('admin')),
						'comment' => array('label'=>'Отзывы', 'url'=>array('//cabinet/commentGii/admin'), 'icon' => 'book', 'visible'=>Yii::app()->user->checkAccess('admin')),
						'order_types' => array('label'=>'Типы работ', 'url'=>array('//cabinet/orderType/admin'), 'icon' => 'folder-open', 'visible'=>Yii::app()->user->checkAccess('admin')),
						'branches' => array('label'=>'Специализации', 'url'=>array('//cabinet/branch/admin'), 'icon' => 'folder-open', 'visible'=>Yii::app()->user->checkAccess('admin')),
						'disciplines' => array('label'=>'Предметы', 'url'=>array('//cabinet/discipline/admin'), 'icon' => 'folder-open', 'visible'=>Yii::app()->user->checkAccess('admin')),
						'seo' => array('label'=>'Seo', 'url'=> array('//cabinet/seoData/admin'), 'icon' => 'icon-file', 'visible'=>Yii::app()->user->checkAccess('admin')),
					),
					'type'=>'tabs',
					'htmlOptions' => array(
						'id' => 'cabinet-menu',
					),
				)
			);
		?>
</div>
</div>
</div>
<div class="container">
<div class="row">
	<div class="span9">
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
	<div class="span3">
		<div id="sidebar">
		<?php
			$this->widget('bootstrap.widgets.TbMenu', array(
					'items'=>$this->menu,
					'type'=>'tabs',
					'stacked' => true,
					'htmlOptions' => array(
					),
				)
			);
		?>
		</div><!-- sidebar -->
		<div>
		<?php if(!empty($this->clips['orderAttachments'])):?>
		<h2>Вложения</h2>
		<?=$this->clips['orderAttachments'];?>
		<?php endif;?>
		</div>
	</div>
</div>
</div>
<?php $this->endContent(); ?>
