<?php $this->beginContent('//layouts/main-nineseven'); ?>
<div class="container">
<div class="row">
	<div class="span9">
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
	<div class="span3">
		<div id="sidebar">
		<?php
			$this->widget('bootstrap.widgets.TbMenu', array(
					'items'=>$this->menu,
					'type'=>'tabs',
					'stacked' => true,
					'htmlOptions' => array(
						//'data-spy' => 'affix',
						//'data-offset-top' => '90',
					),
				)
			);
		?>
		</div><!-- sidebar -->
	</div>
</div>
</div>
<?php $this->endContent(); ?>
