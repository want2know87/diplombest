<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='cabinet';

	/**
	 * @var string the modal layout for the controller view. Defaults to '//layouts/modal',
	 * meaning using a modal window layout. See 'protected/views/layouts/modal.php'.
	 */
	public $modalLayout='//layouts/modal';

	/**
	 * @var string the redirect layout for the controller view. Defaults to '//layouts/redirect',
	 * meaning using a redirect layout. See 'protected/views/layouts/modal.php'.
	 */
	public $topRedirectView='//site/redirect';

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public $banner = '/images/banner.jpg';

	public $services=array();

	public function setModal() {
		$this->layout = $this->modalLayout;
	}

	public function topRedirect($url) {
		if (is_array($url)) {
			$route = isset($url[0]) ? $url[0] : '';
			$url = $this->createUrl($route, array_splice($url, 1));
		}
		$this->renderPartial(
			$this->topRedirectView,
			array(
				'redirect' => true,
				'url' => $url,
			)
		);
		Yii::app()->end();
	}

	public function init()
	{
		$this->services = (new Services)->servicesVisibleAll();
	}
}
