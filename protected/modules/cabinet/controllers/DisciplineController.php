<?php

class DisciplineController extends Controller
{
    public $pageDescription='Предметы';

    public $pageKeywords='Предметы';

    public $npTitle='Предметы';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'roles'=>array('admin','manager'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function loadModel($id)
    {
        $model=Discipline::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionAdmin()
    {
        $model=new Discipline('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Discipline']))
            $model->attributes=$_GET['Discipline'];

        $this->render('admin',array(
            'model'=>$model,
            'branchList'=>$this->getBranchList(),
        ));
    }

    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Discipline');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $model=new Discipline;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Discipline']))
        {
            $model->attributes=$_POST['Discipline'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
            'model'=>$model,
            'branchList'=>$this->getBranchList(),
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Discipline']))
        {
            $model->attributes=$_POST['Discipline'];
            $model->save();
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
            'branchList'=>$this->getBranchList(),
        ));
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    protected function getBranchList()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 't.name asc';
        $models = Branch::model()->findAll($criteria);
        $list = array();
        foreach ($models as $model) {
            $list[$model->id] = $model->name;
        }
        return $list;
    }
}
