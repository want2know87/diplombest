<?php

error_reporting(E_ALL ^ E_STRICT);

class OrderController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='cabinet';
public $pageKeywords='������ �� ����� �������� ����������';
public $pageDescription='������ �� ����� � ������ ���� ������������ �����, ������ � ��������� - ����������';
public $npTitle='������ �� �����, ������ � ��������� - ����������!';
public function filters()
{
	return array(
		'accessControl', // perform access control for CRUD operations
	);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
	return array(
		array('allow',
			'actions'=>array('index', 'view', 'attach', 'message', 'bank', 'perform'),
			'users'=>array('@'),
		),
		array('allow',
			'actions'=>array('create'),
			'users'=>array('@'),
		),
		array('allow',
			'actions'=>array('admin', 'update', 'delete', 'perform'),
			'roles'=>array('manager'),
		),
		array('deny',  // deny all users
			'users'=>array('*'),
		),
	);
}

public function actions()
{
	return array(
		'attach'=>array(
			'class'=>'UploadAction',
			'formClass'=>'OrderAttachment',
			'mimeTypeAttribute'=>'mimeType',
			'fileNameAttribute'=>'path',
			'path' => Yii::app()->params['orderAttachmentFolder'],
			//'publicPath' => Yii::app()->getBaseUrl() . "/uploads",
		),
	);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
	$model = $this->loadModel($id);
	if (!$model->checkAccess('viewOrder')) {
	    throw new CHttpException(403,'Fordidden.');
	}
	$this->render(
		'view',
		array(
			'model' => $model,
		)
	);
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Order('cabinet');

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Order']))
{
$model->attributes=$_POST['Order'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}
$this->render('create',array(
'model' => $model,
));

}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Order']))
{
	$model->setScenario('update');
$model->attributes=$_POST['Order'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

// $model->date = date('d.m.Y', strtotime($model->date));

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$model = $this->loadModel($id);
$model->cancel();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('view','id'=>$model->id));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
	if (Yii::app()->user->checkAccess('manager')) {
		$dataProvider=new CActiveDataProvider('Order', array(
			'criteria'=>array(
				'order'=>'createdOn DESC',
			),
    	));
	} else if (Yii::app()->user->checkAccess('author')) {
	    $dataProvider=new CActiveDataProvider(Order::model()->authorable());
	} else {
	    $dataProvider=new CActiveDataProvider(Order::model()->own());
	}
	//$this->render('index',array(
	//	'dataProvider'=>$dataProvider,
	//));
	$this->render('index',array(
		'dataProvider'=>$dataProvider,
	));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
	$model=new Order('search');
	$model->unsetAttributes();  // clear any default values
	if(isset($_GET['Order']))
	$model->attributes=$_GET['Order'];

	$this->render('admin',array(
	'model'=>$model,
	));
}

public function actionPerform($id) {

	$operation = $this->loadOperation($_REQUEST['operation']);
	if (!$operation) {
		throw new CHttpException(404, 'The requested operation does not exist.');
	}

	if ($operation->action) {
		$action = $this->createAction($operation->action);
	    $this->runAction($action);
	} else {
		$model = $this->loadModel($id);
		$model->perform($operation);
		$this->redirect(array('view', 'id' => $model->id));
	}
}

public function actionEvaluate($id) {
	$model = $this->loadModel($id);
    $model->setScenario('evaluate');

	if (isset($_POST['Order'])) {
		$model->attributes=$_POST['Order'];
		$operation = $this->loadOperation($_REQUEST['operation']);
		if ($model->save()) {
			$model->refresh();
			$model->perform($operation);
			if ($model->getState(OrderStatus::CONTEXT_WORK)->status->isInitial) {
				$operation = $this->loadOperation(OrderOperation::INITIALIZE);
				if ($operation) {
					$model->perform($operation);
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				$this->topRedirect(array('view', 'id'=>$model->id));
			} else {
			    $this->redirect(array('view','id'=>$model->id));
			}
		}
	}

	if (Yii::app()->request->isAjaxRequest) {
		$this->setModal();
	}
	$this->render('evaluate', array(
		'model' => $model,
	));
}

public function actionPreview($id) {
	$model = $this->loadModel($id);
    $model->setScenario('preview');

	if (isset($_POST['Order'])) {
		$model->attributes=$_POST['Order'];
		$operation = $this->loadOperation($_REQUEST['operation']);
		if ($model->save()) {
			$log = $model->perform($operation);
			foreach ($model->attachments as $attachment) {
				if (in_array($attachment->id, $_POST['Order']['previewAttachments'])) {
					if ($log) {
						$attachment->setIsNewRecord(true);
						$attachment->setPrimaryKey(null);
						$attachment->orderStatusLog_id = $log->id;
						$attachment->isPreview = 1;
						$attachment->save();
					}
				} else if ($attachment->isPreview) {
					$attachment->isPreview = 0;
					$attachment->save();
				}
			}
			$model->refresh();
			if (Yii::app()->request->isAjaxRequest) {
				$this->topRedirect(array('view', 'id'=>$model->id));
			} else {
			    $this->redirect(array('view','id'=>$model->id));
			}
		}
	}

	if (Yii::app()->request->isAjaxRequest) {
		$this->setModal();
	}
	$this->render('preview', array(
		'model' => $model,
		'preview' => true,
	));
}

public function actionReady($id) {
	$model = $this->loadModel($id);
    $model->setScenario('ready');

	if (isset($_POST['Order'])) {
		$model->attributes=$_POST['Order'];
		$operation = $this->loadOperation($_REQUEST['operation']);
		if ($model->validate()) {
			$log = $model->perform($operation);
			foreach ($model->attachments as $attachment) {
				if (in_array($attachment->id, $_POST['Order']['finalAttachments'])) {
					if ($log) {
						$attachment->setIsNewRecord(true);
						$attachment->setPrimaryKey(null);
						$attachment->orderStatusLog_id = $log->id;
						$attachment->isFinal = 1;
						$attachment->save();
					}
				} else if ($attachment->isFinal) {
					$attachment->isFinal = 0;
					$attachment->save();
				}
			}
			//$model->refresh();
			if (Yii::app()->request->isAjaxRequest) {
				$this->topRedirect(array('view', 'id'=>$model->id));
			} else {
			    $this->redirect(array('view','id'=>$model->id));
			}
		}
	}

	if (Yii::app()->request->isAjaxRequest) {
		$this->setModal();
	}
	$this->render('preview', array(
		'model' => $model,
		'preview' => false,
	));
}

public function actionMessage($id) {
	$model = $this->loadModel($id);
	$message = new OrderStatusLog;

	if (isset($_POST['OrderStatusLog'])) {
		$message->attributes=$_POST['OrderStatusLog'];
		if ($model->checkAccess('myOrder')) {
		    $message->context = OrderStatus::CONTEXT_USER_MESSAGES;
		} else if ($model->checkAccess('authoredOrder')) {
		    $message->context = OrderStatus::CONTEXT_AUTHOR_MESSAGES;
		} else if ($model->checkAccess('managedOrder')) {
		    $message->context = $message->context;
		} else {
		    throw new CHttpException(403, 'Cannot add message');
		}

		$message->user_id = Yii::app()->user->getId();
		if ($message->save()) {
			$message->refresh();
			if (isset($_POST['attachment'])) {
				foreach ((array) $_POST['attachment'] as $file_id) {
					$attachment = OrderAttachment::model()->findByPk($file_id);
					if ($attachment && !$attachment->orderStatusLog_id) {
						$attachment->orderStatusLog_id = $message->id;
						$attachment->save();
					}
			    }
			}
			Yii::app()->user->setFlash('success', Yii::t('order', 'Сообщение добавлено'));
		}
	}
	$this->render('view', array(
		'model' => $model,
	));
}

public function actionPay($id) {
	$model = $this->loadModel($id);

	if (Yii::app()->request->isAjaxRequest) {
		$this->setModal();
	}
	$this->render('pay', array(
		'model' => $model,
	));
}

public function actionPaymentNotify($id) {
	$model = $this->loadModel($id);
	$payment = new OrderPayment('insert');
	if (Yii::app()->request->isAjaxRequest) {
		$this->setModal();
	}

	if (isset($_POST['OrderPayment'])) {
	    $payment->attributes = $_POST['OrderPayment'];
		$payment->user_id = Yii::app()->user->getId();
		$payment->order_id = $model->id;
		$payment->paymentStatus = OrderPayment::STATUS_NEW;
		$operation = $this->loadOperation($_REQUEST['operation']);
		if ($payment->save()) {
			$payment->refresh();
			$log = $model->perform($operation);
			if (isset($_POST['attachment'])) {
				foreach ((array) $_POST['attachment'] as $file_id) {
					$attachment = OrderAttachment::model()->findByPk($file_id);
					if ($attachment && !$attachment->orderStatusLog_id) {
						$attachment->orderStatusLog_id = $log->id;
						$attachment->save();
					}
				}
			}
			Yii::app()->user->setFlash('success', Yii::t('order', 'Сообщение об оплате добавлено'));
			if (Yii::app()->request->isAjaxRequest) {
				$this->topRedirect(array('view', 'id'=>$model->id));
			} else {
				$this->redirect(array('view','id'=>$model->id));
			}
		}
	}

	$this->render('payNotify', array(
		'model' => $model,
		'payment' => $payment,
	));
}

public function actionPayConfirm($id) {
	$model = $this->loadModel($id);
	$model->setScenario('payConfirm');

	if (isset($_POST['Order'])) {
		$model->attributes=$_POST['Order'];
		$operation = $this->loadOperation($_REQUEST['operation']);
		if ($model->save()) {
			$model->refresh();
			$model->perform($operation);
			if (Yii::app()->request->isAjaxRequest) {
				$this->topRedirect(array('view', 'id'=>$model->id));
			} else {
			    $this->redirect(array('view','id'=>$model->id));
			}
		}
	}

	if (Yii::app()->request->isAjaxRequest) {
		$this->setModal();
	}
	$this->render('payConfirm', array(
		'model' => $model,
	));
}

public function actionBank($id) {
	$model = $this->loadModel($id);

	if (Yii::app()->request->isAjaxRequest) {
		$this->setModal();
	}
	$this->renderPartial('pd4', array(
		'model' => $model,
	));
}



/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Order::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadOperation($id)
{
$model=OrderOperation::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested operation does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='order-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
