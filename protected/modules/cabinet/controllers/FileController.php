<?php

class FileController extends Controller
{
    public $pageKeywords='������ �� ����� �������� ����������';
    public $pageDescription='������ �� ����� � ������ ���� ������������ �����, ������ � ��������� - ����������';
    public $npTitle='������ �� �����, ������ � ��������� - ����������!';
	public function actionDownload($path)
	{
		$file = OrderAttachment::model()->with(array('message', 'order'))->path($path)->find();
		if (!$file) {
		    throw new CHttpException(404, 'File not found');
		} else {
			if ($file->order->checkAccess('managedOrder')) {
			    $this->render('download', array('file' => $file));
			} else if ($file->order->checkAccess('myOrder')) {
			    if (in_array(
						$file->message->context, 
						array(
							OrderStatus::CONTEXT_WORK, 
							OrderStatus::CONTEXT_USER_MESSAGES, 
							OrderStatus::CONTEXT_PAYMENT
						)
					)
				) {
					$this->render('download', array('file' => $file)); 
			    } else {
					throw new CHttpException(403, 'Forbidden');
			    }
			} else if ($file->order->checkAccess('authoredOrder')) {
			    if (in_array(
						$file->message->context, 
						array(
							OrderStatus::CONTEXT_AUTHOR_MESSAGES, 
						)
					)
				) {
					$this->render('download', array('file' => $file)); 
			    } else {
					throw new CHttpException(403, 'Forbidden');
			    }
			} else {
				throw new CHttpException(403, 'Forbidden');
			}
		}
		
	}

	public function actionUnlink()
	{
		$this->render('unlink');
	}

	public function actionUpload()
	{
		$this->render('upload');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}