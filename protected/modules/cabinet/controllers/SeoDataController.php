<?php

class SeoDataController extends Controller
{
    public $pageDescription='Seo texts';

    public $pageKeywords='Seo texts';

    public $npTitle='Seo texts';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'roles'=>array('admin','manager'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function loadModel($id)
    {
        $model=SeoData::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionAdmin()
    {
        $model=new SeoData('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['SeoData']))
            $model->attributes=$_GET['SeoData'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    public function actionCreate($type = SeoData::NEW_SCHOOL)
    {
        $model=new SeoData;
        $model->type = $type;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['SeoData']))
        {
            $model->attributes=$_POST['SeoData'];
            if($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['SeoData']))
        {
            $model->attributes=$_POST['SeoData'];
            $model->save();
            if($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
}
