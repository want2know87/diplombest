<?php

class BranchController extends Controller
{
    public $pageDescription='Специализации';

    public $pageKeywords='Специализации';

    public $npTitle='Специализации';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'roles'=>array('admin','manager'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function loadModel($id)
    {
        $model=Branch::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionAdmin()
    {
        $model=new Branch('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Branch']))
            $model->attributes=$_GET['Branch'];

        $this->render('admin',array(
            'model'=>$model,
            'orderTypeList'=>$this->getOrderTypeList(),
        ));
    }

    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Branch');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $model=new Branch;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Branch']))
        {
            $model->attributes=$_POST['Branch'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
            'model'=>$model,
            'orderTypeList'=>$this->getOrderTypeList(),
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Branch']))
        {
            $model->attributes=$_POST['Branch'];
            $model->save();
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
            'orderTypeList'=>$this->getOrderTypeList(),
        ));
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    protected function getOrderTypeList()
    {
        $models = OrderType::model()->findAll();
        $list = array();
        foreach ($models as $model) {
            $list[$model->id] = $model->name;
        }
        return $list;
    }
}
