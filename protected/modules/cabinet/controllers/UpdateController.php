<?php

Yii::import('ext.yii-booster.widgets.TbEditableSaver');

class UpdateController extends Controller
{
public $pageKeywords='������ �� ����� �������� ����������';
public $pageDescription='������ �� ����� � ������ ���� ������������ �����, ������ � ��������� - ����������';
public $npTitle='������ �� �����, ������ � ��������� - ����������!';
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow', // allow managers to perform actions
				'actions' => array('order'),
				'roles' => array('manager'),
			),
			array('allow', // allow admin to perform actions
				'actions' => array('user', 'config'),
				'roles' => array('admin'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

    /**
     * Updates an order.
     */
    public function actionOrder()
    {
		$es = new TbEditableSaver('Order');
		$es->update();
    }

    /**
     * Updates a user.
     */
    public function actionUser()
    {
		$es = new TbEditableSaver('User');
		$es->update();
    }

    /**
     * Updates config.
     */
    public function actionConfig()
    {
		$es = new TbEditableSaver('Config');
		$es->update();
    }
}
