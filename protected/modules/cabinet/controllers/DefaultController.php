<?php

class DefaultController extends Controller
{
    /**
	 * Declares class-based actions.
	 */
	public $pageKeywords='������ �� ����� �������� ����������';
	public $pageDescription='������ �� ����� � ������ ���� ������������ �����, ������ � ��������� - ����������';
	public $npTitle='������ �� �����, ������ � ��������� - ����������!';

	// added 2017-01-12
	public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions'=>array('install', 'index'),
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }
    // 2017-01-12

	public function actionInstall()
	{

    $auth=Yii::app()->authManager;

    //���������� ��� ������������ �������
    $auth->clearAll();


    $bizRule='return $params["order"]->user_id == $params["user_id"];';
    $bizRule='return $params["order"]->isMine();';
	$myOrder = $auth->createOperation('myOrder', '�������� ������ ������', $bizRule);
    $viewOrder = $auth->createOperation('viewOrder', '�������� ������');

	$myOrder->addChild('viewOrder');

    $bizRule='return $params["order"]->isAuthored();';
	$authoredOrder = $auth->createOperation('authoredOrder', '�������� ������ �������', $bizRule);

	$authoredOrder->addChild('viewOrder');

	$managedOrder = $auth->createOperation('managedOrder', '�������� ������ ����������');
	$managedOrder->addChild('viewOrder');



    //$bizRule='return $params["order"]->isMine() || $params["order"]->isNew();';
    //$task = $auth->createOperation('viewAllNewOrders', '�������� ���� ����� �������', $bizRule);
	//$userActions->addChild('viewAllNewOrders');

    $bizRule='return !Yii::app()->user->isGuest;';
    $user = $auth->createRole('user', '������������', $bizRule);
    $user->addChild('myOrder');

    $role = $auth->createRole('author');
    //$role->addChild('user');
    $role->addChild('authoredOrder');

    //������� ���� ��� ������������ admin � ���������, ����� �������� �� ����� ���������
    $role = $auth->createRole('manager');
    $role->addChild('user');
    $role->addChild('author');
    $role->addChild('managedOrder');


    //��� ������������ ����� ����������� ��-��������� � ����� user,
    //������ root ����� ������ ���� ������� ������������

    //������� ���� ��� ������������ root
    $role = $auth->createRole('admin');
    //��������� ��������, ����������� ��� admin'� � ��������� �����
    //$role->addChild('admin');
    $role->addChild('manager');
    $role->addChild('user');
    $role->addChild('author');

    //������� �������� ��� user'�
    //$bizRule='return Yii::app()->user->id==$params["order"]->user_id;';

	/*
    $auth->createOperation('createContact','�������� ��������');
    $auth->createOperation('viewContacts','�������� ������ ���������');
    $auth->createOperation('readContact','�������� ��������', $bizRule);
    $auth->createOperation('updateContact','�������������� ��������',$bizRule);
    $auth->createTask('deleteContact','�������� ��������',$bizRule);
	*/


    //��������� ������������ � �����
    $auth->assign('admin', 1);

    //��������� ���� � ��������
    $auth->save();

	$this->render('index');
	}

	public function actionIndex()
	{
        if (Yii::app()->user->checkAccess('admin')) {
            $this->redirect(array('//cabinet/order/index'));
        }
		$this->render('index');
	}

}
