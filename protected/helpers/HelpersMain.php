<?php
/**
 * Общий хелпер
 */
class HelpersMain
{
    /**
     * Перемещает загруженные или временные файлы в переданную директорию, присваевая уникальное имя
     * @param array - последовательность элементов в массивах одинаковая
     *      $file['upload_file'][] = 'ПУТЬ К ФАЙЛУ С ИМЕНЕМ' - пути к загруженным файлам
     *      $file['name_file'][] = 'ИМЯ ФАЙЛА С РАСШИРЕНИЕМ'
     *      $file['type_file'][] = 'ТИП ФАЙЛА' - необязательный
     * @param string $directory - Путь к директории
     * @param array $type[] = 'TYPE' - Только с этими типами файлов загружать, необязательный работает в связке с $file['type_file'][]
     * @return array - массив с именами файлов какие закачали
     */
    public static function uploadFil($file, $directory, $type = [])
    {
        // Тип файла
        $this_type = '';

        // Возврощает, массив с именами файлов какие закачали
        $return = [];

        $file_name = ''; // имя файла

        foreach ($file['upload_file'] as $k => $upload_file) {

            // Если файл не разрешен к закачке то вернет false
            $this_type = isset($file['type_file']) ? array_search($file['type_file'][$k], $type) : NULL;

            $nameFile = explode('.', $file['name_file'][$k]);
            $file_name = uniqid(time(),true) . '.' . end($nameFile);

            if($this_type === false)continue;

            move_uploaded_file($upload_file, $directory . $file_name);

            $return[] = $file_name;
        }

        return $return;
    }

    /**
     * Скачать файл
     * @param string $path - путь к файлу
     */
    public static function downloadFile($path)
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($path));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        readfile($path);
    }
}
?>
