<?php

class SeoHelper
{
    public static function descr($url, array $params)
    {
        $descr = $params['defaultDescr'];

        if (strpos($url, 'biznes-plan') !== false) {
            $descr = sprintf(
                'Написание бизнес-плана в Москве, Санкт-Петербурге, %s, более 1000 авторов преподавателей российских ВУЗов, полное сопровождение до защиты, низкие цены.',
                $params['placeholder']
            );
        }

        if (strpos($url, 'diplom') !== false) {
            $descr = sprintf(
                'Написание диплома на заказ, низкие цены. %s заказать в Москве. Редактирование, исправление и доработки бесплатно. Гарантии, официальный договор.',
                $params['placeholder']
            );
        }

        if (strpos($url, 'dissertacija') !== false) {
            $descr = sprintf(
                'Сайт диссертаций на заказ, квалифицированные авторы, %s, профессиональное написание, официальный договор на выполнение курсовой работы, все исправления бесплатно.',
                $params['placeholder']
            );
        }

        if (strpos($url, 'esse') !== false) {
            $descr = sprintf(
                'Написание эссе в Москве профессиональными авторами. %s, низкие цены, полное сопровождение работы и последующие доработки бесплатно.',
                $params['placeholder']
            );
        }

        if (strpos($url, 'kursovaya') !== false) {
            $descr = sprintf(
                'Возможность заказать написание курсовой работы в Москве, %s, низкие цены, более 1000 профессиональных авторов, гарантия, официальный договор.',
                $params['placeholder']
            );
        }

        if (strpos($url, 'otchet-po-praktike') !== false) {
            $descr = sprintf(
                'Предлагаем услуги написания отчета по практике, %s, официальный договор, низкие цены. Более 1000 профессиональных авторов преподавателей российских ВУЗов.',
                $params['placeholder']
            );
        }

        if (strpos($url, 'part-diploma') !== false) {
            $descr = sprintf(
                'Написание части дипломной работы на заказ в Москве, Санкт-Петербурге, %s, полное сопровождение до защиты, низкие цены.',
                $params['placeholder']
            );
        }

        if (strpos($url, 'prakticheskoe-zadanie') !== false) {
            $descr = sprintf(
                'Написание практических заданий на заказ, низкие цены. %s, исправление и доработки бесплатно. Гарантии, официальный договор.',
                $params['placeholder']
            );
        }

        if (strpos($url, 'referat') !== false) {
            $descr = sprintf(
                'Сайт рефератов на заказ, %s, квалифицированные авторы, официальный договор на выполнение курсовой работы, все дополнительные исправления бесплатно.',
                $params['placeholder']
            );
        }

        /** descr для продвигаемых страниц */
        switch ($url) {
            case '/services/diplom':
                $descr = 'Написание дипломной работы на заказ в Москве, Санкт-Петербурге, более 1000 авторов преподавателей российских ВУЗов, полное сопровождение до защиты, низкие цены.';
                break;
            case '/services/dissertacija':
                $descr = 'Написание диссертаций на заказ, низкие цены. Редактирование, исправление и доработки бесплатно. Гарантии, официальный договор. Более 1000 авторов преподавателей российских ВУЗов.';
                break;
            case '/services/kursovaya':
                $descr = 'Сайт курсовых работ на заказ, квалифицированные авторы, официальный договор на выполнение курсовой работы, все дополнительные исправления бесплатно.';
                break;
            case '/services/otchet-po-praktike':
                $descr = 'Написание отчета по научно исследовательской практике на заказ в Москве. Низкие цены, профессиональные авторы, полное сопровождение работы и последующие доработки бесплатно.';
                break;
            case '/services/part-diploma':
                $descr = 'Возможность заказать написание части дипломной работы в Москве, низкие цены, более 1000 профессиональных авторов, гарантия, официальный договор.';
                break;
            case '/services/referat':
                $descr = 'Предлагаем услуги написания реферата на заказ, гарантия, официальный договор, низкие цены. Более 1000 профессиональных авторов преподавателей российских ВУЗов.';
                break;
        }

       if (in_array(
            $url,
            array(
                '/zakaz/chertegzhi',
                '/zakaz/diplom-mba',
                '/zakaz/diplomnaya-rabota',
                '/zakaz/dnevnik',
                '/zakaz/doklad',
                '/zakaz/keis',
                '/zakaz/kontrolnaya-rabota',
                '/zakaz/online-pomoshch',
                '/zakaz/podbor-repetitora',
                '/zakaz/prezentacija',
                '/zakaz/referat-dlya-aspiranturi',
                '/zakaz/statija',
                '/zakaz/test',
                '/zakaz/voprosi-k-ekzamenu',
                '/zakaz/zadachi',
            )
        )) {
            $descr = sprintf(
                'Предлагаем %s в Москве. Низкие цены, профессиональные авторы, полное сопровождение работы и последующие доработки бесплатно.',
                $params['placeholder']
            );
        }

        return $descr;
    }

    public static function title($url, array $params)
    {
        $title = $params['defaultTitle'];

        /** title для продвигаемых страниц */
        switch ($url) {
            case '/services/diplom':
                $title = 'Сколько стоит заказать дипломную работу, купить дипломную работу в Москве недорого';
                break;
            case '/services/dissertacija':
                $title = 'Стоимость диссертации на заказ, кандидатская, докторская, магистерская диссертация под ключ';
                break;
            case '/services/kursovaya':
                $title = 'Купить курсовую работу на заказ, заказать курсовую работу на сайте';
                break;
            case '/services/otchet-po-praktike':
                $title = 'Заказать отчет по практике в Москве, отчет по научно исследовательской практике по доступной цене';
                break;
            case '/services/part-diploma':
                $title = 'Заказать часть дипломной работы в Москве';
                break;
            case '/services/referat':
                $title = 'Заказать реферат онлайн недорого, стоимость реферата на заказ, цены';
                break;
        }

        /** h1 для непродвигаемых страниц */
        if (in_array(
            $url,
            array(
                '/services/prakticheskoe-zadanie',
                '/services/esse',
                '/services/biznes-plan',
            )
        )) {
            $title = sprintf('%s, купить в Москве', $params['placeholder1']);
        }

        if (isset($params['placeholder2'])) {
            $title =sprintf('%s на заказ, купить в Москве', $params['placeholder2']);
        }

        if (isset($params['placeholder3'])) {
            $title =sprintf('%s на заказ, купить в Москве', $params['placeholder3']);
        }

        return $title;
    }

    public static function h1($url, array $params)
    {
        $h1 = $params['defaultH1'];

        /** h1 для продвигаемых страниц */
        switch ($url) {
            case '/services/diplom':
                $h1 = 'Дипломная работа на заказ';
                break;
            case '/services/dissertacija':
                $h1 = 'Диссертация на заказ';
                break;
            case '/services/kursovaya':
                $h1 = 'Курсовые работы на заказ';
                break;
            case '/services/otchet-po-praktike':
                $h1 = 'Отчет по практике на заказ';
                break;
            case '/services/part-diploma':
                $h1 = 'Часть дипломной работы на заказ';
                break;
            case '/services/referat':
                $h1 = 'Реферат на заказ';
                break;
        }

        /** h1 для непродвигаемых страниц */
        if (in_array(
            $url,
            array(
                '/services/biznes-plan',
                '/services/prakticheskoe-zadanie',
                '/zakaz/chertegzhi',
                '/zakaz/diplom-mba',
                '/zakaz/diplomnaya-rabota',
                '/zakaz/dnevnik',
                '/zakaz/doklad',
                '/zakaz/keis',
                '/zakaz/kontrolnaya-rabota',
                '/zakaz/online-pomoshch',
                '/zakaz/podbor-repetitora',
                '/zakaz/prezentacija',
                '/zakaz/referat-dlya-aspiranturi',
                '/zakaz/statija',
                '/zakaz/test',
                '/zakaz/voprosi-k-ekzamenu',
                '/zakaz/zadachi',
                '/services/esse',
            )
        )) {
            $h1 = sprintf('%s на заказ', $params['placeholder1']);
        }

        if (isset($params['placeholder2'])) {
            $h1 =sprintf('%s "%s"', $params['placeholder1'], $params['placeholder2']);
        }

        if (isset($params['placeholder3'])) {
            $h1 =sprintf('%s "%s"', $params['placeholder1'], $params['placeholder3']);
        }

        return $h1;
    }
}
