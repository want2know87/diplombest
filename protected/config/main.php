<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'language' => 'ru',
    'aliases' => array(
        'xupload' => 'ext.xupload',
    ),
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'DiplomBest.com',

    // preloading 'log' component
    'preload'=>array(
        'cache',
        'log',
        'bootstrap',
        'config',
    ),

    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.models.comment.*',
        'application.models.comment.data_source.*',
        'application.components.*',
        'application.helpers.*',
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.*',
        'ext.eauth.services.*',
    ),

    'modules'=>array(
        // uncomment the following to enable the Gii tool
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'gii',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            //'ipFilters'=>array('127.0.0.1','::1'),
            'ipFilters'=>array('*.*.*.*','::1'),
            'generatorPaths' => array(
                'application.gii',
                'ext.yii-booster.gii'
            ),
        ),
        'cabinet',
    ),

    // application components
    'components'=>array(
        'cache'=>array(
            'class'=>'system.caching.CFileCache',
         ),
        'config' => array(
            'class' => 'DConfig',
        ),
        'format' => array('dateFormat' => 'd.m.Y',
                        'timeFormat' => 'H:i:s',
                        'datetimeFormat' => 'd.m.Y H:i:s',
                        'numberFormat' => array('decimals' => null, 'decimalSeparator' => '.', 'thousandSeparator' => ' '),
                        'booleanFormat' => array('Нет','Да'),
                    ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'useStrictParsing' => true,
            'showScriptName' => false,
            'rules' => array(
                //The rule is necessary to use, otherwise "Too many redirects" error may occure during user initialization
                '' => 'site/index',
                'authors' => 'site/authors',
                'contact' => 'site/contact',
                'services' => 'site/services',
                '<view:(about|scheme|payment)>' => 'site/page',
                'services/<order_type_alias:[-\w]+>/<branch_id:[-\w]+>/<discipline_id:[-\w]+>' => 'services/discipline',
                'services/<order_type_alias:[-\w]+>/<branch_id:[-\w]+>' => 'services/branch',
                'services/<order_type_alias:[-\w]+>' => 'services/index',
                'cabinet/file/<action:\w+>/<path:\w+>/<name:.+>' => 'cabinet/file/<action>',
                'zakaz/<type:[\w-]+>' => 'order/promo',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db', //Type in your db connection name instead of db
            'assignmentTable' => 'authAssignment',
            'itemChildTable' => 'authItemChild',
            'itemTable' => 'authItem',
            'defaultRoles'=>array(
                'user',
                //'admin',
            ),
        ),
        'loid' => array(
            'class' => 'ext.lightopenid.loid',
        ),
        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true,
            'services' => array(
                'vkontakte' => array(
                    'class' => 'ext.eauth.custom_services.CustomVKontakteService',
                    'client_id' => '5967435',//'4565430',
                    'client_secret' => 'jTxCnyjZTlzDlArhRpYx',//'1KKprvRaGoBLExQr8YWK',
                ),
                /*'odnoklassniki' => array(
                    'class' => 'ext.eauth.custom_services.CustomOdnoklassnikiService',
                    'client_id' => '1103335424',
                    'client_secret' => '95A65E66E639D0D871751EED',
                ),*/
                'mailru' => array(
                    'class' => 'ext.eauth.custom_services.CustomMailruService',
                    'client_id' => '725224',
                    'client_secret' => 'e02e144f3f35248d602526bd7ba3e317',
                ),
                'yandex' => array(
                    'class' => 'ext.eauth.custom_services.CustomYandexService',
                ),
                /*'google' => array(
                    'class' => 'ext.eauth.custom_services.CustomGoogleService',
                ),*/
                'google_oauth' => array(
                    'class' => 'ext.eauth.services.GoogleOAuthService',
                    'client_id' => '970919442485-4pma3j1r4q1f4mqf0fh486aahq3556ru.apps.googleusercontent.com',
                    'client_secret' => 'W2Zj6NFvkRi2h7-KN_m9lxMV',
                ),
                /*'twitter' => array(
                    'class' => 'lily.services.LTwitterService',
                    'key' => '',
                    'secret' => '',
                ),*/
            ),
        ),
        'user' => array(
            'class' => 'WebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => array('//auth/login'),
            'autoUpdateFlash' => false,
        ),
        //Mail component is configured for use with gmail,
        //read corresponding docs to get acquinted with full set of parameters
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            //'transportType' => 'smtp',
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false,
            /*
            'transportOptions' => array(
                'host' => 'smtp.gmail.com',
                'username' => 'example@gmail.com',
                'password' => 'Example password',
                'port' => 465,
                'encryption' => 'ssl',
            ),
            */
        ),

        'bootstrap' => array(
            'class' => 'ext.yii-booster.components.Bootstrap',
        ),
        //'db'=>array(
        //  'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
        //),
        // uncomment the following to use a MySQL database

        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=diplom_db',
            'emulatePrepare' => true,
            'username' => 'egor',
            'password' => 'X4m9F9e4',
            'charset' => 'utf8',
            // 'schemaCachingDuration' => 60,
        ),

        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        //'adminEmail'=>'admin@diplombest.com',
        'adminEmail'=>'egor.sh@nineseven.ru',
        'noreplyEmail'=>'noreply@diplombest.com',
        'orderAttachmentFolder'=> dirname(__FILE__) . '/../../storage',
        'seoData' => require(dirname(__FILE__). '/seo/data.php'),
    ),
);
