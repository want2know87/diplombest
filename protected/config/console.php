<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=diplom_db',
			'emulatePrepare' => true,
			'username' => 'egor',
			'password' => 'X4m9F9e4',
			'charset' => 'utf8',
		),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db', //Type in your db connection name instead of db
            'assignmentTable' => '{{rbac_assignment}}',
            'itemChildTable' => '{{rbac_item_child}}',
            'itemTable' => '{{rbac_item}}',
            'defaultRoles'=>array('userAuthenticated'),
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
	'commandMap' => array(
		'migrate' => array(
			'class' => 'system.cli.commands.MigrateCommand',
			'migrationPath' => 'application.migrations',
			'migrationTable' => 'ls_migration',
			'connectionID' => 'db',
		),
		'lily_rbac' => array(
			'class' => 'ext.lily.commands.LAuthInstaller'
		),
	),
);
