<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
			/* uncomment the following to provide test database connection
			'db'=>array(
				'connectionString'=>'DSN for test database',
			),
			*/
			'db'=>array(
				'connectionString' => 'mysql:host=localhost;dbname=diplom',
				'emulatePrepare' => true,
				'username' => 'user',
				'password' => '',
				'charset' => 'utf8',
				'enableParamLogging' => true,
			),
			'log'=>array(
				'class'=>'CLogRouter',
				'routes'=>array(
					array(
						'class'=>'CFileLogRoute',
						'levels'=>'error, warning',
					),
					// uncomment the following to show log messages on web pages
					/*
					*/
					array(
						'class'=>'CWebLogRoute',
					),
				),
			),
		),
	)
);
