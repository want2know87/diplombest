<?php
/* @var $comment array - массив с отзывами */

// Отзывы 
$comment_html = ''; 
foreach ($comment as $key => $comment_item) {
	$comment_html .= '
	<div class="swiper-slide">
	    <div class="inl teLef comment">
	        <div class="star">
	            <div class="star_item inl"></div><div class="star_item inl"></div><div class="star_item inl"></div><div class="star_item inl"></div><div class="star_item inl"></div>
	        </div>
	        <div class="marginT9">
	            <div class="inl left alignTop">
	                
	            </div><div class="inl right alignTop">
	                '.$comment_item->content.'<br><br>
	                <b>'.$comment_item->name.'</b>, '.date('d-m-Y',$comment_item->date).' года
	            </div>
	        </div>
	    </div>
	</div>';
}

echo $comment_html;
?>