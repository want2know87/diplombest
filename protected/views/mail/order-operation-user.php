<?php $this->beginContent('//mail/common'); ?>
<p>Здравствуйте!</p>
<p>Новая операция по вашему <?=CHtml::link('заказу №' . $model->order->id, $this->createAbsoluteUrl('//cabinet/order/view', array('id' => $model->order->id)))?>.</p>
<?php if ($model->message): ?>
	<p><?=CHtml::encode($model->user->username)?>: <?=CHtml::encode($model->message)?></p>
<?php endif;?>
<?php $this->endContent(); ?>