<?php $this->beginContent('//mail/common'); ?>
<p>Здравствуйте!</p>
<p>Ваш пароль на сайте <?=Yii::app()->name?> успешно изменен.</p>
<p>Для входа на сайт используйте логин <i><?=$model->email?></i> и пароль <tt><?=$model->password_open?></tt>.
<p>
<?php if ($model->accounts):?>
Также у вас настроен вход без пароля, через <?=implode(', ', CHtml::listData($model->accounts, array('id', 'service')))?>.
<?php else: ?>
В <?=CHtml::link('личном кабинете', Yii::app()->createAbsoluteUrl('cabinet/default/index'))?> настраивается вход без пароля, через социальные сервисы.
<?php endif; ?>
</p>
<?php $this->endContent(); ?>