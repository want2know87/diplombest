<?php $this->beginContent('//mail/common'); ?>
<p>Здравствуйте!</p>
<p>Для восстановления доступа к сайту <?=Yii::app()->name?> перейдите по ссылке <?=CHtml::link($this->createAbsoluteUrl('recover', array('token' => $model->token)), $this->createAbsoluteUrl('recover', array('token' => $model->token)))?>. Ссылка действует до <?=Yii::app()->format->formatDatetime($model->validTill)?>.</p>
<?php $this->endContent(); ?>