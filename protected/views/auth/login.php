<?php
/* @var $this AuthController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Вход';
$this->breadcrumbs=array(
	'Вход',
);

$this->widget(
	'ext.yii-booster.widgets.TbAlert',
	array(
		'block' => true,
		'fade' => true,
		'closeText' => '&times;', // false equals no close link
		'events' => array(),
		'htmlOptions' => array(),
		'userComponentId' => 'user',
	)
);

?>

<div id="content">
    <?=$this->renderPartial('//layouts/_breadcrumbs')?>
    <div class="container">
		<div class="form">
		<?php $form=$this->beginWidget('ext.yii-booster.widgets.TbActiveForm', array(
			'id'=>'login-form',
			'action'=>$this->createUrl('auth/login'),
			'enableClientValidation'=>false,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		)); ?>
			<div class="row">

				<div class="span3 offset3">

				<h4>Вход с паролем</h4>
				<?php
				echo $form->textFieldRow($model, 'email', array('class' => 'span3'));
				echo $form->passwordFieldRow($model, 'password', array('class' => 'span3'));
				echo $form->checkboxRow($model, 'rememberMe');
				?>

				<div class="actions">
					<?=CHtml::submitButton('Войти', array('class' => 'btn btn-primary'))?>
					<?=CHtml::link('Забыли пароль?', array('auth/remind'), array('class' => 'remind'))?>
				</div>

				</div>

				<div class="span3">
				<h4>Вход без пароля</h4>
				<p>Используйте свою учетную запись на&nbsp;одном из&nbsp;этих сайтов:</p>
				<?php
				//$this->widget('ext.eauth.EAuthWidget', array('action' => 'auth/external'));
				$this->widget('AuthWidget', array('action' => 'auth/external'));
				?>
				</div>

			</div>

		<?php $this->endWidget(); ?>
		</div><!-- form -->
	</div>
</div>
