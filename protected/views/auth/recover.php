<?php
/* @var $this AuthController */

$this->breadcrumbs=array(
	'Auth'=>array('/auth'),
	'Recover password',
);
?>


<div id="content">
    <?=$this->renderPartial('//layouts/_breadcrumbs')?>
    <div class="container">
		<div class="form">
		<?php $form = $this->beginWidget('ext.yii-booster.widgets.TbActiveForm', array(
			'id'=>'recover-form',
			'action'=>$this->createUrl('auth/recover', array('token' => $token)),
			'enableClientValidation'=>false,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		)); ?>
			<?=$form->errorSummary($model)?>
			<div class="row">
				<div class="span4 offset4">
					<?php $this->widget(
						'ext.yii-booster.widgets.TbAlert',
						array(
							'block' => true,
							'fade' => true,
							'closeText' => '&times;',
							'userComponentId' => 'user',
						)
					);?>
					<?=$form->passwordFieldRow($model, 'password_new', array('class' => 'span3', 'labelOptions' => array('label' => 'Новый пароль:')));?>

					<?=$form->passwordFieldRow($model, 'password_repeat', array('class' => 'span3', 'labelOptions' => array('label' => 'Повторите пароль:')));?>
					<div class="actions">
						<?=CHtml::submitButton('Сохранить', array('class' => 'btn btn-primary'))?>
					</div>
				</div>
			</div>

		<?php $this->endWidget(); ?>
		</div><!-- form -->
		<?=User::generatePassword()?>
	</div>
</div>
