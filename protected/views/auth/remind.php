<?php
/* @var $this AuthController */

$this->breadcrumbs=array(
	'Auth'=>array('/auth'),
	'Remind',
);
//$user = User::model()->withToken('297aae72cc4d0d068f46a9158469e34d')->find();
//CVarDumper::dump($user);
?>


<!-- <div id="content"> -->
    <?=$this->renderPartial('//layouts/_breadcrumbs')?>
    <div class="container">
		<div class="form">
		<?php $form = $this->beginWidget('ext.yii-booster.widgets.TbActiveForm', array(
			'id'=>'remind-form',
			'action'=>$this->createUrl('auth/remind'),
			'enableClientValidation'=>false,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		)); ?>
			<div class="row">
				<div class="span4 offset4">
					<?php $this->widget(
						'ext.yii-booster.widgets.TbAlert',
						array(
							'block' => true,
							'fade' => true,
							'closeText' => '&times;',
							'userComponentId' => 'user',
						)
					);?>

					<h4>Напомнить пароль</h4>
					<?=$form->textFieldRow($model, 'email', array('class' => 'span3', 'labelOptions' => array('label' => 'Указанный вами при регистрации e-mail:')));?>
					<div class="actions">
						<?=CHtml::submitButton('Отправить', array('class' => 'btn btn-primary'))?>
					</div>
				</div>
			</div>

		<?php $this->endWidget(); ?>
		</div><!-- form -->
	</div>
<!-- </div> -->
