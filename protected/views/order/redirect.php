<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">
		<?php 
			$code = 'if (window.opener) {';
			$code .= 'window.close();';
			if ($redirect) {
				$code .= 'window.opener.$(\'#order-form .nav li.active\').removeClass(\'active\');';
				$code .= 'window.opener.$(\'#order-form\').submit();';
			}
			$code .= '}';
			$code .= 'else {';
			if ($redirect) {
				$code .= '$(\'#order-form .nav li.active\').removeClass(\'active\');';
				$code .= '$(\'#order-form\').submit();';
			}
			$code .= '}';
			echo $code;
		?>
	</script>
</head>
<body>

<h3 id="link"><a href="<?php echo $url; ?>">Click here to return to the application.</a></h3>
<script type="text/javascript">
</script>
</body>
</html>