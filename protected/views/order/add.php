<?php
/* @var $this OrderController */

$this->breadcrumbs=array(
	'Order'=>array('/order'),
	'Add',
);
?>
<h1>Новый заказ</h1>

<?php

// var_dump($model->isValid);

// var_dump($model->attributes);

$this->widget(
	'ext.yii-booster.widgets.TbAlert',
	array(
		'block' => true,
		'fade' => true,
		'closeText' => '&times;', // false equals no close link
		'events' => array(),
		'htmlOptions' => array(),
		'userComponentId' => 'user',
	)
);

$this->widget(
    'bootstrap.widgets.TbDetailView',
    array(
        'data' => $model,
        'attributes' => array(
            array('name' => 'theme'),
            array('name' => 'orderType.name'),
            array('name' => 'discipline'),
            array('name' => 'date'),
            array('name' => 'extra'),
            array('name' => 'phone'),
        ),
    )
);

?>
<p>Стоимость работы пришлём в ближайшее время на e-mail <?php echo $model->email; ?>. В отдельных случаях оценка стоимости может занять сутки.</p>

<?php
if (Yii::app()->user->isGuest) {//TODO переделать логин после добавления заказа на существующий e-mail
	$user = new User(User::SCENARIO_LOGIN);
	if ($model->user->password) {
	    $user->email = $model->user->email;
	}
	$this->renderPartial('//auth/login', array('model' => $user));
}
