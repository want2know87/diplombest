<?php

$url = Yii::app()->request->url;
$seoData = SeoData::model()->find('url=:url', array(':url'=>$url));

//echo "Model={$model->alias}";

$cs = Yii::app()->clientScript;
$cs->registerScript('offer-link', "
  \$('.offer-link').click(
    function(e) {
      var worktype = \$(this).attr('data-type');
      \$('html, body').animate(
        { scrollTop: \$('#order-form').offset().top },
        1000,
        function() {
          \$('#order-form select.worktype').val(worktype).change().blur();
        }
      );
      e.preventDefault();
      e.stopPropagation();
    }
  );"
);

$work = $model->name;
$tagline = "$work на заказ";
$this->npTitle=Yii::app()->name . ' - ' . $work;

$tagline = SeoHelper::h1(
    $url,
    array(
        'defaultH1'    => $tagline,
        'placeholder1' => $work,
    )
);

$this->pageDescription = SeoHelper::descr(
    $url,
    array(
        'defaultDescr' => $this->pageDescription,
        'placeholder'  => $tagline,
    )
);

$this->breadcrumbs=array(
    $model->name,
);
?>

<div id="content">
  <?=$this->renderPartial('//layouts/_breadcrumbs')?>
  <div class="container">
    <h1><?=$tagline?></h1>
    <div class="tab layoutF wi zakaz_tabl">
          <div class="cel alignMiddl">
              <img src="/images/zakaz_img1.png" class="wi" alt="">
          </div>
          <div class="cel re alignTop bag1 zakaz_tabl_content zakaz_tabl_content_left zakaz_tabl_content_left1">
            <?php if ($seoData): ?>
            <?=$seoData->block1?>
            <?php else: ?>
            <h3><?=$tagline?> - значит сэкономить время</h3>
            Обучение в ВУЗе является самой приятной порой в жизни каждого человека. Она оставляет неординарные впечатления, которые запомнятся надолго. Вы заводите новые знакомства и находите друзей, многие из которых будут с Вами всю жизнь, приятно проводите время вместе и радуетесь каждой минуте. Но студенческая жизнь, к сожалению, состоит не только из веселья. Почти каждый семестр возникает необходимость в написании курсовой по какому-либо предмету, из-за чего пропадает все настроение и задор. К счастью, найти выход из положения поможет компания «ДипломБест», в которой Вы можете заказать курсовую и надолго забыть о проблемах, связанных с её написанием и сдачей.
            <?php endif; ?>
          </div>
    </div>
    <div class="tab layoutF wi zakaz_tabl">
          <div class="cel re alignTop bag2 zakaz_tabl_content zakaz_tabl_content_right zakaz_tabl_content_left2">
            <?php if ($seoData): ?>
            <?=$seoData->block2?>
            <?php else: ?>
            <h3><?=$tagline?> — означает быть уверенным в успешной ее сдаче</h3>
            В процессе обучения студентам периодически приходится писать курсовые работы. Они позволяют на практике отобразить полученные знания, воспользоваться приобретёнными навыками и показать своё умение работать с информационными источниками. Написание «курсовой» оформляется в соответствии со строгими требованиями. Но в процессе работы у студентов часто начинают возникать различные трудности. То времени не хватает на походы по библиотекам и бесконечное чтение книг, то становится непонятным дальнейшее оформлением работы. Если Вам пришлось столкнуться с такой ситуацией, то прекрасным её разрешением будет заказать курсовую в компании «ДипломБест».
            <?php endif; ?>
          </div>
          <div class="cel alignMiddl">
              <img src="/images/zakaz_img2.png" class="wi" alt="">
          </div>
    </div>
    <div class="tab layoutF wi zakaz_tabl">
          <div class="cel alignMiddl">
              <img src="/images/zakaz_img3.png" class="wi" alt="">
          </div>
          <div class="cel re alignTop bag3 zakaz_tabl_content zakaz_tabl_content_left zakaz_tabl_content_left3">
            <?php if ($seoData): ?>
            <?=$seoData->block3?>
            <?php else: ?>
            <h3><?=$tagline?> в компании «ДипломБест»</h3>
            На первый взгляд может показаться, что написание курсовой работы — это не очень важное занятие, которое и в сравнение не идёт с подготовкой дипломной работы. Но это не так. Курсовая является Вашим лицом в глазах преподавателя. Её успешное написание может не только повысить Ваш рейтинг, но и поможет заслужить авторитет на факультете.
            <br><br>
            Поэтому лучше воспользоваться услугами компании «ДипломБест» и заказать курсовую здесь. В компании работают только настоящие профессионалы, которые имеют огромный опыт выполнения курсовых работ по различным тематикам и направлениям. Здесь гарантируют, что Ваша работа будет выполнена точно в срок и в соответствии со всеми требованиями, которые поставлены к курсовым работам. Чёткие выводы и грамотное написание текста Вашей «курсовой» не оставит равнодушным даже самого требовательного руководителя. Обращайтесь в «ДипломБест» чтобы получить индивидуальную качественную курсовую работу по доступной цене.
            <?php endif ?>
          </div>
    </div>
    <div class="row" id="order">
      <div class="span12 center order-title">
          <div class="h1_style center">Хотите сейчас узнать стоимость работы?</div>
        <div class="h3_style center">Заполните форму ниже!</div>
      </div>
      <!--<div class="span6 offset3">-->
      <div>
        <?php $this->renderPartial('//order/form', array('model' => $order)); ?>
      </div>
    </div>
  </div>
</div>
