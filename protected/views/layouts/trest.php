<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
    <meta name="description"  content="<?php echo CHtml::encode($this->pageDescription);?>" />
    <meta name="keywords"  content="<?php echo CHtml::encode($this->pageKeywords);?>" />
    <meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/slider.min.css" />
	<link href="http://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/trest.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
	<title><?php echo CHtml::encode($this->npTitle); ?></title>
	<script type="text/javascript" src="/assets/407f06b2/slider.min.js"></script>
	<script type="text/javascript" src="/assets/407f06b2/main.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
</head>

<body>

<div id="page-wrap">
<div class="container" id="page">

	<div id="header" class="row">
		<div class="line span12"></div>
		<div id="logo" class="span4"><?php echo CHtml::image('/images/logo.png', Yii::app()->name); ?></div>
		<div class="contacts span4">

		    <div><a href="tel:+7 (499) 677-68-60">+7 (499) 677-68-60</a> (для&nbsp;абонентов&nbsp;Москвы)</div>

				<?php if (Yii::app()->config->get('contacts.phone.beeline')): ?>
				     <div><a href="tel:<?=Yii::app()->config->get('contacts.phone.beeline')?>"><?=Yii::app()->config->get('contacts.phone.beeline')?></a> (для&nbsp;абонентов&nbsp;Билайн)</div>
			    <?php endif; ?>

            <div><a href="tel:+7 (987) 654-577-5 ">+7 (987) 654-577-5</a> (для&nbsp;абонентов&nbsp;МТС)</div>

            <?php if (Yii::app()->config->get('contacts.phone.megafon')): ?>
				<div><a href="tel:<?=Yii::app()->config->get('contacts.phone.megafon')?>"><?=Yii::app()->config->get('contacts.phone.megafon')?></a> (для&nbsp;абонентов&nbsp;Мегафон)</div>
			<?php endif; ?>


			<div><img src="http://wwp.icq.com/scripts/online.dll?icq=<?=Yii::app()->config->get('contacts.icq')?>&amp;img=5" border="0" /> <?=Yii::app()->config->get('contacts.icq')?></div>
		</div>
		<div class="contacts span4">
			<div>
			<?php if (Yii::app()->user->isGuest): ?>
				<a href="#" data-target="#modal" data-toggle="modal"><i class="icon-user"></i> Войти в личный кабинет</a>
			<?php else: ?>
				<a href="<?php echo $this->createUrl('//cabinet/default/index'); ?>"><i class="icon-user"></i> Личный кабинет</a> <?php echo Yii::app()->user->getRecord()->username; ?>
			<?php endif; ?>
			</div>
		</div>
	</div><!-- header -->

	<div id="mainmenu" class="row">
		<div class="span12">
		<?php
		$menuItems = array(
			'home' => array('url'=>array('/site/index'), 'icon' => 'home'),
			'about' => array('label'=>'о компании', 'url'=>array('/site/page', 'view'=>'about')),
			'scheme' => array('label'=>'схема работы', 'url'=>array('/site/page', 'view'=>'scheme')),
			'payment' => array('label'=>'оплата', 'url'=>array('/site/page', 'view' => 'payment')),
			'authors' => array('label'=>'авторам', 'url'=>array('/site/authors'), 'visible' => false),
			'contact' => array('label'=>'контакты', 'url'=>array('/site/contact')),
			'logout' => array('label'=>'выход ('.(Yii::app()->user->isGuest ? '' : Yii::app()->user->getRecord()->username).')', 'url'=>array('/auth/logout'), 'visible'=>!Yii::app()->user->isGuest)
		);
		$this->widget('bootstrap.widgets.TbMenu',array(
			'type' => 'pills',
			'items'=> $menuItems,
		));
		?>
		</div>
	</div><!-- mainmenu -->
	<?php if (0 && isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<div class="row">
		<div class="span12">
		<?php
		if ($this->banner) {
		    echo CHtml::image($this->banner);
		}
		?>
		</div>
	</div>

	<?php echo $content; ?>

	<div class="clear"></div>

</div><!-- page -->

<div id="push"></div>
</div><!-- page-wrap -->

<div id="footer">
<div class="container">
<div class="row">
	<div class="span4">
		<?php echo CHtml::image('/images/logo-bw.png', '', array('class' => 'logo')); ?><br />
		&copy; Все права защищены - Diplombest.com
	</div>
	<div class="span8 inl">
		<div class="pull-right clear-right">
		<?php
		$menuItems['authors']['visible'] = true;
		$this->widget('bootstrap.widgets.TbMenu',array(
			'type' => 'pills',
			'items'=> $menuItems,
			'htmlOptions' => array(
			)
		)); ?>
		</div>
	</div>
	<div class="span8 teRig">
		<div><a href="tel:+7 (499) 677-68-60">+7 (499) 677-68-60</a> (для&nbsp;абонентов&nbsp;Москвы)</div>
		<div><a href="tel:+7 (909) 384-555-0">+7 (909) 384-555-0</a> (для&nbsp;абонентов&nbsp;Билайн)</div>
	</div>
</div>


</div>
</div><!-- footer -->

<?php

$cs = Yii::app()->getClientScript();
//$cs->registerScriptFile('/js/the-modal/jquery.the-modal.js');
//$cs->registerCssFile('/js/the-modal/the-modal.css');

/*$cs->registerScript('the-modal-init', "
$('#modal button.close').on('click', function(e) {
	e.preventDefault();
	$.modal().close();
});
$('a[data-toggle=modal]').on('click', function(e) {
	e.preventDefault();
	var \$this = $(this), href = \$this.attr('href');

	$('#modal').modal().open({
		cloning: false,
		onOpen: !href || /#/.test(href) ? null :
				function(el, options) {
					\$('#modal .modal-body').html('<i class=\"icon-spinner icon-spin icon-large\"></i>');
					\$('#modal .modal-body').load(href);
				},
		onClose: function() {\$('#modal').hide();}
	});
});

");*/

$cs->registerScript('modal-init', "
$('#modal').on('show', function(e) {
	\$this = $(this);
	if (!\$this.data('modal').isShown && \$this.data('modal').options.remote) {
		\$this.find('.modal-title').html('Идёт загрузка...');
		\$this.find('.modal-body').html('<i class=\"icon-spinner icon-spin icon-large\"></i>');
	}
});
");

?>
<div id="modal" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<div class="modal-title"><?php if (Yii::app()->user->isGuest): ?><h3><i class="icon-user"></i> Личный кабинет</h3><?php endif; ?></div>
	</div>
	<div class="modal-body">
		<?php if (Yii::app()->user->isGuest): ?>
		<?=$this->renderPartial('//auth/login', array('model' => new User(User::SCENARIO_LOGIN)));?>
		<?php endif; ?>
	</div>
</div>
</body>
</html>
