<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
    <meta name="description"  content="<?php echo CHtml::encode($this->pageDescription);?>" />
    <meta name="keywords"  content="<?php echo CHtml::encode($this->pageKeywords);?>" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/slider.min.css" />
	<link href="http://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
	<title><?php echo CHtml::encode($this->npTitle); ?></title>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendors/walkway.min.js"></script>
	<script type="text/javascript" src="/assets/407f06b2/slider.min.js"></script>
	<script type="text/javascript" src="/assets/407f06b2/main.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TMQFVLL');</script>
<!-- End Google Tag Manager -->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMQFVLL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page-wrap">
<div class="container" id="page">

	<div id="header" class="row">
		<div class="line span12"></div>
		<div id="logo" class="span4"><?php echo CHtml::image('/images/logo.png', Yii::app()->name); ?></div>
	</div><!-- header -->

	<?php if (/*0 && */isset($this->breadcrumbs)):?>
		<?php /*$this->widget('bootstrap.widgets.TbBreadcrumbs', array(*/ ?>
	    <?php $this->widget('SchemaBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<div class="row">
		<div class="span12">
		<?php
		if ($this->banner) {
		    echo CHtml::image($this->banner);
		}
		?>
		</div>
	</div>

	<?php echo $content; ?>

	<div class="clear"></div>

</div><!-- page -->

<div id="push"></div>
</div><!-- page-wrap -->

<div id="footer">
<div class="container">
<div class="row">
	<div class="span4">
		<?php echo CHtml::image('/images/logo-bw.png', '', array('class' => 'logo')); ?><br />
		&copy; Все права защищены - Diplombest.com
		<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t44.5;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
//--></script><!--/LiveInternet-->
	</div>
	<div class="span8 inl">
		<div class="pull-right clear-right">
		<?php
		$menuItems['authors']['visible'] = true;
		$this->widget('bootstrap.widgets.TbMenu',array(
			'type' => 'pills',
			'items'=> $menuItems,
			'htmlOptions' => array(
			)
		)); ?>
		</div>
	</div>
	<div class="span8 teRig">
		<div><a href="tel:+7 (499) 677-68-60">+7 (499) 677-68-60</a> (для&nbsp;абонентов&nbsp;Москвы)</div>
		<div><a href="tel:+7 (909) 384-555-0">+7 (909) 384-555-0</a> (для&nbsp;абонентов&nbsp;Билайн)</div>
	</div>
</div>


</div>
</div><!-- footer -->

<?php

$cs = Yii::app()->getClientScript();
//$cs->registerScriptFile('/js/the-modal/jquery.the-modal.js');
//$cs->registerCssFile('/js/the-modal/the-modal.css');

/*$cs->registerScript('the-modal-init', "
$('#modal button.close').on('click', function(e) {
	e.preventDefault();
	$.modal().close();
});
$('a[data-toggle=modal]').on('click', function(e) {
	e.preventDefault();
	var \$this = $(this), href = \$this.attr('href');

	$('#modal').modal().open({
		cloning: false,
		onOpen: !href || /#/.test(href) ? null :
				function(el, options) {
					\$('#modal .modal-body').html('<i class=\"icon-spinner icon-spin icon-large\"></i>');
					\$('#modal .modal-body').load(href);
				},
		onClose: function() {\$('#modal').hide();}
	});
});

");*/

$cs->registerScript('modal-init', "
$('#modal').on('show', function(e) {
	\$this = $(this);
	if (!\$this.data('modal').isShown && \$this.data('modal').options.remote) {
		\$this.find('.modal-title').html('Идёт загрузка...');
		\$this.find('.modal-body').html('<i class=\"icon-spinner icon-spin icon-large\"></i>');
	}
});
");

?>
<div id="modal" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<div class="modal-title"><?php if (Yii::app()->user->isGuest): ?><h3><i class="icon-user"></i> Личный кабинет</h3><?php endif; ?></div>
	</div>
	<div class="modal-body">
		<?php if (Yii::app()->user->isGuest): ?>
		<?=$this->renderPartial('//auth/login', array('model' => new User(User::SCENARIO_LOGIN)));?>
		<?php endif; ?>
	</div>
</div>
</body>
</html>
