<script>
$(function() {
	if (title = $('#modal .modal-body .modal-title')) {
		$('#modal .modal-header .modal-title').replaceWith(title);
	}
	$('#modal > .modal-footer').remove();
	if (footer = $('#modal .modal-body .modal-footer').detach()) {
		$('#modal').append(footer);
		$('#modal .modal-footer *[type=submit]').on('click', function() {
			$('#modal form').submit();
		});
	}
	$('#modal form').submit(function (e) {
		$('#modal .modal-body').load(
			$(this).attr('action'), 
			$(this).serializeArray(),
			function(responseText, textStatus, XMLHttpRequest) {
				console.log(XMLHttpRequest);
			}
		);
		e.preventDefault();
		return false;
	});
});
</script>
<?php echo $content; ?>