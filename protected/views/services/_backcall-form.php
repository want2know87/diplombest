<div id="backcall-modal" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <div class="modal-title"><h3>Заказать звонок</h3></div>
    </div>
    <div class="modal-body">
                <div class="form">
                    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                        'id'=>'backcall-form',
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                    )); ?>

                    <p class="note">Оставьте номер телефона и мы перезвоним вам.</p>

                    <?php echo $form->textFieldRow($backcallModel, 'name'); ?>
                    <?php echo $form->MaskedTextFieldRow($backcallModel, 'phone', '+7 999 999 9999'); ?>

                    <div class="buttons">
                            <?php echo CHtml::submitButton('Отправить', array('class' => 'btn btn-primary')); ?>
                    </div>

                    <?php $this->endWidget(); ?>
                </div>
    </div>
</div>
