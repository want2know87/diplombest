<section class="page-block">
    <h3 class="page-block__title">
        Контакты
    </h3>
    <p class="page-block__desc page-block__desc--margin">
        Свяжитесь с нами по номеру
    </p>
    <div class="page-block__phone">
        <div class="page-block__phone-item">+7 987 654 57 75</div>
        <div class="page-block__phone-item page-block__phone-item--light">+7 987 654 57 75</div>
    </div>
    <p class="page-block__desc">
        Либо напишите нам Вконтакте
    </p>
    <a href="https://m.vk.com/write33414448" class="page-block__link" target="_blank">
        <?php echo CHtml::image("/images/new_img/svg/vk.svg")?>
    </a>
</section>
