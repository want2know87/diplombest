<!-- form -->
<div class="row row-grey" id="order">
    <div class="span12 center order-title order-title-non-border">
        <div class="h1_style center">Хотите сейчас узнать стоимость работы?</div>
        <div class="h3_style center">Заполните форму ниже!</div>
    </div>
    <div class="">
        <?php $this->renderPartial('_form', array('model' => new Order('add'), 'orderType' => $orderType)); ?>
    </div>
</div>
<!-- /form -->
