<?php
$this->widget(
    'ext.yii-booster.widgets.TbAlert',
    array(
        'block' => true,
        'fade' => true,
        'closeText' => '&times;', // false equals no close link
        'events' => array(),
        'htmlOptions' => array(),
        'userComponentId' => 'user',
    )
);
?>
