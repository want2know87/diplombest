<?php

$url = Yii::app()->request->url;
$h1 = '';
$h1 = SeoHelper::h1(
    $url,
    array(
        'defaultH1'    => $h1,
        'placeholder1' => $orderType->title,
        'placeholder2' => $branch->name,
        'placeholder3' => $discipline->name,
    )
);

$title = $discipline->name . ' - ' . $this->npTitle;
$title = SeoHelper::title(
    $url,
    array(
        'defaultTitle' => $title,
        'placeholder1' => $h1,
        'placeholder2' => $h1,
        'placeholder3' => $h1,
    )
);
$this->npTitle = $title;

$this->pageDescription = SeoHelper::descr(
    $url,
    array(
        'defaultDescr' => $this->pageDescription,
        'placeholder'  => $h1,
    )
);

$this->breadcrumbs=array(
    $orderType->name => array('/services/index', 'order_type_alias' => $orderType->alias),
    $branch->name => array('/services/branch', 'order_type_alias' => $orderType->alias, 'branch_id' => Yii::app()->request->getParam('branch_id')),
    $discipline->name,
);
?>
<div id="content">
    <?=$this->renderPartial('//layouts/_breadcrumbs')?>
    <!-- begin of denis layout-->
    <div class="container">
        <!-- banner-->
    <div class="row">
        <section class="banner banner--target span12">
         <div class="banner__title">
            Вы в одном шаге от сотрудничества, заполните поля оставшиеся ниже
         </div>
         <p class="banner__desc">
            Заказ реферата, диплома или курсовой освобождает вам большое количество времени.
         </p>
         <a href="#"  data-target="#backcall-modal" data-toggle="modal" class="banner__btn">Узнать цену</a>
        </section>
    </div>
    <!-- end of banner-->

    <?php $this->renderPartial('_success-alert', array(
    )); ?>

    <!-- selection-->
    <?php if ($h1): ?>
    <section class="selection">
        <h1 class="selection__title"><?=$h1?></h1>
    </section>
    <?php endif; ?>

    <!-- page-block -->

    <?php $this->renderPartial('_form-wrapper', array('orderType' => $orderType)); ?>

    <?php $this->renderPartial('_price-period-block', array(
        'orderType' => $orderType,
    )); ?>

    <?php $this->renderPartial('_guarantees', array()); ?>

    <?php $this->renderPartial('_contacts-block', array()); ?>

    <!-- end of page-block-->
    <!-- pay slider -->
    <?php $this->renderPartial('_pay-block', array()); ?>
    <!-- end of pay-slider -->
    <!-- page-block -->
    <section class="page-block">
        <h3 class="page-block__title">
            Реквизиты
        </h3>
        <p class="page-block__desc">
            Реквизиты для оплаты есть в личном кабинете или можно запросить у персонального менеджера
        </p>
    </section>
    <!-- end of page-block-->

    <?php $this->renderPartial('_process-block', array()); ?>

    <?php $this->renderPartial('_benefits', array()); ?>

    </div>
    <!-- end of denis layout-->
</div>

<?php

$cs = Yii::app()->clientScript;
$cs->registerScript('setOrderTypeId', <<<JS
$('#Order_orderType_id').val({$orderType->id});
$('#Order_discipline').val("{$discipline->name}");
JS
);
?>

<?php $this->renderPartial('_backcall-form', array(
    'backcallModel' => $backcallModel,
)); ?>
