<!-- process -->
<section class="process">
    <h3 class="process__title">Как мы работаем</h3>
    <ul class="process__list">
        <li class="process__item">
            <div class="process__img">
                <img src="/images/icon5.png" alt="">
            </div>
            <p class="process__desc">
                Оформляете заказ работы на сайте
            </p>
        </li>
        <li class="process__item">
            <div class="process__img">
                <img src="/images/icon2.png" alt="">
            </div>
            <p class="process__desc">
                Вносите аванс 50% от стоимости
            </p>
        </li>
        <li class="process__item">
            <div class="process__img">
                <img src="/images/icon4.png" alt="">
            </div>
            <p class="process__desc">
                Мы приступаем к выполнению работы
            </p>
        </li>
        <li class="process__item">
            <div class="process__img">
                <img src="/images/icon6.png" alt="">
            </div>
            <p class="process__desc">
                В срок сдачи высылаем часть работы для ознакомления
            </p>
        </li>
        <li class="process__item">
            <div class="process__img">
                <img src="/images/icon1.png" alt="">
            </div>
            <p class="process__desc">
                Вы оплачиваете вторую часть и получаете работу полностью
            </p>
        </li>
        <li class="process__item">
            <div class="process__img">
                <img src="/images/icon3.png" alt="">
            </div>
            <p class="process__desc">
                Если будут замечания, мы устраним их БЕСПЛАТНО
            </p>
        </li>
    </ul>
</section>
<!-- end of process -->
