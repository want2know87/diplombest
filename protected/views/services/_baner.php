<?php

$title = $orderType->title;
?>

<!-- banner-->
<div class="row">
    <section class="banner span12">
     <div class="banner__title">
        <?php echo $title; ?> по любой дисциплине
     </div>
     <p class="banner__desc">
        Заказ реферата, диплома или курсовой освобождает вам большое количество времени.
     </p>
     <a href="#" data-target="#backcall-modal" data-toggle="modal" class="banner__btn">Узнать цену</a>
    </section>
</div>
<!-- end of banner-->
