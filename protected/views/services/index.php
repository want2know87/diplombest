<?php

$url = Yii::app()->request->url;
$h1 = 'Выберите специализацию';

$h1 = SeoHelper::h1(
    $url,
    array(
        'defaultH1'    => $h1,
        'placeholder1' => $orderType->title,
    )
);

$title = 'Выберите специализацию' . ' - ' . $this->npTitle;
$title = SeoHelper::title(
    $url,
    array(
        'defaultTitle' => $title,
        'placeholder1' => $h1,
    )
);
$this->npTitle = $title;

$this->pageDescription = SeoHelper::descr(
    $url,
    array(
        'defaultDescr' => $this->pageDescription,
        'placeholder'  => $h1,
    )
);
$this->breadcrumbs=array(
    $orderType->name,
);
?>

<!-- begin of denis layout-->
<div id="content">
    <?=$this->renderPartial('//layouts/_breadcrumbs')?>
    <div class="container">
        <?php $this->renderPartial('_baner', array(
            'orderType' => $orderType,
        )); ?>

        <?php $this->renderPartial('_success-alert', array(
        )); ?>

        <!-- selection-->
            <section class="selection">
                <h1 class="selection__title"><?=$h1?></h1>
                <div class="selection__wrapper">
                    <?php foreach ($branches as $branch): ?>
                    <a href="<?php echo $this->createUrl('services/branch', array('order_type_alias' => $orderType->alias, 'branch_id' => $branch->getBranchAlias())); ?>" class="selection__item"><?php echo $branch->name; ?></a>
                    <?php endforeach; ?>
                </div>
            </section>
        <!-- end of selection-->
        <!-- page-block -->
        <?php $this->renderPartial('_price-period-block', array(
            'orderType' => $orderType,
        )); ?>
        <!-- end of page-block-->

        <?php $this->renderPartial('_pay-block', array()); ?>

        <?php $this->renderPartial('_process-block', array()); ?>

        <!-- end page-block -->
        <?php $this->renderPartial('_contacts-block', array()); ?>
        <!-- end of page-block -->

        <?php $this->renderPartial('_form-wrapper', array('orderType' => $orderType)); ?>

        <?php $this->renderPartial('_benefits', array()); ?>

    </div>
    <!-- end of denis layout-->
</div>

<?php

$cs = Yii::app()->clientScript;
$cs->registerScript('setOrderTypeId', <<<JS
$('#Order_orderType_id').val({$orderType->id});
JS
);
?>

<?php $this->renderPartial('_backcall-form', array(
    'backcallModel' => $backcallModel,
)); ?>
