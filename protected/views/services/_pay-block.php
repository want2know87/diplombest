<!-- pay slider -->
<h3>Оплата</h3>
<p>Перевод на расчетный счет в любых коммерческих банках, а так же в терминалах и банкоматах</p>
<div class="page-slider">
    <div class="pay-row swiper-container pay-container-js">
        <div class="pay-list swiper-wrapper">
            <div class="pay-item swiper-slide">
                <div class="pay-item__img">
                    <img src="/images/img-sb.gif" alt=""></div>
                <div class="pay-item__popup"><span>Для осуществления операции перевода с использованием карточки отправителю достаточно знать только полный номер карточки получателя денежных средств (16-значный номер на лицевой стороне карточки).</span></div>
            </div>
            <div class="pay-item swiper-slide">
                <div class="pay-item__img">
                    <img src="/images/img-alfa.gif" alt=""></div>
                <div class="pay-item__popup"><span>У нашей компании есть расчетный счет в Альфа-банке. Перевести денежные средства Вы можете через отделение любого банка, а также при помощи Альфа-Клик или через банкоматы.</span></div>
            </div>
            <div class="pay-item swiper-slide">
                <div class="pay-item__img">
                    <img src="/images/img-vtb.gif" alt=""></div>
                <div class="pay-item__popup"><span>Специально для Вашего удобства мы выбрали три самых крупных банка РФ. Перевод на счет ВТБ 24 возможен после оформления заказа. Реквизиты будут присланы Вам персональным менеджером.</span></div>
            </div>
            <div class="pay-item swiper-slide">
                <div class="pay-item__img">
                    <img src="/images/img-qiwi.gif" alt="">
                </div>
                <div class="pay-item__popup"><span>Перевод на наш счет возможен через платежные терминалы QIWI, а также при помощи мобильного приложения. Транзакции абсолютно безопасны и просты. Примерное время зачисления до 24 часов.</span></div>
            </div>

            <div class="pay-item swiper-slide">
                <div class="pay-item__img">
                    <img src="/images/img-sv.gif" alt="">
                </div>
                <div class="pay-item__popup"><span>После того, как Вам выставят счет на оплату, Вы можете внести наличные деньги через кассу салона связи. Оператор выдаст Вам чек. Услуга доступна на всей территории РФ.</span></div>
            </div>
            <div class="pay-item swiper-slide">
                <div class="pay-item__img">
                    <img src="/images/img-evro.gif" alt="">
                </div>
                <div class="pay-item__popup"><span>Салоны связи всегда находятся в ближайшей доступности. Оплату можно совершить через кассира-операциониста. Зачисление платежа происходит мгновенно.</span></div>
            </div>
            <div class="pay-item swiper-slide">
                <div class="pay-item__img">
                    <img src="/images/img-mtc.gif" alt="">
                </div>
                <div class="pay-item__popup"><span>Для перевода достаточно получить реквизиты. Они будут доступны в личном кабинете, либо Вы можете запросить их у персонального менеджера.</span></div>
            </div>
            <div class="pay-item swiper-slide">
                <div class="pay-item__img">
                    <img src="/images/img-bil.gif" alt="">
                </div>
                <div class="pay-item__popup"><span>Для перевода достаточно получить реквизиты. Они будут доступны в личном кабинете, либо Вы можете запросить их у персонального менеджера.</span></div>
            </div>
            <div class="pay-item swiper-slide">
                <div class="pay-item__img">
                    <img src="/images/img-ya.gif" alt="">
                </div>
                <div class="pay-item__popup"><span>Вы можете удобно внести оплату со своего личного электронного кошелька. Зачисление мгновенное.</span></div>
            </div>
            <div class="pay-item swiper-slide">
                <div class="pay-item__img"><img src="/images/img-wm.gif" alt=""></div>
                <div class="pay-item__popup"><span>Наша компания имеет аттестат WebMoney. Перевод возможен с Вашего личного кошелька. Зачисление мгновенное.</span></div>
            </div>
        </div>
    </div>
    <div class="page-slider__prev swiper-button-prev"></div>
    <div class="page-slider__next swiper-button-next"></div>
</div>
<!-- end of pay-slider -->
