<?php

$orderTypeLines = preg_split('/[\r\n]+/', $orderType->descr, -1, PREG_SPLIT_NO_EMPTY);
$orderTypePrice = isset($orderTypeLines[0]) ? $orderTypeLines[0] : null;
$orderTypePeriod = isset($orderTypeLines[1]) ? $orderTypeLines[1] : null;
?>

<section class="page-block">
    <h3 class="page-block__title">
        Сроки и цены
    </h3>
    <p class="page-block__desc page-block__desc--margin">
        Предлагаем вам ознакомиться с ценами и сроками. Стоимость заказа дипломной, курсовой, контрольной или реферата меняется исходя из степени сложности  задания, объема и срока выполнения работы.
    </p>

    <div class="page-block__wrapper">
        <h4 class="page-block__title order-type__h4" style="margin-top: 60px;"><?=$orderType->name?></h4>
        <?php if ($orderType->getSvgIcon()): ?>
        <div class="page-block__icon"
             style="background: url(/images/new_img/svg/<?=$orderType->getSvgIcon()?>) no-repeat 0px 0px; background-size: cover;"
        >
        </div>
        <?php endif; ?>
    </div>


    <div class="page-block__price">
        <div class="page-block__price-item">
            <span>Цена</span>: <?php echo $orderTypePrice; ?>
        </div>
        <div class="page-block__price-item">
            <span>Время</span>: <?php echo $orderTypePeriod; ?>
        </div>
    </div>
</section>
