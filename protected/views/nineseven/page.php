<!-- nineseven/page -->
<!-- Новый блок -->
<div id="content">
	<section class="work-type">
		 <div class="container">
		 	<h2 class="work-type__title">Типы студенческих работ</h2>
			<div class="work-type__wrapper">
				<div class="work-type__block">
					<div class="work-type__heading">Дипломы</div>
					<div class="work-type__about">
						<div class="work-type__about-price">
							<div class="work-type__about-header">Цена</div>
							<div class="work-type__about-sum">от 6 500 р.</div>
						</div>
						<div class="work-type__about-price">
							<div class="work-type__about-header">Время</div>
							<div class="work-type__about-sum">от 3 дней</div>
						</div>
					</div>
					<ul class="work-type__list">
						<li class="work-type__item">предоплата 50%</li>
						<li class="work-type__item">бесплатные доработки</li>
						<li class="work-type__item">предоплата 50%</li>
						<li class="work-type__item">гарантии, договор</li>
						<li class="work-type__item">aвтор:преподаватель,кандидат наук</li>
					</ul>
					<a href="#" class="work-type__block-link">Заказать</a>
				</div>
				<div class="work-type__block">
					<div class="work-type__heading">Курсовые</div>
					<div class="work-type__about">
						<div class="work-type__about-price">
							<div class="work-type__about-header">Цена</div>
							<div class="work-type__about-sum">от 1 500 р.</div>
						</div>
						<div class="work-type__about-price">
							<div class="work-type__about-header">Время</div>
							<div class="work-type__about-sum">от 2 дней</div>
						</div>
					</div>
					<ul class="work-type__list">
						<li class="work-type__item">предоплата 50%</li>
						<li class="work-type__item">бесплатные доработки</li>
						<li class="work-type__item">предоплата 50%</li>
						<li class="work-type__item">гарантии, договор</li>
						<li class="work-type__item">aвтор:преподаватель,кандидат наук</li>
					</ul>
					<a href="#" class="work-type__block-link">Заказать</a>
				</div>
				<div class="work-type__block">
					<div class="work-type__heading">Отчеты по практике</div>
					<div class="work-type__about">
						<div class="work-type__about-price">
							<div class="work-type__about-header">Цена</div>
							<div class="work-type__about-sum">от 1 500 р.</div>
						</div>
						<div class="work-type__about-price">
							<div class="work-type__about-header">Время</div>
							<div class="work-type__about-sum">от 2 дней</div>
						</div>
					</div>
					<ul class="work-type__list">
						<li class="work-type__item">предоплата 50%</li>
						<li class="work-type__item">бесплатные доработки</li>
						<li class="work-type__item">предоплата 50%</li>
						<li class="work-type__item">гарантии, договор</li>
						<li class="work-type__item">aвтор:преподаватель,кандидат наук</li>
					</ul>
					<a href="#" class="work-type__block-link">Заказать</a>
				</div>
				<div class="work-type__block">
					<div class="work-type__heading">Рефераты</div>
					<div class="work-type__about">
						<div class="work-type__about-price">
							<div class="work-type__about-header">Цена</div>
							<div class="work-type__about-sum">от 1 500 р.</div>
						</div>
						<div class="work-type__about-price">
							<div class="work-type__about-header">Время</div>
							<div class="work-type__about-sum">от 2 дней</div>
						</div>
					</div>
					<ul class="work-type__list">
						<li class="work-type__item">предоплата 50%</li>
						<li class="work-type__item">бесплатные доработки</li>
						<li class="work-type__item">предоплата 50%</li>
						<li class="work-type__item">гарантии, договор</li>
						<li class="work-type__item">aвтор:преподаватель,кандидат наук</li>
					</ul>
					<a href="#" class="work-type__block-link">Заказать</a>
				</div>
			</div>
			<div class="work-type__show">
				<a href="#" class="work-type__link">Показать все типы работ</a>
			</div>
		 </div>
	</section>
</div>