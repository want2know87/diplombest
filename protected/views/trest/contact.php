<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

//$this->npTitle=Yii::app()->name . ' - Обратная связь';
$this->breadcrumbs=array(
	'Contact',
);
?>

<div class="contacts-padding"></div>
<div class="contacts-wrap">
	<div class="contacts-wrap__header">Diplombest.com</div>
	<div class="contacts-wrap__txt">Удобный онлайн сервис помощи для заказа студенческих работ</div>
	<div class="contacts-wrap__col">
		<div class="contacts-wrap__col-head">
			<p>105005 Москва ул. Радио д 5 стр 3</p>
			<p>(не является офисом для клиентов) </p>
			<p><a href="mailto:a@diplombest.com">a@diplombest.com </a></p>
		</div>
		<div class="contacts-wrap__col-phone">
			<p><span class="ico-info"><img src="/images/ico-gor.gif" alt=""></span><a href="tel:+74996776860">+7 (499) 677-68-60</a></p>
			<p><span class="ico-info"><img src="/images/ico-bil.gif" alt=""></span><a href="tel:+79093845550">+7 (909) 384-555-0</a></p>
			<p><span class="ico-info"><img src="/images/ico-mts.gif" alt=""></span><a href="tel:+79876545775">+7 (987) 654-577-5</a></p>
			<p><span class="ico-info"><img src="/images/ico-meg.gif" alt=""></span><a href="tel:+79377425558">+7 (937) 742-555-8</a></p>
		</div>
		<p><span class="ico-info"><img src="/images/ico-icq.gif" alt=""></span>370014682, 650929011</p>
		<p><span class="ico-info"><img src="/images/ico-sky.gif" alt=""></span>kreichi1985</p>
		<p><span class="ico-info"><img src="/images/ico-vac.gif" alt=""></span>+7 987 654 57 75</p>
		<p><span class="ico-info"><img src="/images/ico-vb.gif" alt=""></span>+7 987 654 57 75</p>
	</div>
	<div class="contacts-wrap__col">
		<div class="contacts-wrap__col-head">
			<p>ИП Крейчи Артем Олегович</p>
			<p>ОГРНИП 314344333000036</p>
			<p>ИНН 344213038401</p>
		</div>
		<div class="contacts-wrap__col-time">
			<p>Пн-пт: 08:00-24:00</p>
			<p>Сб: 09:00-18:00</p>
			<p>Вс: выходной</p>
		</div>
		<p><span class="ico-info"><img src="/images/ico-vk.gif" alt=""></span><a href="">Мы в контакте</a></p>
		<p><span class="ico-info"><img src="/images/ico-inst.gif" alt=""></span><a href="">Мы в инстаграм</a></p>
	</div>
</div>
<div class="matter-form">
	<h1>Задать вопрос</h1>
		<?php if(Yii::app()->user->hasFlash('contact')): ?>

		<div class="flash-success">
			<?php echo Yii::app()->user->getFlash('contact'); ?>
		</div>

		<?php else: ?>

		<div class="form">

			<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'id'=>'contact-form',
				'enableClientValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			)); ?>

				<p class="note">Поля, отмеченные <span class="required">*</span>, обязательны для заполнения.</p>

				<?=$form->errorSummary($model); ?>

				<?=$form->textFieldRow($model, 'name'); ?>
				<?=$form->textFieldRow($model, 'email'); ?>
				<?=$form->textFieldRow($model, 'subject',array('size'=>60,'maxlength'=>128)); ?>
				<?=$form->textAreaRow($model, 'body',array('rows'=>6, 'cols'=>50)); ?>

				<?php if(0 && CCaptcha::checkRequirements()): ?>
					<?php echo $form->labelEx($model,'verifyCode'); ?>
					<div>
					<?php $this->widget('CCaptcha'); ?>
					<?php echo $form->textField($model,'verifyCode'); ?>
					</div>
					<div class="hint">Please enter the letters as they are shown in the image above.
					<br/>Letters are not case-sensitive.</div>
					<?php echo $form->error($model,'verifyCode'); ?>
				<?php endif; ?>

				<div class="buttons">
					<?php echo CHtml::submitButton('Отправить', array('class' => 'btn btn-primary')); ?>
				</div>

			<?php $this->endWidget(); ?>

		</div><!-- form -->

		<?php endif; ?>	

	
</div>

<div class="contacts-padding"></div>
<div class="pay-wrap">
	<div class="pay-wrap__header">Оплата</div>
	<h3>Перевод на расчетный счет в любых коммерческих банках, а так же в терминалах и банкоматах</h3>
	<div class="pay-row">
		<div class="pay-list">
			<div class="pay-item">
				<div class="pay-item__img"><img src="/images/img-sb.gif" alt=""></div>
				<div class="pay-item__popup"><span>Для осуществления операции перевода с использованием карточки отправителю достаточно знать только полный номер карточки получателя денежных средств (16-значный номер на лицевой стороне карточки).</span></div>
			</div>
			<div class="pay-item">
				<div class="pay-item__img"><img src="/images/img-alfa.gif" alt=""></div>
				<div class="pay-item__popup"><span>У нашей компании есть расчетный счет в Альфа-банке. Перевести денежные средства Вы можете через отделение любого банка, а также при помощи Альфа-Клик или через банкоматы.</span></div>
			</div>
			<div class="pay-item">
				<div class="pay-item__img"><img src="/images/img-vtb.gif" alt=""></div>
				<div class="pay-item__popup"><span>Специально для Вашего удобства мы выбрали три самых крупных банка РФ. Перевод на счет ВТБ 24 возможен после оформления заказа. Реквизиты будут присланы Вам персональным менеджером.</span></div>
			</div>
			<div class="pay-item">
				<div class="pay-item__img"><img src="/images/img-qiwi.gif" alt=""></div>
				<div class="pay-item__popup"><span>Перевод на наш счет возможен через платежные терминалы QIWI, а также при помощи мобильного приложения. Транзакции абсолютно безопасны и просты. Примерное время зачисления до 24 часов.</span></div>
			</div>
		</div>
	</div>

	<h3>Салоны связи</h3>
	<div class="pay-row">
		<div class="pay-list">
			<div class="pay-item">
				<div class="pay-item__img"><img src="/images/img-sv.gif" alt=""></div>
				<div class="pay-item__popup"><span>После того, как Вам выставят счет на оплату, Вы можете внести наличные деньги через кассу салона связи. Оператор выдаст Вам чек. Услуга доступна на всей территории РФ.</span></div>
			</div>
			<div class="pay-item">
				<div class="pay-item__img"><img src="/images/img-evro.gif" alt=""></div>
				<div class="pay-item__popup"><span>Салоны связи всегда находятся в ближайшей доступности. Оплату можно совершить через кассира-операциониста. Зачисление платежа происходит мгновенно.</span></div>
			</div>
			<div class="pay-item">
				<div class="pay-item__img"><img src="/images/img-mtc.gif" alt=""></div>
				<div class="pay-item__popup"><span>Для перевода достаточно получить реквизиты. Они будут доступны в личном кабинете, либо Вы можете запросить их у персонального менеджера.</span></div>
			</div>
			<div class="pay-item">
				<div class="pay-item__img"><img src="/images/img-bil.gif" alt=""></div>
				<div class="pay-item__popup"><span>Для перевода достаточно получить реквизиты. Они будут доступны в личном кабинете, либо Вы можете запросить их у персонального менеджера.</span></div>
			</div>
		</div>
	</div>

	<h3>Электронные деньги</h3>
	<div class="pay-row pay-row--last">
		<div class="pay-list">
			<div class="pay-item">
				<div class="pay-item__img"><img src="/images/img-ya.gif" alt=""></div>
				<div class="pay-item__popup"><span>Вы можете удобно внести оплату со своего личного электронного кошелька. Зачисление мгновенное.</span></div>
			</div>
			<div class="pay-item">
				<div class="pay-item__img"><img src="/images/img-wm.gif" alt=""></div>
				<div class="pay-item__popup"><span>Наша компания имеет аттестат WebMoney. Перевод возможен с Вашего личного кошелька. Зачисление мгновенное.</span></div>
			</div>
		</div>
	</div>
</div>

<div class="contacts-padding"></div>
<div class="scheme">
	<div class="scheme__header">Схема работы</div>
	<div class="scheme-wrap">
		<div class="scheme-wrap__item">
			<div class="scheme-wrap__img"><img src="/images/scheme-1.png" alt=""></div>
			<span>Оформление</span>
			Оформляете заказ работы на сайте
		</div>
		<div class="scheme-wrap__item">
			<div class="scheme-wrap__img"><img src="/images/scheme-2.png" alt=""></div>
			<span>Наша оценка</span>
			Производим подбор специалиста на вашу работу и отправляем вам наше предложение по цене
		</div>
		<div class="scheme-wrap__item">
			<div class="scheme-wrap__img"><img src="/images/scheme-3.png" alt=""></div>
			<span>Аванс</span>
			Вностите аванс 50% от стоимости услуг
		</div>
		<div class="scheme-wrap__item">
			<div class="scheme-wrap__img"><img src="/images/scheme-4.png" alt=""></div>
			<span>Выполнение задания</span>
			мы приступаем к выполнению всей работы
		</div>

		<div class="scheme-wrap__item">
			<div class="scheme-wrap__img"><img src="/images/scheme-5.png" alt=""></div>
			<span>Бесплатные правки</span>
			Если будут замечания, мы устраним их бесплатно
		</div>
		<div class="scheme-wrap__item">
			<div class="scheme-wrap__img"><img src="/images/scheme-6.png" alt=""></div>
			<span>Сдача преподавателю</span>
			Сдаете работу преподаватлю и получаете оценку.
		</div>
		<div class="scheme-wrap__item">
			<div class="scheme-wrap__img"><img src="/images/scheme-7.png" alt=""></div>
			<span>Оплата</span>
			Оплаты второй части суммы и получение вами работы
		</div>
		<div class="scheme-wrap__item">
			<div class="scheme-wrap__img"><img src="/images/scheme-8.png" alt=""></div>
			<span>Срок сдачи</span>
			В срок сдачи автор сдаёт уже готовый заказ, мы проверяем её и высылаем часть для ознакомления
		</div>
	</div>
</div>


