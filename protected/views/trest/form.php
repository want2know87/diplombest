<?php
/* @var $this SiteController */
/* @var $model OrderForm */
/* @var $form CActiveForm */

$cs = Yii::app()->clientScript;

Yii::import('ext.yii-booster.helpers.TbHtml');

$cs->registerScript('order-now', "
    \$('#order-now').click(
        function(e) {
            \$('html, body').animate(
                { scrollTop: \$('#order-form').offset().top }, 
                1000,
                function() {
                    \$('#order-form input:first').select(); 
                }
            );
            e.preventDefault();
            e.stopPropagation();
        }
    );
", CClientScript::POS_READY);

?>

<div id="order-now"><a href="#order-form">Хочу заказать</a></div>
<div class="form-horizontal form-horizontal-grey span12">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'order-form',
    'action'=>$this->createUrl('//order/add'),
    'enableClientValidation'=>true,
    //'enableAjaxValidation'=>Yii::app()->user->isGuest,
    'type'=>'horizontal',
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data'
    ),
)); ?>

    <?php echo $form->errorSummary($model); ?>
<div class="oreder_block_form re bag4">

    <div class="oder_theme">
    <?php  
    echo '
    <div class="marginB6">
        <span class="required">*</span> <div class="color1 inl">'.$model->attributeLabels()['theme'] . '</div>
    </div>
    ';

    echo 
    $form->textFieldRow(
        $model,
        'theme',
        array(
            'data-toggle' => 'tooltip',
            'data-placement' => 'right',
            'data-title' => 'Для оценки стоимости работы обязательно нужно указать тему. Иначе никак.',
            'message' => 'Тему нужно указать',
            'class' => 'wi sbR order_theme'
        )
    )
    ?>
    </div>

    <div class="item_data num1 inl alignTop">
    <?php   
    echo '
    <div class="marginB6">
        <span class="required">*</span> <div class="color1 inl">'.$model->attributeLabels()['orderType_id'] . '</div>
    </div>
    ';

    echo $form->dropDownListRow(
        $model,
        'orderType_id',
        CHtml::listData(OrderType::model()->base()->findAll(), 'id', 'name'),
        array(
            'empty' => '',
            'class' => 'worktype wi',
            'onchange' => CJavaScript::encode("js:
                val = $(this).val();
                \$(this).closest('form').find('.hidden-control-group').each(
                    function(){
                        \$this = \$(this);
                        if (\$this.attr('data-parent') == val) {
                            \$this.find('input').attr('disabled', false);
                            \$this.show(400);
                        } else {
                            \$this.hide(400);
                            \$this.find('input').attr('disabled', true);
                        }
                    }
                );
            "),
            'encode' => false,
            'message' => 'Необходимо указать тип'
        )
    );
    ?>

    <?php 
    $secondaryTypes = OrderType::model()->secondary()->findAll();
    foreach ($secondaryTypes as $secondaryType) {
        echo '<div class="control-group hidden-control-group hide fade in" data-parent="', $secondaryType->parent_id, '"><div class="controls"><label class="checkbox">';
        echo $form->checkbox(
            $model,
            'orderType_secondary',
            array(
                'value' => $secondaryType->id,
                'disabled' => true,
                'data-parent' => $secondaryType->parent_id
            )
        );
        echo $form->label(
            $model,
            'orderType_secondary',
            array(
                'label' => $secondaryType->title,
            )
        );
        echo '</label></div></div>';
    }

    ?>
    </div><div class="item_data num1 inl alignTop">
    <?php 
    echo '
    <div class="marginB6">
        <span class="required">*</span> <div class="color1 inl">'.$model->attributeLabels()['discipline'] . '</div>
    </div>
    ';

    echo $form->typeAheadRow(
        $model,
        'discipline',
        array(
            //'source' => $model->subjects,
            'source' => array_keys(CHtml::listData(Discipline::model()->findAll(), 'name', 'name')),
        )
    );
    ?>
    </div><div class="item_data inl alignTop order_date">
    <?php 
    echo '
    <div class="marginB6">
        <span class="required">*</span> <div class="color1 inl">'.$model->attributeLabels()['date'] . '</div>
    </div>
    ';

    echo $form->datepickerRow(
            $model,
            'date',
            array(
                'options' => array(
                    'title' => 'Дата сдачи работы влияет на стоимость',
                    'language' => 'ru',
                    'format' => 'dd.mm.yyyy',
                    'weekStart' => 1,
                    'beforeShowDay' => 'js:function(date) {return date.valueOf() < (new Date()).valueOf() ? \'disabled\' : \'\';}',
                ),
                'append' => '<label for="' . CHtml::activeId($model, 'date') . '"><i class="icon-calendar"></i></label>',
                'class' => 'input-small'
            )
        ); ?>
    </div>

    <div class="item_data num1 inl alignTop">
        <?php
        if (Yii::app()->user->isGuest || !Yii::app()->user->getRecord(User::SCENARIO_ORDER)->validate('email')) {
            echo '
            <div class="marginB6">
                <span class="required">*</span> <div class="color1 inl">'.$model->attributeLabels()['email'] . '</div>
            </div>
            ';

            echo $form->textFieldRow(
                $model,
                'email',
                array(
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Нигде не публикуется'
                )
            );
        }
        ?>
    </div><div class="item_data num1 inl alignTop">
        <?php
        echo '
        <div class="marginB6">
            <span class="required">*</span> <div class="color1 inl">'.$model->attributeLabels()['phone'] . '</div>
        </div>
        ';

        echo $form->MaskedTextFieldRow(
            $model,
            'phone',
            '+7 999 999 9999',
            array(
                'data-toggle' => 'tooltip',
                'data-placement' => 'right',
                'title' => 'Телефон нужен, чтобы ускорить процесс нашего общения. Нигде не публикуется.'
            )
        );
        ?>
    </div><div class="item_data inl alignTop order_date">
        <div class="marginB6">
            <div class="color1 inl">Прикрепить файлы</div>
        </div>

        <div class="control-group ">
            <label class="control-label">Прикрепить файлы</label>
            <div class="controls">
                <div class="file_block sbR btn btn-success fileinput-button">
                    <div class="inl sbR file_block_left">

                    </div><div class="inl sbR file_block_right">

                    </div>
                    <!--
                    <i class="icon-plus icon-white"></i>
                    <span>Приложить файлы</span>
                    -->
                    <input type="file" class="btn btn-success fileinput-button" name="pictures[]" multiple="true" />
                </div>
            </div>
        </div>
    </div>

<div class="item_data num1 inl alignTop">
    <?php 
    echo $form->textAreaRow(
        $model,
        'extra',
        array(
            'rows' => 6, 
            'cols' => 50,
            'data-toggle' => 'tooltip',
            'data-placement' => 'right',
            'title' => 'Постарайтесь сразу указать существенные требования к работе',
            'class' => ' none '
        )
    );
    ?>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function(){
            $('body').on('click', '.control-label[for="Order_extra"]', function () {
                $('#Order_extra').removeClass('none');
            });
        });
    </script>
    </div>
    <?php  /*
    echo $form->fileFieldRow(
        $model,
        'uploadFiles',
        array(
            'data-toggle' => 'tooltip',
            'data-placement' => 'right',
            'data-title' => 'Закачайте файлы',
            'multiple' => 'true'
        )
    );*/
    ?>
    



    </div>

    <div class="control-group">
        <div class="controls"><?php echo CHtml::submitButton('Отправить', array('class' => 'btn btn-red btn-large btn-primary')); ?></div>        
    </div>


    <?php if(0 && CCaptcha::checkRequirements()): ?>
    <div class="control-group">
        <div class="control-label"><?php echo $form->labelEx($model,'verifyCode'); ?></div>
        <div class="controls">
            <?php $this->widget('CCaptcha'); ?><br />
            <?php echo $form->textField($model,'verifyCode'); ?>
            <div class="hint">Please enter the letters as they are shown in the image above.
            <br/>Letters are not case-sensitive.</div>
            <?php echo $form->error($model,'verifyCode'); ?>
        </div>
    </div>
    <?php endif; ?>

<?php $this->endWidget(); ?>

</div><!-- form -->