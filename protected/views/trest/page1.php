 <!-- begin of denis layout-->
<div id="content">
	<!-- banner-->
<div class="row">
	<section class="banner span12">
	 <h1 class="banner__title">
	 	Дипломная работа, курсовая, реферат по любой дисциплине 
	 </h1>
	 <p class="banner__desc">
	 	Заказ реферата, диплома или курсовой освобождает вам большое количество времени. 
	 </p>
	 <a href="#" class="banner__btn">Узнать цену</a>
	</section>
</div>
<!-- end of banner-->
<!-- selection-->
 	<section class="selection">
 		<h2 class="selection__title">Выберите предмет</h2>
 		<div class="selection__wrapper">
 			<a href="#" class="selection__item">Экономика</a>
 			<a href="#" class="selection__item">Бухгалтерский учет</a>
 			<a href="#" class="selection__item">Деньги, кредит, банки</a>
 			<a href="#" class="selection__item">Маркетинг</a>
 			<a href="#" class="selection__item">Менеджмент</a>
 			<a href="#" class="selection__item">Торговля</a>
 			<a href="#" class="selection__item">Ресторанно-гостиничный бизнес,туризм </a>
 			<a href="#" class="selection__item">Налоги</a>
 			<a href="#" class="selection__item">Логистика</a>
 			<a href="#" class="selection__item">Макроэкономика</a>
 		</div>
 	</section>
<!-- end of selection-->
<!-- page-block -->
<section class="page-block">
	<h3 class="page-block__title">
		Гарантии
	</h3>
	<p class="page-block__desc">
		Мы сделаем вам диплом с 50% предоплатой, бесплатными дорaботками. Даем вам гаринтии, что доведем до защиты.
	</p>
</section>
<!-- end of page-block-->
<!-- pay slider -->
<div class="page-slider">
	<div class="pay-row swiper-container pay-container-js">
		<div class="pay-list swiper-wrapper">
			<div class="pay-item swiper-slide">
				<div class="pay-item__img">
					<img src="/images/img-sb.gif" alt=""></div>
				<div class="pay-item__popup"><span>Для осуществления операции перевода с использованием карточки отправителю достаточно знать только полный номер карточки получателя денежных средств (16-значный номер на лицевой стороне карточки).</span></div>
			</div>
			<div class="pay-item swiper-slide">
				<div class="pay-item__img">
					<img src="/images/img-alfa.gif" alt=""></div>
				<div class="pay-item__popup"><span>У нашей компании есть расчетный счет в Альфа-банке. Перевести денежные средства Вы можете через отделение любого банка, а также при помощи Альфа-Клик или через банкоматы.</span></div>
			</div>
			<div class="pay-item swiper-slide">
				<div class="pay-item__img">
					<img src="/images/img-vtb.gif" alt=""></div>
				<div class="pay-item__popup"><span>Специально для Вашего удобства мы выбрали три самых крупных банка РФ. Перевод на счет ВТБ 24 возможен после оформления заказа. Реквизиты будут присланы Вам персональным менеджером.</span></div>
			</div>
			<div class="pay-item swiper-slide">
				<div class="pay-item__img">
					<img src="/images/img-qiwi.gif" alt="">
				</div>
				<div class="pay-item__popup"><span>Перевод на наш счет возможен через платежные терминалы QIWI, а также при помощи мобильного приложения. Транзакции абсолютно безопасны и просты. Примерное время зачисления до 24 часов.</span></div>
			</div>
			<div class="pay-item swiper-slide">
				<div class="pay-item__img">
					<img src="/images/img-sb.gif" alt=""></div>
				<div class="pay-item__popup"><span>Для осуществления операции перевода с использованием карточки отправителю достаточно знать только полный номер карточки получателя денежных средств (16-значный номер на лицевой стороне карточки).</span></div>
			</div>
			<div class="pay-item swiper-slide">
				<div class="pay-item__img">
					<img src="/images/img-alfa.gif" alt=""></div>
				<div class="pay-item__popup"><span>У нашей компании есть расчетный счет в Альфа-банке. Перевести денежные средства Вы можете через отделение любого банка, а также при помощи Альфа-Клик или через банкоматы.</span></div>
			</div>
			<div class="pay-item swiper-slide">
				<div class="pay-item__img">
					<img src="/images/img-vtb.gif" alt=""></div>
				<div class="pay-item__popup"><span>Специально для Вашего удобства мы выбрали три самых крупных банка РФ. Перевод на счет ВТБ 24 возможен после оформления заказа. Реквизиты будут присланы Вам персональным менеджером.</span></div>
			</div>
			<div class="pay-item swiper-slide">
				<div class="pay-item__img">
					<img src="/images/img-qiwi.gif" alt="">
				</div>
				<div class="pay-item__popup"><span>Перевод на наш счет возможен через платежные терминалы QIWI, а также при помощи мобильного приложения. Транзакции абсолютно безопасны и просты. Примерное время зачисления до 24 часов.</span></div>
			</div>
		</div></div>
	<div class="page-slider__prev swiper-button-prev"></div>
    <div class="page-slider__next swiper-button-next"></div>
</div>
<!-- end of pay-slider -->
<!-- process -->
<section class="process">
	<h3 class="process__title">Как мы работаем</h3>
	<ul class="process__list">
		<li class="process__item">
			<div class="process__img">
				<img src="/images/icon5.png" alt="">
			</div>
			<p class="process__desc">
				Оформляете заказ работы на сайте
			</p>
		</li>
		<li class="process__item">
			<div class="process__img">
				<img src="/images/icon2.png" alt="">
			</div>
			<p class="process__desc">
				Вносите аванс 50% от стоимости
			</p>
		</li>
		<li class="process__item">
			<div class="process__img">
				<img src="/images/icon4.png" alt="">
			</div>
			<p class="process__desc">
				Мы приступаем к выполнению работы
			</p>
		</li>
		<li class="process__item">
			<div class="process__img">
				<img src="/images/icon6.png" alt="">
			</div>
			<p class="process__desc">
				В срок сдачи высылаем часть работы для ознакомления
			</p>
		</li>
		<li class="process__item">
			<div class="process__img">
				<img src="/images/icon1.png" alt="">
			</div>
			<p class="process__desc">
				Вы оплачиваете вторую часть и получаете работу полностью
			</p>
		</li>
		<li class="process__item">
			<div class="process__img">
				<img src="/images/icon3.png" alt="">
			</div>
			<p class="process__desc">
				Если будут замечания, мы устраним их БЕСПЛАТНО
			</p>
		</li>
	</ul>
</section>
<!-- end of process -->
<!-- end page-block -->
<section class="page-block">
	<h3 class="page-block__title">
		Контакты
	</h3>
	<p class="page-block__desc page-block__desc--margin">
		Свяжитесь с нами по номеру
	</p>
	<div class="page-block__phone">
		<div class="page-block__phone-item">+7 987 654 57 75</div>
		<div class="page-block__phone-item page-block__phone-item--light">+7 987 654 57 75</div>
	</div>
</section>
<!-- end of page-block -->

<!-- form -->
<div class="row row-grey" id="order">
	<div class="span12 center order-title order-title-non-border">
		<h1>Хотите сейчас узнать стоимость работы?</h1>
		<h3>Заполните форму ниже!</h3>
	</div>
	<div class="">
		<?php $this->renderPartial('form', array('model' => new Order('add'))); ?>
	</div>
</div>
<!-- /form -->

<h3>Преимущества сотрудничества с компанией «ДипломБест»</h3>

<p><a href="#" class="offer-link" data-type="4">Заказ реферата</a>, диплома или курсовой освобождает вам большое количество времени. Все что от вас потребуется — это предоставить подробное задание. В оговоренный срок вы получите готовую работу и отнесете ее преподавателю на проверку. Если по работе будут какие-либо замечания, то мы обязательно их доработаем.</p>
<p>Благодаря высокопрофессиональным работам от наших специалистов вы сможете удивить преподавателей высокой компетентностью и знаниями, ведь ваша работа будет наполнена полезной и интересной информацией, которую не каждый сможет найти. При этом мы даем полную гарантию на то, что работу от нас вы получите вовремя и она будет сдана на высокую оценку.</p>
</div>
<!-- end of of denis layout-->