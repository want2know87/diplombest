<?php
/* @var $this SiteController */
/* @var $services array - массив с услугами */
/* @var $comment array - массив с отзывами */

$this->pageTitle=Yii::app()->name;

// Отзывы
$comment_html = $comment;
?>
<div id="content">
    <?=$services?>
    <div class="container">
        <div class="types_work_line"></div>
        <div>
            <div class="block_info block_info_item1 inl"></div><div class="block_info block_info_item2 inl">

            </div><div class="block_info block_info_slide inl re">
                <div class="ab navigation_swiper_block_info z_index30 wi teCen">
                    <div class="inl item">
                        <div class="inl alignMiddl swiper-button-prev swiper_block_info_prev swiper_block_info_prev-js"></div><div class="inl alignMiddl swiper_block_info_pagination swiper_block_info_pagination-js"></div><div class="inl alignMiddl swiper-button-next swiper_block_info_next swiper_block_info_next-js"></div>
                    </div>
                </div>
                <div class="swiper-container swiper_comment swiper_block_info-js teCen marginT0_9em wi he ab2">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide block_info_new7"></div>
                        <div class="swiper-slide block_info_new6"></div>
                        <div class="swiper-slide block_info_new1"></div>
                        <div class="swiper-slide block_info_new3"></div>
                    </div>
                </div>
            </div><div class="block_info block_info_slide inl re">
                <div class="ab navigation_swiper_block_info z_index30 wi teCen">
                    <div class="inl item">
                        <div class="inl alignMiddl swiper-button-prev swiper_block_info_prev swiper_block_info_prev2-js"></div><div class="inl alignMiddl swiper_block_info_pagination swiper_block_info_pagination2-js"></div><div class="inl alignMiddl swiper-button-next swiper_block_info_next swiper_block_info_next2-js"></div>
                    </div>
                </div>
                <div class="swiper-container swiper_comment swiper_block_info2-js teCen marginT0_9em wi he ab2">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide block_info_new8"></div>
                        <div class="swiper-slide block_info_new5"></div>
                        <div class="swiper-slide block_info_new2"></div>
                        <div class="swiper-slide block_info_new4"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="types_work_line2"></div>
        <div class="teCen h3">Как мы работаем</div>
            <div class="teCen marginT20">
                <div class="inl how_we_work_item alignTop">
                    <img width="80%" class="marginB6" src="/images/icon5.png" alt="">
                    <div>Оформляете заказ работы на сайте</div>
                </div><div class="inl how_we_work_item alignTop">
                    <img width="80%" class="marginB6" src="/images/icon2.png" alt="">
                    <div>Вносите аванс 50% от стоимости</div>
                </div><div class="inl how_we_work_item alignTop">
                    <img width="80%" class="marginB6" src="/images/icon4.png" alt="">
                    <div>Мы приступаем к выполнению работы</div>
                </div><div class="inl how_we_work_item alignTop">
                    <img width="80%" class="marginB6" src="/images/icon6.png" alt="">
                    <div>В срок сдачи высылаем часть работы для ознакомления</div>
                </div><div class="inl how_we_work_item alignTop">
                    <img width="80%" class="marginB6" src="/images/icon1.png" alt="">
                    <div>Вы оплачиваете вторую
                        часть и получаете
                        работу полностью
                    </div>
                </div><div class="inl how_we_work_item alignTop">
                    <img width="80%" class="marginB6" src="/images/icon3.png" alt="">
                    <div>Если будут замечания, мы устраним их БЕСПЛАТНО</div>
                </div>
            </div>
        <div class="types_work_line2"></div>
        <div class="teCen h3">Отзывы клиентов</div>

        <div class="teCen marginT20">
            <div class="tab layoutF wi">
                <div class="cel button_comment_cel alignMiddl">
                     <div class="swiper-button-prev button_comment swiper-button_comment-prev-js swiper-button-prev_my inl"></div>
                </div>
                <div class="cel">
                    <div class="swiper-container swiper_comment swiper_comment-js teCen marginT0_9em">
                        <div class="swiper-wrapper">
                            <?=$comment_html?>
                        </div>
                    </div>
                </div>
                <div class="cel button_comment_cel alignMiddl">
                    <div class="swiper-button-next  button_comment re2 swiper-button_comment-next-js swiper-button-next_my inl"></div>
                </div>
            </div>

            <div class="marginT20">
                <div class="link inl comment_click-js">Оставить отзыв</div>
                <div class="marginT20 comment_form-js none2">
                    <form enctype="multipart/form-data" class="form-horizontal teLef inl" action="/" method="post">
                        <div class="control-group ">
                            <label class="control-label required" for="Order_theme">Имя <span class="required">*</span></label>
                            <div class="controls">
                                <input class="name-js" data-toggle="tooltip" data-placement="right" data-title="Введите имя." message="Тему нужно указать" name="comment_name" type="text" maxlength="255" /><span class="help-inline error" id="Order_theme_em_" style="display: none"></span>
                            </div>
                        </div>
                        <div class="control-group ">
                            <label class="control-label required" for="Order_theme">Отзыв <span class="required">*</span></label>
                            <div class="controls">
                                <textarea class="content-js" rows="6" cols="50" data-toggle="tooltip" data-placement="right" title="Введите текст отзыва." name="comment"></textarea><span class="help-inline error" ></span>
                            </div>
                        </div>
                        <div class="control-group ">
                            <div class="controls">
                                <div class="btn btn-large btn-primary comment_submit-js">Добавить</div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="tab marginT20 layoutF wi zakaz_tabl">
              <div class="cel re alignTop bag1  zakaz_tabl_content zakaz_tabl_content_right zakaz_tabl_content_left4">
                <h3>Станьте нашим автором</h3>
                Приглашаем к сотрудничеству всех заинтересованных высококвалифицированных преподавателей и специалистов, готовых работать на рынке написания научных и студенческих работ.
                <div class="teCen">
                    <a href="/site/authors" class="inl become_author">
                        СТАТЬ АВТОРОМ
                    </a>
                </div>
              </div>
              <div class="cel alignMiddl">
                  <img src="/images/zakaz_img4.png" class="wi" alt="">
              </div>
        </div>

        <div>
        <h2>ДипломБест - заказать курсовую или сделать диплом на заказ?!</h2>

        <p>В жизни каждого человека бывают обстоятельства, из-за которых он не может уделять учебе столько времени, сколько она его требует. Причины могут быть различными: семейные обстоятельства, болезнь, параллельная работа с целью заработка и получения стажа. При написании рефератов, курсовых, контрольных или диплома у студента возникают «пожары»: работу в любом случае надо делать, а времени и желания на нее ну совсем нет. Чтобы решить возникшие проблемы компания «ДипломБест» предлагает вам <a href="#" class="offer-link" data-type="2">заказать курсовую</a>, реферат или диплом у наших специалистов, тем самым сохранив свое время и силы.</p>

        <h3>Неприятности от самостоятельного написания работ</h3>

        <p>Подумайте про все те неприятности, которыми сопровождается написание любой работы в ВУЗе: посещение холодных библиотек, уставшие красные глаза и боль в спине от сидения за компьютером, уйма потраченного времени, постоянное волнение и переживание. Студенческие годы не стоят того, чтобы так тратить их. Обратитесь в нашу компанию и мы выполним для вас <a href="#" class="offer-link" data-type="1">диплом на заказ</a>, насколько бы сложной не была его тема, а вы освободите таким образом несколько месяцев времени, которое сможете потратить на более любимое занятие. У нашей компании уже очень большое количество клиентов, их лица счастливы и беззаботны, ведь они могут заниматься любимым делом и не переутомляться.</p>

        <h3>Что вам предлагает компания «ДипломБест»?</h3>
        <ul class="list-unstyled">
        <li>Выполнение контрольных работ</li>
        <li>Курсовые, рефераты и <a href="#" class="offer-link" data-type="1">диплом на заказ</a> — оригинальные работы, которые выполнены специально для вас, независимо от их сложности и требований</li>
        <li>Короткие сроки выполнения работ и приемлемые цены</li>
        <li>Срочные заказы</li>
        <li>Сопровождение проекта до тех пор, пока он не будет сдан</li>
        <li>Конфиденциальность, про то, что вы заказывали у нас работу, никто не узнает</li>
        </ul>

        <h3>Преимущества сотрудничества с компанией «ДипломБест»</h3>

        <p><a href="#" class="offer-link" data-type="4">Заказ реферата</a>, диплома или курсовой освобождает вам большое количество времени. Все что от вас потребуется — это предоставить подробное задание. В оговоренный срок вы получите готовую работу и отнесете ее преподавателю на проверку. Если по работе будут какие-либо замечания, то мы обязательно их доработаем.</p>
        <p>Благодаря высокопрофессиональным работам от наших специалистов вы сможете удивить преподавателей высокой компетентностью и знаниями, ведь ваша работа будет наполнена полезной и интересной информацией, которую не каждый сможет найти. При этом мы даем полную гарантию на то, что работу от нас вы получите вовремя и она будет сдана на высокую оценку.</p>

        <h3>Не рискуйте</h3>

        <p>Сейчас существует большое количество фирм, которые предлагают <a href="#" class="offer-link" data-type="2">заказать курсовую</a>, реферат или <a href="#" class="offer-link" data-type="1">диплом на заказ</a> за мизерную цену с супер качеством. Не стоит верить подобным предложениям, ведь часто за ними скрывается обман. Часто люди после того, как стали жертвами подобных предложений, обращаются в нашу компанию с просьбой помочь в выполнении работы. Такие заказы попадают в категорию срочных, и наши специалисты делают все возможное для того, чтобы работа была сдана в срок и выполнена безупречно.</p>
        <p>В такой ситуации вы теряете деньги и время, поэтому лучше сразу обратиться в компанию с проверенной репутацией, которой является «ДипломБест». Все работы, которые мы выполняем, являются полностью уникальными. Чтобы выполнить <a href="#" class="offer-link" data-type="4">заказ реферата</a>, диплома, курсовой или контрольной, вам необходимо связаться с нами по указанным на сайте контактам, либо заполнить бланк заказа и как можно подробней и точнее объяснить задание.</p>

        </div>
    </div>
</div>
