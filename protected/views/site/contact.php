<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

//$this->npTitle=Yii::app()->name . ' - Обратная связь';
$this->breadcrumbs=array(
	'Обратная связь',
);
?>

<div id="content">
	<?=$this->renderPartial('//layouts/_breadcrumbs')?>
	<div class="container">
		<div class="row">
			<?php
			if ($this->banner) {
			    echo CHtml::image($this->banner);
			}
			?>
		</div>
	</div>
    <div class="container">
        <div class="row">
			<div class="contacts-padding"></div>
			<div class="contacts-wrap">
				<div class="contacts-wrap__header" style="margin-bottom: 15px;">Diplombest.com</div>
				<div class="contacts-wrap__txt">Удобный онлайн сервис помощи для заказа студенческих работ</div>
				<div class="contacts-wrap__col">
					<div class="contacts-wrap__col-head">
						<p>105005 Москва ул. Радио д 5 стр 3</p>
						<p>(не является офисом для клиентов) </p>
						<p><a href="mailto:a@diplombest.com">a@diplombest.com </a></p>
					</div>
					<div class="contacts-wrap__col-phone">
						<p><span class="ico-info"><img src="/images/ico-gor.gif" alt=""></span><a href="tel:+74996776860">+7 (499) 677-68-60</a></p>
						<p><span class="ico-info"><img src="/images/ico-bil.gif" alt=""></span><a href="tel:+79093845550">+7 (909) 384-555-0</a></p>
						<p><span class="ico-info"><img src="/images/ico-mts.gif" alt=""></span><a href="tel:+79876545775">+7 (987) 654-577-5</a></p>
						<p><span class="ico-info"><img src="/images/ico-meg.gif" alt=""></span><a href="tel:+79377425558">+7 (937) 742-555-8</a></p>
					</div>
					<p><span class="ico-info"><img src="/images/ico-icq.gif" alt=""></span>370014682, 650929011</p>
					<p><span class="ico-info"><img src="/images/ico-sky.gif" alt=""></span><a href="skype:kreichi1985">kreichi1985</a></p>
					<p><span class="ico-info"><img src="/images/ico-vac.gif" alt=""></span><a href="whatsapp://chat:+79876545775">+7 987 654 57 75</a></p>
					<p><span class="ico-info"><img src="/images/ico-vb.gif" alt=""></span>+7 987 654 57 75</p>
				</div>
				<div class="contacts-wrap__col">
					<div class="contacts-wrap__col-head">
						<p>ИП Крейчи Артем Олегович</p>
						<p>ОГРНИП 314344333000036</p>
						<p>ИНН 344213038401</p>
					</div>
					<div class="contacts-wrap__col-time">
						<p>Пн-пт: 08:00-24:00</p>
						<p>Сб: 09:00-18:00</p>
						<p>Вс: выходной</p>
					</div>
					<p><span class="ico-info"><img src="/images/ico-vk.gif" alt=""></span><a href="https://vk.com/diplombest_com" target="_blank">Мы в контакте</a></p>
					<p><span class="ico-info"><img src="/images/ico-inst.gif" alt=""></span><a href="https://www.instagram.com/_u/diplombest_com/" target="_blank">Мы в инстаграм</a></p>
				</div>
			</div>
			<div class="matter-form">
				<h1>Задать вопрос</h1>
					<?php if(Yii::app()->user->hasFlash('contact')): ?>

					<div class="flash-success">
						<?php echo Yii::app()->user->getFlash('contact'); ?>
					</div>

					<?php else: ?>

					<div class="form">

						<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
							'id'=>'contact-form',
							'enableClientValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
							),
						)); ?>

							<p class="note">Поля, отмеченные <span class="required">*</span>, обязательны для заполнения.</p>

							<?=$form->errorSummary($model); ?>

							<?=$form->textFieldRow($model, 'name'); ?>
							<?=$form->textFieldRow($model, 'email'); ?>
							<?=$form->textFieldRow($model, 'subject',array('size'=>60,'maxlength'=>128)); ?>
							<?=$form->textAreaRow($model, 'body',array('rows'=>6, 'cols'=>50)); ?>

							<?php if(0 && CCaptcha::checkRequirements()): ?>
								<?php echo $form->labelEx($model,'verifyCode'); ?>
								<div>
								<?php $this->widget('CCaptcha'); ?>
								<?php echo $form->textField($model,'verifyCode'); ?>
								</div>
								<div class="hint">Please enter the letters as they are shown in the image above.
								<br/>Letters are not case-sensitive.</div>
								<?php echo $form->error($model,'verifyCode'); ?>
							<?php endif; ?>

							<div class="buttons">
								<?php echo CHtml::submitButton('Отправить', array('class' => 'btn btn-primary')); ?>
							</div>

						<?php $this->endWidget(); ?>

					</div><!-- form -->

					<?php endif; ?>
			</div>
		</div>
	</div>
</div>
