<?php
/* @var $this SiteController */

$this->npTitle=Yii::app()->name . ' - Схема работы';
$this->breadcrumbs=array(
	'Схема работы',
);
?>

<style>
	.scheme-wrap-mob {display: none;}
	@media (max-width: 767px) {
	    .scheme-wrap-pc {display: none;}
	    .scheme-wrap-mob {display: block;}
	}
</style>

<div id="content">
	<?=$this->renderPartial('//layouts/_breadcrumbs')?>
	<div class="container">
		<div class="row">
			<div class="contacts-padding"></div>
			<div class="scheme">
				<div class="scheme__header">Схема работы</div>
				<div class="scheme-wrap scheme-wrap-pc">
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-1.png" alt=""></div>
						<span>Оформление</span>
						Оформляете заказ работы на сайте
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-2.png" alt=""></div>
						<span>Наша оценка</span>
						Производим подбор специалиста на вашу работу и отправляем вам наше предложение по цене
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-3.png" alt=""></div>
						<span>Аванс</span>
						Вностите аванс 50% от стоимости услуг
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-4.png" alt=""></div>
						<span>Выполнение задания</span>
						Мы приступаем к выполнению всей работы
					</div>

					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-5.png" alt=""></div>
						<span>Бесплатные правки</span>
						Если будут замечания, мы устраним их бесплатно
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-6.png" alt=""></div>
						<span>Сдача преподавателю</span>
						Сдаете работу преподаватлю и получаете оценку.
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-7.png" alt=""></div>
						<span>Оплата</span>
						Оплаты второй части суммы и получение вами работы
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-8.png" alt=""></div>
						<span>Срок сдачи</span>
						В срок сдачи автор сдаёт уже готовый заказ, мы проверяем её и высылаем часть для ознакомления
					</div>
				</div>

				<div class="scheme-wrap scheme-wrap-mob">
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-1.png" alt=""></div>
						<span>Оформление</span>
						Оформляете заказ работы на сайте
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-2.png" alt=""></div>
						<span>Наша оценка</span>
						Производим подбор специалиста на вашу работу и отправляем вам наше предложение по цене
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-3.png" alt=""></div>
						<span>Аванс</span>
						Вностите аванс 50% от стоимости услуг
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-4.png" alt=""></div>
						<span>Выполнение задания</span>
						Мы приступаем к выполнению всей работы
					</div>

					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-8.png" alt=""></div>
						<span>Срок сдачи</span>
						В срок сдачи автор сдаёт уже готовый заказ, мы проверяем её и высылаем часть для ознакомления
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-7.png" alt=""></div>
						<span>Оплата</span>
						Оплаты второй части суммы и получение вами работы
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-6.png" alt=""></div>
						<span>Сдача преподавателю</span>
						Сдаете работу преподаватлю и получаете оценку.
					</div>
					<div class="scheme-wrap__item">
						<div class="scheme-wrap__img"><img src="/images/scheme-5.png" alt=""></div>
						<span>Бесплатные правки</span>
						Если будут замечания, мы устраним их бесплатно
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
