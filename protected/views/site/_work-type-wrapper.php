<div class="work-type__wrapper">
    <?php foreach ($services as $item_services): ?>
        <div class="work-type__block" data-type="<?=$item_services->id?>">
            <?php if ($item_services->is_primary): ?>
            <a href="<?=$this->createUrl('/services/index', array('order_type_alias' => $item_services->alias))?>" class="work-type__heading"><?=CHtml::encode($item_services->title)?></a>
            <?php else: ?>
            <a href="/zakaz/<?=$item_services->alias?>" class="work-type__heading"><?=CHtml::encode($item_services->title)?></a>
            <?php endif; ?>
            <?php
            $lines = preg_split('/[\r\n]+/', $item_services->descr, -1, PREG_SPLIT_NO_EMPTY);
            ?>
            <div class="work-type__about">
                <div class="work-type__about-price">
                    <div class="work-type__about-header">Цена</div>
                    <div class="work-type__about-sum"><?=$lines[0]?></div>
                </div>
                <div class="work-type__about-price">
                    <div class="work-type__about-header">Время</div>
                    <div class="work-type__about-sum"><?=$lines[1]?></div>
                </div>
            </div>
            <ul class="work-type__list">
                <?php $lines = array_slice($lines, 2);?>
                <?php foreach ($lines as $line):?>
                    <li class="work-type__item"><?=CHtml::encode($line)?></li>
                <?php endforeach;?>
            </ul>
            <a href="" class="work-type__block-link offer-link" data-type="<?=$item_services->id?>">Заказать</a>
        </div>
        <?php endforeach ?>
</div>
