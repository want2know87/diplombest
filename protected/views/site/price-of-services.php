<?php
/* @var $this SiteController */
/* @var $services array - массив с услугами */
/* @var $services_all_boolean boolean - если true то показываем ссылку на (все типы работ) */

$servicesChunked = array();
$servicesChunked = array_chunk($services, 4);

$cs = Yii::app()->clientScript;
$cs->registerScript('offer-hover', "
	\$('.offer a').on(
		'mouseenter',
		function() {
			\$(this).closest('.offer').addClass('offer-active');
		}
	).on(
		'mouseleave',
		function() {
			\$(this).closest('.offer').removeClass('offer-active');
		}
	);
	/*
	\$('.offer a').click(
		function(e) {
			var worktype = \$(this).closest('.offer').attr('data-type');
			\$('html, body').animate(
				{ scrollTop: \$('#order-form').offset().top },
				1000,
				function() {
					\$('#order-form select.worktype').val(worktype).change().blur();
				}
			);
			e.preventDefault();
			e.stopPropagation();
		}
	); */
	\$('.offer-link').click(
		function(e) {
			var worktype = \$(this).attr('data-type');
			\$('html, body').animate(
				{ scrollTop: \$('#order-form').offset().top },
				1000,
				function() {
					\$('#order-form select.worktype').val(worktype).change().blur();
				}
			);
			e.preventDefault();
			e.stopPropagation();
		}
	);"
);
?>
<div id="work-type-container">

<section class="work-type">
	 <div class="container">
	 	<h2 class="work-type__title">Типы студенческих работ</h2>

		<?php foreach ($servicesChunked as $chunk): ?>
			<?=$this->renderPartial('_work-type-wrapper', array(
				'services' => $chunk,
			))?>
		<?php endforeach; ?>

		<?php if (isset($services_all_boolean)): ?>
		<div class="work-type__show">
		    <a class="work-type__link"
		       ic-get-from="<?=$this->createUrl('site/ajaxservices')?>"
		       ic-target="#work-type-container"
		       ic-replace-target="true"
		       ic-indicator="#indicator"
	        >Показать все типы работ <i id="indicator" class="fa fa-spinner fa-spin ic-indicator" style="display:none"></i></a>
		</div>
		<?php endif; ?>
	</div>
</section>
<div class="container">
	<div class="row" id="order">
		<div class="span12 center order-title">
		    <div class="h1_style center">Хотите сейчас узнать стоимость работы?</div>
			<div class="h3_style center">Заполните форму ниже!</div>
		</div>
		<div class="">
			<?php $this->renderPartial('//order/form', array('model' => new Order('add'))); ?>
		</div>
	</div>
</div>

</div>
