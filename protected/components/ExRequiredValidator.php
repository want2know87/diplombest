<?php
/**
 * ExRequiredValidator class file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2008-2013 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

class ExRequiredValidator extends CRequiredValidator
{
	/**
	 * @var mixed the desired value that the attribute must have.
	 * If this is null, the validator will validate that the specified attribute does not have null or empty value.
	 * If this is set as a value that is not null, the validator will validate that
	 * the attribute has a value that is the same as this property value.
	 * Defaults to null.
	 */
	public $boundValue;
	/**
	 * @var mixed the desired attribute.
	 * If this is set as a value that is not null, the validator will validate that
	 * the attribute has a value that is the same as this property value.
	 * Defaults to null.
	 */
	public $boundAttribute;
	/**
	 * Validates the attribute of the object.
	 * If there is any error, the error message is added to the object.
	 * @param CModel $object the object being validated
	 * @param string $attribute the attribute being validated
	 */
	protected function validateAttribute($object,$attribute)
	{
		$value=$object->$attribute;
		if (!isset($this->boundAttribute) || !isset($object->{$this->boundAttribute})) {
			throw new CException(Yii::t('yii','The "boundAttribute" property must be specified with a list of values.'));
		}
		if (!isset($this->boundValue)) {
			throw new CException(Yii::t('yii','The "boundValue" property must be specified with a list of values.'));
		}
		$boundValue = $object->{$this->boundAttribute};
		if ($this->boundValue != $boundValue) {
		    return;
		}
		if($this->requiredValue!==null)
		{
			if(!$this->strict && $value!=$this->requiredValue || $this->strict && $value!==$this->requiredValue)
			{
				$message=$this->message!==null?$this->message:Yii::t('yii','{attribute} must be {value}.',
					array('{value}'=>$this->requiredValue));
				$this->addError($object,$attribute,$message);
			}
		}
		elseif($this->isEmpty($value,$this->trim))
		{
			$message=$this->message!==null?$this->message:Yii::t('yii','{attribute} cannot be blank.');
			$this->addError($object,$attribute,$message);
		}
	}

	/**
	 * Returns the JavaScript needed for performing client-side validation.
	 * @param CModel $object the data object being validated
	 * @param string $attribute the name of the attribute to be validated.
	 * @return string the client-side validation script.
	 * @see CActiveForm::enableClientValidation
	 * @since 1.1.7
	 */
	public function clientValidateAttribute($object,$attribute)
	{
		$message=$this->message;
		if($this->requiredValue!==null)
		{
			if($message===null)
				$message=Yii::t('yii','{attribute} must be {value}.');
			$message=strtr($message, array(
				'{value}'=>$this->requiredValue,
				'{attribute}'=>$object->getAttributeLabel($attribute),
			));
			return "
if(jQuery('#" . CHtml::activeId($object, $this->boundAttribute) . "').val()==" . CJSON::encode($this->boundValue) . " && value!=" . CJSON::encode($this->requiredValue) . ") {
	messages.push(".CJSON::encode($message).");
} else {
	jQuery('label[for=" . CHtml::activeId($object, $this->boundAttribute) . "]').removeClass('error success');
}
";
		}
		else
		{
			if($message===null)
				$message=Yii::t('yii','{attribute} cannot be blank.');
			$message=strtr($message, array(
				'{attribute}'=>$object->getAttributeLabel($attribute),
			));
			if($this->trim)
				$emptyCondition = "jQuery.trim(value)==''";
			else
				$emptyCondition = "value==''";
			return "
if(jQuery('#" . CHtml::activeId($object, $this->boundAttribute) . "').val()==" . CJSON::encode($this->boundValue) . ") {
	if({$emptyCondition}) {
		messages.push(".CJSON::encode($message).");
	}
} else {
	jQuery('label[for=" . CHtml::activeId($object, $this->boundAttribute) . "]').removeClass('error');
}
";
		}
	}
}
