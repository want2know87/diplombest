<?php
/**
 * EAuthWidget class file.
 *
 * @author Maxim Zemskov <nodge@yandex.ru>
 * @link http://github.com/Nodge/yii-eauth/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

/**
 * The EAuthWidget widget prints buttons to authenticate user with OpenID and OAuth providers.
 *
 * @package application.extensions.eauth
 */

Yii::import('ext.eauth.EAuthWidget');

class AuthWidget extends EAuthWidget {


	/**
	 * Executes the widget.
	 * This method is called by {@link CBaseController::endWidget}.
	 */
	public function run() {
		//parent::run();

		$this->registerAssets();
		$this->render('external', array(
			'id' => $this->getId(),
			'services' => $this->services,
			'action' => $this->action,
		));
	}

}
