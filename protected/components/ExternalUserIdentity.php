<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class ExternalUserIdentity extends EAuthUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		if (parent::authenticate()) {
			$account = Account::model()->with('user')->findByAttributes(
				array(
					'service' => $this->service->getServiceName(),
					'account_id' => $this->service->getId()
				)
			);
			if (is_null($account) || is_null($account->user)) {
				if (Yii::app()->user->isGuest) {
					$user = new User(User::SCENARIO_EXTERNAL);
					$name = $this->service->getAttribute('name');
					$user->username = $name ? $name : $this->service->getId();
					$user->name = $name ? $name : $this->service->getId();
					$user->email = $this->service->getAttribute('email');
				    if ($user->save()) {
						$user->refresh();
				    } else {
						$this->errorCode = self::ERROR_NOT_AUTHENTICATED;
						return !$this->errorCode;
				    }
				} else {
				    $user = Yii::app()->user->getRecord();
				}
				if (is_null($account)) {
					$account = new Account;
					$account->service = $this->service->getServiceName();
					$account->account_id = $this->service->getId();
				}
				$account->user_id = $user->id;
				$account->save();
				$account->refresh();
			} else {
				$user = $account->user;
				if (!Yii::app()->user->isGuest && $user->id != Yii::app()->user->getId()) {
					//TODO: users merge
					$user = Yii::app()->user->getRecord()->mergeWith($user->id);
				}
			}

			$this->id = $user->id;
			$this->name = $user->name;
		}
		return !$this->errorCode;
	}
}