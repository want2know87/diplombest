<?php

/**
 * WebUser represents current apllication user.
 */
class WebUser extends CWebUser
{
    protected $record;

    public function getRecord($scenario = null)
    {
        if (!$this->record) {
            $this->record = User::model()->with('accounts')->findByPk($this->getId());
			if (!$this->record) {
			    $this->record = new User;
			}
        }
		if ($scenario) {
		    $this->record->setScenario($scenario);
		}
		
		return $this->record;
    }

}