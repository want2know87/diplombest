<div class="services">
	<ul class="unstyled auth-services">
	<?php foreach ($services as $service): ?>
		<li class="auth-service <?=$service->id?>">
		<?php if (!Yii::app()->user->isGuest && ($account = Yii::app()->user->getRecord()->account($service->id))): ?>
			<a class="btn btn-large disabled" href="#">
				<i class="icon-ok icon-large"></i> <?=$service->title?>
			</a>
		<?php else: ?>
			<a href="<?=Yii::app()->controller->createUrl('//auth/external', array('service' => $service->id))?>" class="auth-link <?=$service->id?>"><span class="auth-icon <?=$service->id?>"><i></i></span><span class="auth-title"><?=$service->title?></span></a>
		<?php endif; ?>
		</li>
	<?php endforeach; ?>
	</ul>
</div>