<?php

/**
 * SilentUserIdentity represents the data needed to identity a user.
 * It contains the authentication method that always returns true.
 */
class SilentUserIdentity extends CUserIdentity
{
    private $_id;

	public function __construct(User $user) {
		if ($user->isNewRecord) {
		    throw new Exception('Cannot login unsaved user');
		}
		$this->_id = $user->id;
		$this->username = $user->username;
		parent::__construct($this->username, null);
		$this->authenticate();
	}

	public function authenticate()
    {
        return true;
    }

    public function getId()
    {
        return $this->_id;
    }

}