<?php

Yii::import('ext.yii-booster.widgets.TbBreadcrumbs');

class SchemaBreadcrumbs extends TbBreadcrumbs
{
    public $htmlOptions = array(
        'class'     => 'breadcrumb',
        'itemscope' => '',
        'itemtype'  => 'https://schema.org/BreadcrumbList',
    );

    public $activeLinkTemplate='<a itemprop="item" href="{url}"><span itemprop="name">{label}</span><meta itemprop="position" content="{position}"></a>';

    public function run()
    {
        if (empty($this->links))
            return;

        echo CHtml::openTag($this->tagName, $this->htmlOptions);

        if ($this->homeLink === null) {
            $this->homeLink = CHtml::link(
                '<span itemprop="name">' . Yii::t('zii', 'Home') . '</span>' . '<meta itemprop="position" content="0">',
                Yii::app()->homeUrl,
                array(
                    'itemprop' => 'item',
                )
            );
        }
        if ($this->homeLink !== false) {
            // check whether home link is not a link
            $active = (stripos($this->homeLink, '<a') === false) ? ' class="active"' : '';
            echo '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"' . $active . '>' . $this->homeLink . $this->separator . '</li>';
        }

        end($this->links);
        $lastLink = key($this->links);

        $i = 1;
        foreach ($this->links as $label => $url) {
            if (is_string($label) || is_array($url)) {
                echo '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">';
                echo strtr($this->activeLinkTemplate, array(
                    '{url}' => CHtml::normalizeUrl($url),
                    '{label}' => $this->encodeLabel ? CHtml::encode($label) : $label,
                    '{position}' => $i++,
                ));
            } else {
                echo '<li class="active">';
                echo str_replace('{label}', $this->encodeLabel ? CHtml::encode($url) : $url, $this->inactiveLinkTemplate);
            }

            if ($lastLink !== $label) {
                echo $this->separator;
            }
            echo '</li>';
        }

        echo CHtml::closeTag($this->tagName);
    }
}
