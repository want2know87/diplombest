$(document).ready(function(){
    
    var MyJs = {
        slides_property:'',
        
        start: function () {
            this.index();
            this.slides();
        },
        
        /**
         * Слайдер для комментариев
         */
        slides: function () {
            var slides = new Swiper('.swiper_comment-js', {
                spaceBetween: 5,
                slidesPerView: 3,
                nextButton: '.swiper-button_comment-next-js',
                prevButton: '.swiper-button_comment-prev-js',
                breakpoints: {
                    1008: {
                        slidesPerView: 1
                    }
                }
            });
            
            this.slides_property = slides;
        },
        
        index: function () {
            var thi_s = this;

            new Swiper('.swiper_block_info-js', {
                spaceBetween: 5,
                slidesPerView: 1,
                paginationClickable: true,
                pagination: '.swiper_block_info_pagination-js',
                nextButton: '.swiper_block_info_next-js',
                prevButton: '.swiper_block_info_prev-js',
            });
            
            new Swiper('.swiper_block_info2-js', {
                spaceBetween: 5,
                slidesPerView: 1,
                paginationClickable: true,
                pagination: '.swiper_block_info_pagination2-js',
                nextButton: '.swiper_block_info_next2-js',
                prevButton: '.swiper_block_info_prev2-js',
            });
            
            $('body').on('click', '.comment_submit-js', function () {
                
                var name = $('.comment_form-js .name-js');
                var content = $('.comment_form-js .content-js');
                var error = false;
                
                if(!name.val() || !content.val()){
                    if(!name.val()){
                        name.parents('.control-group').addClass('error');
                    }else{
                        name.parents('.control-group').removeClass('error');
                    }
                    
                    if(!content.val()){
                        content.parents('.control-group').addClass('error');
                    }else{
                        content.parents('.control-group').removeClass('error');
                    }
                    
                    error = true;
                }
                
                if(!error){
                    $('.comment_form-js .control-group').removeClass('error');
                    
                    var data = {};
                    data.name = name.val();
                    data.content = content.val();
                    
                    $.ajax({
                        type: "POST",
                        dataType:'json',
                        url: '/comment/createajax',
                        data: data,
                        success: function(server){
                            //$('body').html(server.comment);
                            name.val('');
                            content.val('');
                           
                           thi_s.slides_property.prependSlide([server.comment]);
                        }
                    });
                }
            });
        
            $('body').on('click', '.comment_click-js', function () {
                $('.comment_form-js').slideToggle();
            });
            
            // Вставляем html галки успешно заполненной формы
            $('#order-form .control-group .required').before('<div class="forma_jackdaw ab none2"></div>');
        }
    };
    
    MyJs.start();
});